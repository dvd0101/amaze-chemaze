#!/bin/bash

# This script runs the Amaze server re-opening it at the right time just after
# the outro ends

# Assuming the audio format we use is fixed, we can derive ms length from file
# size. We use the longest outro as reference.
LEN=`wc -c assets/outro_2.wav | cut -d " " -f 2`

# length / ((44100 * 2 * 2) / 1000)
F=176

OUTRO_MS_DURATION=$((LEN / F))

# XXX: server expects cwd to be its own directory
cd build

while true; do
    ./net-server $@

    sleep $OUTRO_MS_DURATION
done
