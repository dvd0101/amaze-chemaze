"""Utility to produce lookup tables simulating the dripping effect.

This program reads a splat rendered as red on black, and saves a separate
image where red channel is the same as source, and green and blue contain
the encoding of the pixel distance from the closest red upwards.

Usage: dripper.py <image> <output image>
"""

from PIL import Image

import sys

im = Image.open(sys.argv[1])
px_in = im.load()
width, height = im.size

out = Image.new(im.mode, im.size)
px_out = out.load()

for x in xrange(width):
    if x % 100 == 0:
        print x

    last_red = None
    red_gauge = 0

    # Scan for red
    for y in xrange(height):
        red, green, blue = px_in[x, y]

        # If we find a red pixel, refill the red gauge
        if red > 127:
            red_gauge += 10
            last_red = y
            green = 0
            blue = 0
        else:
            if red_gauge:
                distance = y - last_red
                green = distance // 256
                blue = distance % 256
                red_gauge -= 1
            else:
                # No red gauge loaded, assume max distance
                green = 255
                blue = 255

        px_out[x, y] = (red, green, blue)

out.save(sys.argv[2])
