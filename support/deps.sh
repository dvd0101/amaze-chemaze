#!/bin/bash
set -euo pipefail

INSTALL_DIR=$(pwd)/deps

LIGHT_BLUE="\033[1;34m"
LIGHT_GREEN="\033[1;32m"
CYAN="\033[0;36m"
YELLOW="\033[1;33m"
RESET_COLOR="\033[0m"

function log {
    echo "--> ${@}"
}

function install_lib {
    project=$1
    src=$2
    dest=${INSTALL_DIR}/lib

    echo -e "${CYAN}Installing ${src} to ${dest}"
    mkdir -p ${dest}
    cp ${src} ${dest}
}

function install_lib_dir {
    project=$1
    src=$2
    dest=${INSTALL_DIR}/lib

    echo -e "${CYAN}Installing ${src} to ${dest}"
    mkdir -p ${dest}
    cp -r ${src}/* ${dest}
}

function install_include {
    project=$1
    src=$2
    mode=${3:-"subdir"}
    if [[ $mode == "base" ]]; then
        dest=${INSTALL_DIR}/include/
    else
        dest=${INSTALL_DIR}/include/${project}
    fi

    echo -e "${CYAN}Installing ${src} to ${dest}"
    mkdir -p ${dest}
    cp ${src} ${dest}
}

function install_include_dir {
    project=$1
    src=$2
    dest=${INSTALL_DIR}/include/${project}

    echo -e "${CYAN}Installing ${src} to ${dest}"
    rm -rf ${dest}
    cp -r ${src} ${dest}
}

function log_project {
    name=$1
    version=$2
    echo -e "${LIGHT_BLUE}${name} ${YELLOW}(${version})${RESET_COLOR}"
}

function log_download {
    url=$1
    echo -e "${LIGHT_GREEN}--> ${url}${RESET_COLOR}"
}

function make_temp_dir {
    # mktemp invocation compatible with both GNU and BSD variants
    mktemp -t tmp.XXXXXXXXXX -d
}

function download {
    url=$1
    output=$2
    log_download "${url}"
    curl --progress-bar --location --output ${output} "${url}"
}

function install_cppformat {
    NAME=C++Format
    VERSION=2.0.0
    URL=https://github.com/cppformat/cppformat/archive/2.0.0.zip
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} sourceball.zip
        unzip -q -o sourceball

        src_dir=${temp_dir}/fmt-2.0.0
        (
            mkdir ${src_dir}/build
            cd ${src_dir}/build
            cmake -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE .. > /dev/null
            make -j5
            install_lib "format" libcppformat.a
            install_include "format" ../format.h
            cd ..
            rm -rf build
        )
        rm -rf ${temp_dir}
    )
}

function install_boost {
    NAME="Boost"
    VERSION="1.60.0"
    V_="${VERSION//./_}"
    URL="http://downloads.sourceforge.net/project/boost/boost/${VERSION}/boost_${V_}.7z?r=&ts=1438894451&use_mirror=skylink"
    log_project "${NAME}" "${VERSION}"
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} boost-${VERSION}.7z

        log "unpacking..."
        7z x boost-${VERSION}.7z > /dev/null

        cd boost_${V_}
        log "start building"
        ./bootstrap.sh --with-libraries=system,thread,coroutine2,coroutine,program_options

        # On OS X the build process fails with this error:
        # libtool: file: bin.v2/libs/program_options/build/darwin-4.2.1/release/link-static/threading-multi/winmain.o has no symbols
        # which is probably a warning related to empty modules (winmain.o is
        # probably all #ifdef-ed out under OS X). We ignore the error there.
        if [[ $(uname) == "Darwin" ]]; then
            ./b2 -j5 cxxflags="-std=c++14" || true
        else
            ./b2 -j5 cxxflags="-std=c++14"
        fi

        install_include_dir "boost" ./boost
        install_lib_dir "boost" ./stage/lib
        rm -rf ${temp_dir}
    )
}

function install_inline_variant {
    NAME=inline_variant_visitor
    COMMIT=772dde63a7f616cf5237a32af5ab014b7ea5ac1e
    VERSION=git+${COMMIT}
    URL=https://github.com/exclipy/inline_variant_visitor/archive/${COMMIT}.zip
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} inline_variant.zip
        unzip -q -o inline_variant.zip
        (
            cd inline_variant_visitor-${COMMIT}
            install_include "inline_variant_visitor" inline_variant.hpp
            install_include "inline_variant_visitor" function_signature.hpp
        )
        rm -rf ${temp_dir}
    )
}

function install_stb {
    NAME=stb
    COMMIT=c9ead07188b342350530e92e14542222c3ad9abe
    VERSION=git+${COMMIT}
    URL=https://github.com/nothings/stb/archive/${COMMIT}.zip
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} stb
        unzip -q -o stb

        src_dir=${temp_dir}/stb-${COMMIT}
        (
            cd ${src_dir}
            install_include "stb" stb_image.h
            install_include "stb" stb_image_write.h
        )
        rm -rf ${temp_dir}
    )
}

function install_concurrentqueue {
    NAME=moodycamel::ConcurrentQueue
    COMMIT=ea7c7c82f7e4e1e5e32108c26e92229dfe09f044
    VERSION=git+${COMMIT}
    URL=https://github.com/cameron314/concurrentqueue/archive/${COMMIT}.zip
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} concurrentqueue
        unzip -q -o concurrentqueue

        src_dir=${temp_dir}/concurrentqueue-${COMMIT}
        (
            cd ${src_dir}
            install_include "concurrentqueue" concurrentqueue.h
            install_include "concurrentqueue" blockingconcurrentqueue.h
        )
        rm -rf ${temp_dir}
    )
}

function install_concurrentqueue {
    NAME=moodycamel::ConcurrentQueue
    COMMIT=3d216afaf9a9320c5b30ef13fed8d6c8a5b1d634
    VERSION=git+${COMMIT}
    URL=https://github.com/cameron314/concurrentqueue/archive/${COMMIT}.zip
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} concurrentqueue
        unzip -q -o concurrentqueue

        src_dir=${temp_dir}/concurrentqueue-${COMMIT}
        (
            cd ${src_dir}
            install_include "concurrentqueue" concurrentqueue.h
            install_include "concurrentqueue" blockingconcurrentqueue.h
        )
        rm -rf ${temp_dir}
    )
}

function install_asio {
    NAME=ASIO
    VERSION="1.11.0 (Development)"
    URL="http://downloads.sourceforge.net/project/asio/asio/1.11.0%20%28Development%29/asio-1.11.0.zip?r=&ts=1440742477&use_mirror=kent"
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} asio-1.11.0.zip
        unzip -q -o asio-1.11.0.zip
        cd asio-1.11.0
        install_include_dir "asio" ./include/asio
        install_include "asio" ./include/asio.hpp "base"
        rm -rf ${temp_dir}
    )
}

function install_glm {
    NAME=GLM
    VERSION="0.9.7.4"
    URL=https://github.com/g-truc/glm/releases/download/0.9.7.4/glm-0.9.7.4.zip
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} sourceball.zip
        unzip -q -o sourceball

        src_dir=${temp_dir}/glm
        (
            cd $src_dir
            install_include_dir "glm" ./glm
        )
        rm -rf ${temp_dir}
    )
}

function install_catch {
    NAME=Catch
    VERSION=(master)
    URL=https://raw.githubusercontent.com/philsquared/Catch/master/single_include/catch.hpp
    log_project ${NAME} ${VERSION}
    temp_dir=$(make_temp_dir)
    (
        log "entering ${temp_dir}"
        cd ${temp_dir}
        download ${URL} catch.hpp
        install_include "catch" catch.hpp
        rm -rf ${temp_dir}
    )
}

ARG=${1:-all}
if [[ $ARG == "cppformat" || $ARG == "all" ]]; then
    install_cppformat
fi
if [[ $ARG == "boost" || $ARG == "all" ]]; then
    install_boost
fi
if [[ $ARG == "inline_variant" || $ARG == "all" ]]; then
    install_inline_variant
fi
if [[ $ARG == "asio" || $ARG == "all" ]]; then
    install_asio
fi
if [[ $ARG == "concurrentqueue" || $ARG == "all" ]]; then
    install_concurrentqueue
fi
if [[ $ARG == "glm" || $ARG == "all" ]]; then
    install_glm
fi
if [[ $ARG == "catch" || $ARG == "all" ]]; then
    install_catch
fi
if [[ $ARG == "stb" || $ARG == "all" ]]; then
    install_stb
fi
