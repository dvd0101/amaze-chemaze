#pragma once
#include "../gl/texture.h"
#include <cstdint>
#include <cstdlib>
#include <memory>
#include <format/format.h>

namespace amaze {
    struct rgb {
        static constexpr uint8_t components = 3;

        uint8_t r() const {
            return data[0];
        }
        uint8_t g() const {
            return data[1];
        }
        uint8_t b() const {
            return data[2];
        }

        bool operator==(const rgb& other) const {
            return data[0] == other.data[0] && data[1] == other.data[1] && data[2] == other.data[2];
        }

        bool operator!=(const rgb& other) const {
            return !(*this == other);
        }

        // Only defined to use instances of this struct in a map
        bool operator<(const rgb& other) const {
            uint32_t c = (data[0] << 16) + (data[1] << 8) + data[2];
            uint32_t o = (other.data[0] << 16) + (other.data[1] << 8) + other.data[2];
            return c < o;
        }

        uint8_t data[3];
    };

    inline std::ostream& operator<<(std::ostream& os, rgb c) {
        fmt::print(os, "rgb({:x},{:x},{:x})", c.r(), c.g(), c.b());
        return os;
    }

    template <typename Pixel>
    class Image {
      public:
        Image(size_t width, size_t height)
            : Image(width, height, reinterpret_cast<Pixel*>(malloc(width * height * sizeof(Pixel)))) {}
        // stb_image allocates memory with std::malloc, so we need to define
        // std::free as deallocator
        Image(size_t width, size_t height, Pixel* data) : _width{width}, _height{height}, _data(data, std::free) {}

      public:
        size_t width() const {
            return _width;
        }
        size_t height() const {
            return _height;
        }

        Pixel* data() {
            return _data.get();
        }

        const Pixel* data() const {
            return _data.get();
        }

      public:
        operator Texture_data() const {
            GLenum fmt = Pixel::components == 3 ? GL_RGB : GL_RGBA;
            auto ptr = reinterpret_cast<const uint8_t*>(data());
            return {width(), height(), fmt, ptr};
        }

      private:
        size_t _width;
        size_t _height;
        std::unique_ptr<Pixel[], void (*)(void*)> _data;
    };
};
