#pragma once
#include "pixels.h"
#include <stdexcept>
#include <string>
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>

namespace amaze {

    namespace png {
        template <typename Pixel>
        Image<Pixel> read(const std::string& filename) {
            int width, height, comp;
            uint8_t* ptr = stbi_load(filename.c_str(), &width, &height, &comp, Pixel::components);
            if (ptr == nullptr) {
                throw std::runtime_error{std::string{"error opening/decoding image "} + filename};
            }

            return Image<Pixel>{static_cast<size_t>(width), static_cast<size_t>(height), reinterpret_cast<Pixel*>(ptr)};
        }

        template <typename Pixel>
        void write(const std::string& filename, const Image<Pixel>& image) {
            int result = stbi_write_png(filename.c_str(),
                                        image.width(),
                                        image.height(),
                                        Pixel::components,
                                        image.data(),
                                        0);
            if (result == 0) {
                throw std::runtime_error{std::string{"error writing the image "} + filename};
            }
        }
    }
}
