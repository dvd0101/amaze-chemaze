#include "world/stream_adapters.h"
#include <boost/program_options.hpp>
#include <format/format.h>
#include <iostream>
#include <fstream>
#include <string>

using amaze::optional;

struct config {
    std::string input;
};

optional<config> parse_cmdline(int argc, char* argv[]) {
    namespace po = boost::program_options;
    using std::string;
    using std::vector;
    config cfg;

    po::options_description desc("Allowed options");
    desc.add_options()                    //
        ("help", "produce help message"); //

    po::options_description input_files("Input file");
    input_files.add_options()                                             //
        ("input", po::value<string>(&cfg.input)->required(), "log file"); //

    po::positional_options_description pos;
    pos.add("input", -1);

    po::options_description all("Allowed options");
    all.add(desc).add(input_files);

    bool force_help = false;
    po::variables_map vm;
    try {
        po::store(                              //
            po::command_line_parser(argc, argv) //
                .options(all)                   //
                .positional(pos)                //
                .run(),                         //
            vm);                                //
        po::notify(vm);
    } catch (po::unknown_option const&) {
        force_help = true;
    } catch (po::required_option const&) {
        force_help = true;
    }

    if (force_help || vm.count("help")) {
        fmt::print("Usage: {} [OPTION] log_file\n", argv[0]);
        fmt::print("{}\n", desc);
        return {};
    }
    return cfg;
}

int main(int argc, char* argv[]) {
    using namespace amaze;
    auto cfg = parse_cmdline(argc, argv);
    if (!cfg) {
        return 1;
    }

    std::ifstream in{cfg->input};
    if (!in) {
        fmt::print(std::cerr, "E: error opening input file\n");
        return 2;
    }

    auto source = amaze::Source_stream{in};
    while(auto evt = source.pull()) {
        fmt::print("{}\n", *evt);
    }
}
