#include "game_renderer.h"
#include "walls_texture.h"
#include "../world/world.h"
#include "../level/builder.h"

#include <format/format.h>

namespace amaze {
    Game_renderer::Game_renderer(const World& w, Hint_textures h, Draw_walls dw, Draw_portals dp, Draw_intro di)
        : world{w}, walls{std::move(dw)}, portals{std::move(dp)}, intro{std::move(di)}, hints{std::move(h)}, t0{now()} {
        setup_level(world.level());
        walls.setup(t0);
    }

    void Game_renderer::setup_level(const level::Level& lvl) {
        {
            level::Walls_builder<> builder;
            auto iter = builder.construct(lvl);
            vertices.load(prepare(iter, hints));
        }

        {
            level::Portals_builder<> builder;
            auto iter = builder.construct(lvl);
            Portal_manager vertices;
            vertices.load(prepare(iter, hints));
            portals.vertices(std::move(vertices));

            portals.setup();
        }

        {
            intro.setup();
        }
    }

    void Game_renderer::draw() {
        const auto t = now() - t0;

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        glEnable(GL_CULL_FACE);

        // WINDING: change polygon winding to clockwise. This is due to our
        // model using a left-handed reference system instead of a right-
        // handed one, and to our use of a transformation matrix to fix this.
        // Note: we could have asked the GL to cull front-facing polygon
        // instead of changing the winding, but this would have been
        // semantically incorrect - we *do* want to cull back-facing polygons,
        // it's just that our back-facing polygons have vertices defined in an
        // order different from GL default.
        glFrontFace(GL_CW);

        if (!world.intro_mode()) {
            portals.draw(world, t);
            walls.draw(world, vertices, t);
        } else {
            intro.draw(world, t);
        }
        GLenum err = glGetError();
        if (err != 0) {
            fmt::print("glGetError(): {}\n", err);
        }
    }
}
