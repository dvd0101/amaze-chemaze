#pragma once
#include "../models/geom.h"
#include "../gl/texture.h"
#include "../images/pixels.h"

#include <vector>

namespace amaze {
    class Hint_textures {
        /*
         * Manager for the hint textures.
         *
         * A splat on our walls can reveal an hint; an hint is an image and
         * this class maintains the textures where these images are kept.
         *
         * The hints are kept in up to 4 atlases; all the hints have the same
         * size so the texture coordinates can be computed with just an int
         * that represents the hint index.
         */
      public:
        using texture = Texture2D<GL_RGB, GL_UNSIGNED_BYTE>;

        static constexpr int max_textures = 4;

      public:
        /*
         * constructs a Hint_textures given the base texture unit to use
         * and the hints size
         */
        Hint_textures(GLenum, uint16_t);

        /*
         * adds a new atlas; an exception is raised if the image is not
         * compatible with the hints size or if the maximum number of atlases
         * is already reached.
         *
         * Note that the image is loaded on the GPU but not copied
         */
        void add_atlas(const Image<rgb>&);

        /*
         * returns the texture coordinates (atlas index, top left, right bottom) of the
         * given hint.
         *
         * raises an exception if the position cannot be mapped to an atlas
         */
        std::tuple<int, Tex_coord, Tex_coord> pos(uint16_t) const;
        /*
         * returns the hint texture coordinates but reduced by a factor;
         *
         * h.pos(1, 0.1/100);
         *
         * returns the texture coordinates for the hint 1 reduced by 0.1%
         *
         * This is useful
         */
        std::tuple<int, Tex_coord, Tex_coord> pos(uint16_t, float) const;

        template <typename T>
        void map(T& tt, uint16_t index) const {
            int atlas_index;
            Tex_coord tl, rb;
            // the hint texture is reduced to prevent some pixels of the adjacent
            // hint (due to the float precision) to become visible.
            std::tie(atlas_index, tl, rb) = pos(index, 0.1 / 100);

            // detect if the triangle is the half bottom or the half top of the cube face;
            // XXX only works for the vertical faces
            auto min_z       = std::min({tt.a.p.z, tt.b.p.z, tt.c.p.z});
            bool half_bottom = tt.a.p.z == min_z;
            if (half_bottom) {
                tt.a.t.x = tl.x;
                tt.a.t.y = rb.y;
                if (tt.b.p.z > tt.c.p.z) {
                    tt.b.t = tl;
                    tt.c.t = rb;
                } else {
                    tt.b.t = rb;
                    tt.c.t = tl;
                }
            } else {
                tt.a.t.x = rb.x;
                tt.a.t.y = tl.y;
                if (tt.b.p.z < tt.c.p.z) {
                    tt.b.t = rb;
                    tt.c.t = tl;
                } else {
                    tt.b.t = tl;
                    tt.c.t = rb;
                }
            }
            tt.setTexture(atlas_index);
        }

      private:
        std::vector<texture> atlases;
        uint16_t hint_size;
        uint16_t atlas_size;
        GLenum texture_unit0;
    };

    // converts a Cube_triangle in a Textured_triangle (assisted by an
    // Hint_textures)
    Textured_triangle prepare(const Cube_triangle&, const Hint_textures&);
    Portal_textured_triangle prepare(const Portal_triangle&, const Hint_textures&);

    template <template <typename> class Iter>
    std::vector<Textured_triangle> prepare(Iter<const Cube_triangle&>& i, const Hint_textures& hints) {
        std::vector<Textured_triangle> triangles;
        for (auto& t : i) {
            if (t.visible) {
                triangles.push_back(prepare(t, hints));
            }
        }
        return triangles;
    }

    template <typename Iter>
    std::vector<Portal_textured_triangle> prepare(Iter& i, const Hint_textures& hints) {
        std::vector<Portal_textured_triangle> triangles;
        for (const Portal_triangle& t : i) {
            triangles.push_back(prepare(t, hints));
        }
        return triangles;
    }

    class Tile_manager {
        // A Tile_manager handles the vertex buffers required to draw the maze.
        //
        // It does not contains a copy of the vertices (and thus has a very low
        // memory footprint) just an handle to a GL buffer.
        //
        // It knows the layout of the vertices and, via the `activate` method,
        // can be used to configure the Vertex Attrib pointer of a GL program
      public:
        void load(const std::vector<Textured_triangle>&);

        int points() const;
        // attributes describes which input we want to feed the shader; it's
        // meant as a interface structure, used by the client to only talk
        // about positions and textures, and not actually care how and where
        // they are stored in CPU/GPU memory. Example usage:
        //
        // Wall_manager::attributes attrs;
        // attrs.positions = 1;
        // mgr.activate(attrs);
        //
        // Specifies that vertices positions are exposed to shader in
        // attribute with index 1.
        struct attributes {

            static constexpr GLuint not_set = std::numeric_limits<GLuint>::max();

            // Attribute to contain vertex positions (3 floats)
            GLuint positions = not_set;

            // Attribute to contain texture coordinates (2 floats)
            GLuint texture_coords = not_set;

            // Attribute to contain texture id (1 int)
            GLuint texture_id = not_set;

            // Attribute to contain vertex normals (3 floats)
            GLuint normals = not_set;
        };

        // activate exposes the requested attributes to the program currently
        // in use
        void activate(attributes&) const;

      private:
        Buffer_h vertex;
        int _points;
    };

    class Portal_manager {
      public:
        void load(const std::vector<Portal_textured_triangle>&);

        int points() const;

        struct attributes {

            static constexpr GLuint not_set = std::numeric_limits<GLuint>::max();

            // Attribute to contain vertex positions (3 floats)
            GLuint positions = not_set;

            // Attribute to contain texture coordinates (2 floats)
            GLuint texture_coords = not_set;

            // Attribute to contain texture id (1 int)
            GLuint texture_id = not_set;

            // Attribute to contain the cube level (1 int)
            GLuint levels = not_set;

            // Attribute to contain the cube center (3 float)
            GLuint centers = not_set;
        };

        // activate exposes the requested attributes to the program currently
        // in use
        void activate(attributes&) const;

      private:
        Buffer_h vertex;
        int _points;
    };
}
