#pragma once
#include "programs.h"
#include "../world/config.h"

namespace amaze {
    class World;

    namespace level {
        class Level;
    }

    class Game_renderer {
      public:
        Game_renderer(const World&, Hint_textures, Draw_walls, Draw_portals, Draw_intro);

        void draw();

      private:
        void setup_level(const level::Level&);

      private:
        const World& world;

      private:
        Draw_walls walls;
        Draw_portals portals;
        Draw_intro intro;
        Tile_manager vertices;
        Hint_textures hints;
        time_point t0;
    };
}
