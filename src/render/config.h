#pragma once
#include "../gl/gl.h"
#include <cstdint>

namespace amaze {
    namespace render {
        struct Config {
            // the first texture for the underlying hints
            static constexpr GLenum tile_hints_texture0 = GL_TEXTURE0;
            static constexpr uint16_t hint_size = 1024;

            static constexpr GLenum splats_texture0 = GL_TEXTURE0 + 4;
            static constexpr GLenum splats_data_texture = GL_TEXTURE0 + 8;
            static constexpr GLenum intro_texture = GL_TEXTURE0 + 9;
            static constexpr GLenum noise_texture = GL_TEXTURE0 + 10;
            static constexpr GLenum wall_texture = GL_TEXTURE0 + 11;

            static constexpr GLenum sky_texture = GL_TEXTURE0;
        };
    }
}
