#pragma once
#include "../../gl/handles.h"
#include "../../gl/shaders.h"
#include "../../gl/texture.h"
#include "../walls_texture.h"
#include "../../world/config.h"
#include <chrono>

namespace amaze {
    class World;

    class Splat_textures {
      public:
        using texture = Texture2D<GL_RGB, GL_UNSIGNED_BYTE>;

        static constexpr int max_textures = 4;

      public:
        Splat_textures(GLenum);

        void load();

      private:
        GLenum texture_unit0;
        texture splat0;
        texture splat1;
        texture splat2;
        texture splat3;
    };

    class Draw_walls {
      public:
        Draw_walls(amaze::Program);

        void setup(const time_point&);
        void draw(const World&, const Tile_manager&, const std::chrono::milliseconds&);

      private:
        void upload_splats(const World&);

      private:
        amaze::Program prg;
        Vertex_array_h vertex_array;

        Splat_textures splats;
        Texture2D<GL_RGB32F, GL_FLOAT> splat_data;

        Texture2D<GL_RGB, GL_UNSIGNED_BYTE> wall;

        struct {
            GLint mvp;
            GLint hint_textures;
            GLint splat_textures;
            GLint splats_data;
            GLint splats_count;
            GLint timestamp;
            GLint wall_sampler;
        } uniforms;

        Tile_manager::attributes prg_attrs;

        size_t splats_count = 0;

        time_point t0;
    };
}
