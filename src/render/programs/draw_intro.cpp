#include "draw_intro.h"
#include "../config.h"
#include "../../world/world.h"
#include "../../images/formats.h"

namespace amaze {
    Draw_intro::Draw_intro(Program p) : _prg{std::move(p)} {
        uniforms.image_texture = glGetUniformLocation(_prg.id(), "image_texture");
        uniforms.noise_texture = glGetUniformLocation(_prg.id(), "noise_texture");
        uniforms.timestamp     = glGetUniformLocation(_prg.id(), "timestamp");
    }

    void Draw_intro::setup() {
        glBindVertexArray(_vertex_array);
        glUseProgram(_prg.id());

        glActiveTexture(render::Config::intro_texture);
        bitmap.load(png::read<rgb>("../assets/intro.png"));

        glActiveTexture(render::Config::noise_texture);
        noise.load(png::read<rgb>("../assets/noise.png"));

        std::vector<Textured_point> buffer(6);
        buffer[0].p = Point{-1, 1, 0};
        buffer[0].t = Tex_coord{0, 0};
        buffer[1].p = Point{1, -1, 0};
        buffer[1].t = Tex_coord{1, 1};
        buffer[2].p = Point{-1, -1, 0};
        buffer[2].t = Tex_coord{0, 1};
        buffer[3].p = Point{-1, 1, 0};
        buffer[3].t = Tex_coord{0, 0};
        buffer[4].p = Point{1, 1, 0};
        buffer[4].t = Tex_coord{1, 0};
        buffer[5].p = Point{1, -1, 0};
        buffer[5].t = Tex_coord{1, 1};

        glBindBuffer(GL_ARRAY_BUFFER, _vertices);
        glBufferData(GL_ARRAY_BUFFER, buffer.size() * sizeof(Textured_point), buffer.data(), GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Textured_point), (void*)0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Textured_point), (void*)(sizeof(Point)));

        glUniform1i(uniforms.image_texture, render::Config::intro_texture - GL_TEXTURE0);
        glUniform1i(uniforms.noise_texture, render::Config::noise_texture - GL_TEXTURE0);
    }

    void Draw_intro::draw(const World& world, const std::chrono::milliseconds& ms) {
        glBindVertexArray(_vertex_array);
        glUseProgram(_prg.id());

        glUniform1f(uniforms.timestamp, float(ms.count()));

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glDrawArrays(GL_TRIANGLES, 0, 2 * 3);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);
    }
}
