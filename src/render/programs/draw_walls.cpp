#include "draw_walls.h"
#include "../config.h"
#include "../../world/world.h"
#include "../../gl/texture.h"
#include "../../images/formats.h"
#include <chrono>

namespace amaze {
    Splat_textures::Splat_textures(GLenum t0) : texture_unit0{t0} {}

    void Splat_textures::load() {
        glActiveTexture(texture_unit0);
        splat0.load(png::read<rgb>("../assets/dripper0.png"));

        glActiveTexture(texture_unit0 + 1);
        splat1.load(png::read<rgb>("../assets/dripper1.png"));

        glActiveTexture(texture_unit0 + 2);
        splat2.load(png::read<rgb>("../assets/dripper2.png"));

        glActiveTexture(texture_unit0 + 3);
        splat3.load(png::read<rgb>("../assets/dripper3.png"));
    }

    Draw_walls::Draw_walls(Program p) : prg{std::move(p)}, splats{render::Config::splats_texture0} {

        uniforms.mvp            = glGetUniformLocation(prg.id(), "mvp");
        uniforms.hint_textures  = glGetUniformLocation(prg.id(), "hint_textures");
        uniforms.splat_textures = glGetUniformLocation(prg.id(), "splat_textures");
        uniforms.splats_data    = glGetUniformLocation(prg.id(), "splats_data");
        uniforms.splats_count   = glGetUniformLocation(prg.id(), "splats_count");
        uniforms.timestamp      = glGetUniformLocation(prg.id(), "timestamp");
        uniforms.wall_sampler   = glGetUniformLocation(prg.id(), "wall_sampler");

        // This class works with a known shader, and is aware of the mapping
        // the shader expects
        prg_attrs.positions      = 0;
        prg_attrs.texture_coords = 1;
        prg_attrs.texture_id     = 2;
        prg_attrs.normals        = 3;

        splats.load();

        // Also, load the wall texture
        glActiveTexture(render::Config::wall_texture);
        wall.load(png::read<rgb>("../assets/wall.png"), Graphics);
        glBindTexture(GL_TEXTURE_2D, wall.id());
    }

    void Draw_walls::setup(const time_point& t) {
        t0 = t;
    }

    void Draw_walls::upload_splats(const World& world) {

        std::vector<float> raw;

        for (auto& splat : world.splats()) {
            raw.push_back(splat.center.x);
            raw.push_back(splat.center.y);
            raw.push_back(splat.center.z);
            raw.push_back(splat.normal.x);
            raw.push_back(splat.normal.y);
            raw.push_back(splat.normal.z);

            // XXX: we should use a start time sent by the server instead of
            // relying on the system clock (which can be off)
            raw.push_back(float((splat.timestamp - t0).count()));
            raw.push_back(float(splat.type));
            raw.push_back(0.0);
        }

        // Update our internal splats count, used to avoid uploading each time
        splats_count = world.splats().size();
        glUniform1i(uniforms.splats_count, static_cast<int32_t>(splats_count));

        // Also, load the splat data texture
        glActiveTexture(render::Config::splats_data_texture);
        splat_data.load(3, splats_count, GL_RGB, raw.data());
        glBindTexture(GL_TEXTURE_2D, splat_data.id());
    }

    void Draw_walls::draw(const World& world, const Tile_manager& mgr, const std::chrono::milliseconds& ms) {
        glBindVertexArray(vertex_array);
        glUseProgram(prg.id());

        if (world.splats().size() != splats_count) {
            upload_splats(world);
        }

        mgr.activate(prg_attrs);

        // this is a design mismatch, these four hints are known only to the
        // Hint_textures but here we have no access to that instance
        GLint hints[] = {render::Config::tile_hints_texture0 - GL_TEXTURE0 + 0,
                         render::Config::tile_hints_texture0 - GL_TEXTURE0 + 1,
                         render::Config::tile_hints_texture0 - GL_TEXTURE0 + 2,
                         render::Config::tile_hints_texture0 - GL_TEXTURE0 + 3};
        glUniform1iv(uniforms.hint_textures, 4, hints);

        GLint splats[] = {render::Config::splats_texture0 - GL_TEXTURE0 + 0,
                          render::Config::splats_texture0 - GL_TEXTURE0 + 1,
                          render::Config::splats_texture0 - GL_TEXTURE0 + 2,
                          render::Config::splats_texture0 - GL_TEXTURE0 + 3};
        glUniform1iv(uniforms.splat_textures, 4, splats);

        glUniform1i(uniforms.splats_data, render::Config::splats_data_texture - GL_TEXTURE0);
        glUniform1f(uniforms.timestamp, float(ms.count()));
        glUniform1i(uniforms.wall_sampler, render::Config::wall_texture - GL_TEXTURE0);

        const auto mvp = world.camera().vp();
        glUniformMatrix4fv(uniforms.mvp, 1, GL_FALSE, &mvp[0][0]);

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(3);
        glDrawArrays(GL_TRIANGLES, 0, mgr.points());
        glDisableVertexAttribArray(3);
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);
    }
}
