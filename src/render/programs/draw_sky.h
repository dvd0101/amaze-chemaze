
#pragma once
#include "../../gl/handles.h"
#include "../../gl/shaders.h"
#include "../../gl/texture.h"
#include <chrono>

namespace amaze {
    class World;

    class Draw_sky {
      public:
        Draw_sky(Program);

        void setup();
        void draw(const World&, const std::chrono::milliseconds&);
      private:
        amaze::Program _prg;
        Vertex_array_h _vertex_array;
        Buffer_h _vertices;
        TextureCube<GL_RGB> _sky;

        struct {
            GLint sky_texture;
            GLint timestamp;
        } uniforms;
    };
}

