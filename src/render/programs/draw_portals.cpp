#include "draw_portals.h"
#include "../config.h"
#include "../../world/world.h"
#include <glm/gtc/matrix_transform.hpp>

namespace amaze {
    Draw_portals::Draw_portals(Program p) : _prg{std::move(p)} {
        uniforms.mvp            = glGetUniformLocation(_prg.id(), "mvp");
        uniforms.image_textures = glGetUniformLocation(_prg.id(), "image_textures");
        uniforms.transform      = glGetUniformLocation(_prg.id(), "transform");
    }

    void Draw_portals::vertices(Portal_manager mgr) {
        _vertices = std::move(mgr);
    }

    void Draw_portals::setup() {
        Portal_manager::attributes attrs;
        attrs.positions      = 0;
        attrs.texture_coords = 1;
        attrs.texture_id     = 2;
        attrs.levels         = 3;
        attrs.centers        = 4;

        glBindVertexArray(_vertex_array);
        glUseProgram(_prg.id());
        _vertices.activate(attrs);

        GLint hints[] = {render::Config::tile_hints_texture0 - GL_TEXTURE0 + 0,
                         render::Config::tile_hints_texture0 - GL_TEXTURE0 + 1,
                         render::Config::tile_hints_texture0 - GL_TEXTURE0 + 2,
                         render::Config::tile_hints_texture0 - GL_TEXTURE0 + 3};
        glUniform1iv(uniforms.image_textures, 4, hints);
    }

    void Draw_portals::draw(const World& world, const std::chrono::milliseconds& ms) {
        glBindVertexArray(_vertex_array);
        glUseProgram(_prg.id());

        const auto mvp = world.camera().vp();
        glUniformMatrix4fv(uniforms.mvp, 1, GL_FALSE, &mvp[0][0]);

        // period of revolution (in ms) for a cube of level 1
        const int revolution = 20000;
        const auto radians = static_cast<float>(ms.count() * 2 * pi / revolution);
        const auto transform = glm::rotate(glm::mat4(1.0f), radians, glm::vec3(0, 0, 1));
        glUniformMatrix4fv(uniforms.transform, 1, GL_FALSE, &transform[0][0]);

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(4);
        glDrawArrays(GL_TRIANGLES, 0, _vertices.points());
        glDisableVertexAttribArray(4);
        glDisableVertexAttribArray(3);
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);
    }
}
