#include "draw_sky.h"
#include "../config.h"
#include "../../world/world.h"
#include "../../images/formats.h"

namespace amaze {
    Draw_sky::Draw_sky(Program p) : _prg{std::move(p)} {
        uniforms.sky_texture = glGetUniformLocation(_prg.id(), "sky_texture");
        uniforms.timestamp     = glGetUniformLocation(_prg.id(), "timestamp");
    }

    void Draw_sky::setup() {
        glBindVertexArray(_vertex_array);
        glUseProgram(_prg.id());

        glActiveTexture(render::Config::sky_texture);
        _sky.load(TextureCube_faces::right, png::read<rgb>("../assets/sky/right.png"));
        _sky.load(TextureCube_faces::left, png::read<rgb>("../assets/sky/left.png"));
        _sky.load(TextureCube_faces::top, png::read<rgb>("../assets/sky/top.png"));
        _sky.load(TextureCube_faces::bottom, png::read<rgb>("../assets/sky/bottom.png"));
        _sky.load(TextureCube_faces::back, png::read<rgb>("../assets/sky/back.png"));
        _sky.load(TextureCube_faces::front, png::read<rgb>("../assets/sky/front.png"));

        // clang-format off
        std::vector<Point> buffer{
            {-1.0f,  1.0f, -1.0f,},
            {1.0f, -1.0f, -1.0f,},
            {-1.0f, -1.0f, -1.0f,},
            {1.0f, -1.0f, -1.0f,},
            {-1.0f,  1.0f, -1.0f,},
            {1.0f,  1.0f, -1.0f,},

            {-1.0f, -1.0f,  1.0f,},
            {-1.0f,  1.0f, -1.0f,},
            {-1.0f, -1.0f, -1.0f,},
            {-1.0f,  1.0f, -1.0f,},
            {-1.0f, -1.0f,  1.0f,},
            {-1.0f,  1.0f,  1.0f,},

            {1.0f, -1.0f, -1.0f,},
            {1.0f,  1.0f,  1.0f,},
            {1.0f, -1.0f,  1.0f,},
            {1.0f,  1.0f,  1.0f,},
            {1.0f, -1.0f, -1.0f,},
            {1.0f,  1.0f, -1.0f,},

            {-1.0f, -1.0f,  1.0f,},
            {1.0f,  1.0f,  1.0f,},
            {-1.0f,  1.0f,  1.0f,},
            {1.0f,  1.0f,  1.0f,},
            {-1.0f, -1.0f,  1.0f,},
            {1.0f, -1.0f,  1.0f,},

            {-1.0f,  1.0f, -1.0f,},
            {1.0f,  1.0f,  1.0f,},
            {1.0f,  1.0f, -1.0f,},
            {1.0f,  1.0f,  1.0f,},
            {-1.0f,  1.0f, -1.0f,},
            {-1.0f,  1.0f,  1.0f,},

            {-1.0f, -1.0f, -1.0f,},
            {1.0f, -1.0f, -1.0f,},
            {-1.0f, -1.0f,  1.0f,},
            {1.0f, -1.0f, -1.0f,},
            {1.0f, -1.0f,  1.0f},
            {-1.0f, -1.0f,  1.0f,},
        };
        // clang-format on

        glBindBuffer(GL_ARRAY_BUFFER, _vertices);
        glBufferData(GL_ARRAY_BUFFER, buffer.size() * sizeof(Point), buffer.data(), GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Point), (void*)0);

        glUniform1i(uniforms.sky_texture, render::Config::sky_texture - GL_TEXTURE0);
    }

    void Draw_sky::draw(const World& world, const std::chrono::milliseconds& ms) {
        glBindVertexArray(_vertex_array);
        glUseProgram(_prg.id());

        glUniform1f(uniforms.timestamp, float(ms.count()));

        glEnableVertexAttribArray(0);
        glDrawArrays(GL_TRIANGLES, 0, 2 * 3);
        glDisableVertexAttribArray(0);
    }
}
