#pragma once
#include "../../gl/handles.h"
#include "../../gl/shaders.h"
#include "../../gl/texture.h"
#include <chrono>

namespace amaze {
    class World;

    class Draw_intro {
      public:
        Draw_intro(Program);

        void setup();
        void draw(const World&, const std::chrono::milliseconds&);
      private:
        amaze::Program _prg;
        Vertex_array_h _vertex_array;
        Buffer_h _vertices;
        Texture2D<GL_RGB, GL_UNSIGNED_BYTE> bitmap;
        Texture2D<GL_RGB, GL_UNSIGNED_BYTE> noise;

        struct {
            GLint image_texture;
            GLint noise_texture;
            GLint timestamp;
        } uniforms;
    };
}

