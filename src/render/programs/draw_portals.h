#pragma once
#include "../../gl/handles.h"
#include "../../gl/shaders.h"
#include "../walls_texture.h"
#include <chrono>

namespace amaze {
    class World;

    class Draw_portals {
      public:
        Draw_portals(Program);

        void vertices(Portal_manager);
        void setup();
        void draw(const World&, const std::chrono::milliseconds&);
      private:
        amaze::Program _prg;
        Vertex_array_h _vertex_array;
        Portal_manager _vertices;

        struct {
            GLint mvp;
            GLint image_textures;
            GLint transform;
        } uniforms;
    };
}
