#include "walls_texture.h"
#include <stdexcept>

namespace amaze {
    Hint_textures::Hint_textures(GLenum tu, uint16_t s) : hint_size{s}, atlas_size{0}, texture_unit0{tu} {
        atlases.reserve(Hint_textures::max_textures);
    }

    void Hint_textures::add_atlas(const Image<rgb>& img) {
        if (atlases.size() >= Hint_textures::max_textures) {
            throw std::runtime_error{"hint textures full"};
        }

        if (img.width() != img.height()) {
            throw std::runtime_error{"the atlas must be square"};
        }

        if (img.width() % hint_size != 0) {
            throw std::runtime_error{"incompatible atlas size"};
        }

        if (atlases.size() == 0) {
            atlas_size = img.width();
        } else if (img.width() != atlas_size) {
            throw std::runtime_error{fmt::format("all the atlases must be of the same size: {}x{} != {}x{}",
                                                 img.width(),
                                                 img.height(),
                                                 atlas_size,
                                                 atlas_size)};
        }

        const GLenum tu = texture_unit0 + atlases.size();
        glActiveTexture(tu);
        texture t;
        t.load(img);
        atlases.push_back(std::move(t));
    }

    std::tuple<int, Tex_coord, Tex_coord> Hint_textures::pos(uint16_t index) const {
        const uint16_t hints_per_row   = atlas_size / hint_size;
        const uint16_t hints_per_atlas = hints_per_row * hints_per_row;
        const uint16_t atlas_index = index / hints_per_atlas;
        if (atlas_index >= atlases.size()) {
            throw std::runtime_error{fmt::format("hint index out of range: {}", index)};
        }

        index = index - atlas_index * hints_per_atlas;

        const uint16_t row = index / hints_per_row;
        const uint16_t col = index % hints_per_row;

        Tex_coord tl{float(col) / hints_per_row, float(row) / hints_per_row};
        Tex_coord rb{float(col + 1) / hints_per_row, float(row + 1) / hints_per_row};
        return std::make_tuple(atlas_index, tl, rb);
    }

    std::tuple<int, Tex_coord, Tex_coord> Hint_textures::pos(uint16_t index, float padding) const {
        int atlas_index;
        Tex_coord tl, rb;
        std::tie(atlas_index, tl, rb) = pos(index);

        const float offset = (rb.x - tl.x) * padding;
        tl.x += offset;
        tl.y += offset;
        rb.x -= offset;
        rb.y -= offset;

        return std::make_tuple(atlas_index, tl, rb);
    }

    Textured_triangle prepare(const Cube_triangle& t, const Hint_textures& hints) {
        const auto n = t.normal();

        Textured_triangle tt;
        tt.a.p = t.a;
        tt.a.n = n;
        tt.b.p = t.b;
        tt.b.n = n;
        tt.c.p = t.c;
        tt.c.n = n;
        hints.map(tt, t.hint);
        return tt;
    }

    Portal_textured_triangle prepare(const Portal_triangle& t, const Hint_textures& hints) {
        const auto n = t.normal();

        Portal_textured_triangle tt;
        tt.a.p           = t.a;
        tt.a.n           = n;
        tt.a.level       = t.level;
        tt.a.cube_center = t.cube_center;
        tt.b.p           = t.b;
        tt.b.n           = n;
        tt.b.level       = t.level;
        tt.b.cube_center = t.cube_center;
        tt.c.p           = t.c;
        tt.c.n           = n;
        tt.c.level       = t.level;
        tt.c.cube_center = t.cube_center;
        hints.map(tt, t.image);
        return tt;
    }

    void Tile_manager::load(const std::vector<Textured_triangle>& triangles) {
        _points = triangles.size() * 3;
        glBindBuffer(GL_ARRAY_BUFFER, vertex);
        glBufferData(
            GL_ARRAY_BUFFER, triangles.size() * sizeof(amaze::Textured_triangle), triangles.data(), GL_STATIC_DRAW);
    }

    int Tile_manager::points() const {
        return _points;
    }

    void Tile_manager::activate(attributes& attrs) const {
        glBindBuffer(GL_ARRAY_BUFFER, vertex);
        if (attrs.positions != attributes::not_set) {
            glVertexAttribPointer(attrs.positions, 3, GL_FLOAT, GL_FALSE, sizeof(amaze::Textured_point), (void*)0);
        }
        if (attrs.texture_coords != attributes::not_set) {
            glVertexAttribPointer(attrs.texture_coords,
                                  2,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  sizeof(amaze::Textured_point),
                                  (void*)(sizeof(amaze::Point)));
        }
        if (attrs.texture_id != attributes::not_set) {
            glVertexAttribIPointer(attrs.texture_id,
                                   1,
                                   GL_INT,
                                   sizeof(amaze::Textured_point),
                                   (void*)(sizeof(amaze::Point) + sizeof(amaze::Tex_coord)));
        }
        if (attrs.normals != attributes::not_set) {
            glVertexAttribPointer(attrs.normals,
                                  3,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  sizeof(amaze::Textured_point),
                                  (void*)(sizeof(amaze::Point) + sizeof(amaze::Tex_coord) + sizeof(GLint)));
        }
    };

    void Portal_manager::load(const std::vector<Portal_textured_triangle>& triangles) {
        _points = triangles.size() * 3;
        glBindBuffer(GL_ARRAY_BUFFER, vertex);
        glBufferData(
            GL_ARRAY_BUFFER, triangles.size() * sizeof(Portal_textured_triangle), triangles.data(), GL_STATIC_DRAW);
    }

    int Portal_manager::points() const {
        return _points;
    }

    void Portal_manager::activate(attributes& attrs) const {
        const GLsizei stride = sizeof(Portal_textured_triangle::P);

        glBindBuffer(GL_ARRAY_BUFFER, vertex);
        if (attrs.positions != attributes::not_set) {
            glVertexAttribPointer(attrs.positions, 3, GL_FLOAT, GL_FALSE, stride, (void*)0);
        }
        if (attrs.texture_coords != attributes::not_set) {
            glVertexAttribPointer(attrs.texture_coords, 2, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(Point)));
        }
        if (attrs.texture_id != attributes::not_set) {
            glVertexAttribIPointer(attrs.texture_id, 1, GL_INT, stride, (void*)(sizeof(Point) + sizeof(Tex_coord)));
        }
        if (attrs.levels != attributes::not_set) {
            glVertexAttribIPointer(
                attrs.levels,
                1,
                GL_INT,
                stride,
                (void*)(sizeof(Point) + sizeof(Tex_coord) + sizeof(GLint) + sizeof(Direction) + sizeof(Point)));
        }
        if (attrs.centers != attributes::not_set) {
            glVertexAttribPointer(attrs.centers,
                                  3,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  stride,
                                  (void*)(sizeof(Point) + sizeof(Tex_coord) + sizeof(GLint) + sizeof(Direction)));
        }
    };
}
