#include "main.h"

#include "level/level_parse.h"

#include "world/input_source.h"
#include "world/stream.h"
#include "world/updater.h"
#include "world/world.h"
#include "world/stream_adapters.h"

#include "render/config.h"
#include "render/programs.h"
#include "render/game_renderer.h"
#include "render/walls_texture.h"

#include "gl/context.h"
#include "glm/gtc/matrix_transform.hpp"

#include <format/format.h>
#include <fstream>
#include <string>
#include <random>
#include <iostream>

#include "net/thread.h"

#include "SDL2/SDL.h"

amaze::player_id_t random_id() {
    std::random_device rd;
    return static_cast<amaze::player_id_t>(rd());
}

bool file_exists(const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}

amaze::level::Level load_level(const std::string& fname) {
    std::ifstream f{fname};
    return amaze::level::parse(f);
}

std::vector<std::string> find_hints(const std::string& level_name) {
    auto atlas_name = [&level_name](uint16_t index) {
        size_t dot = level_name.find_last_of('.');
        return fmt::format("{}-{:03}.png", level_name.substr(0, dot), index, level_name.substr(dot));
    };

    std::vector<std::string> output;
    for (uint16_t index = 0; index < 1000; index++) {
        auto name = atlas_name(index);
        if (file_exists(name)) {
            output.push_back(std::move(name));
        } else {
            break;
        }
    }
    return output;
}

int main(int argc, char* argv[]) {

    if (SDL_Init(SDL_INIT_AUDIO) != 0) {
        fmt::print("Unable to initialize SDL audio:  %s\n", SDL_GetError());
        return 1;
    }

    // Init Simple-SDL2-Audio
    initAudio();

    auto cfg = parse_cmdline(argc, argv);
    if (!cfg) {
        return 2;
    }

    auto player_id = random_id();
    if (!cfg->replay.filename.empty() && cfg->replay.player) {
        player_id = cfg->replay.player;
    }
    fmt::print("I am {}\n", player_id);

    App_stream stream{*cfg, player_id};

    amaze::World world;
    amaze::Updater<> updater{world, player_id};

    if (!cfg->record.filename.empty()) {
        fmt::print("session recorded on {}\n", cfg->record.filename);

        // Push the local player start event
        stream.sink().push(amaze::Player_starts{});
    } else if (!cfg->replay.filename.empty()) {
        // Check if the specified replay file exists before committing to it
        if (file_exists(cfg->replay.filename)) {
            fmt::print("replay from {} ({}x)\n", cfg->replay.filename, cfg->replay.speedup);
        } else {
            fmt::print("unable to find file {}\n", cfg->replay.filename);
            return 3;
        }
    }

    const std::string level_file = "../levels/level1.txt";
    if (!file_exists(level_file)) {
        fmt::print("unable to read level {}\n", level_file);
        return 4;
    }

    fmt::print("loading level...\n");
    {
        std::ifstream f{level_file};
        world.level() = amaze::level::parse(f);
    }

    asio::io_service io;
    auto net_client = amaze::net::Client{
        io,                                                            //
        stream.sink(),                                                 //
        [&updater](const amaze::World_status& s) { updater.reset(s); } //
    };                                                                 //

    if (cfg->server) {
        net_client.join(player_id, *cfg->server);
    }
    auto net_thread = amaze::net::start_client_thread(io);
    net_thread.detach();

    using std::chrono::milliseconds;

    size_t intro_len_ms = soundLength("../assets/intro.wav");

    auto intro_timeout = asio::steady_timer(io, milliseconds{intro_len_ms});
    auto start_intro_timeout = [&intro_timeout, &stream]() {
        intro_timeout.async_wait([&stream](const std::error_code&) {
            // BEWARE, this function is called from another thread (the one who joined the
            // io_service)
            stream.sink().push(amaze::Intro_ends{});
        });
    };

    size_t outro1_len_ms = soundLength("../assets/outro_1.wav");
    size_t outro2_len_ms = soundLength("../assets/outro_2.wav");

    asio::steady_timer outro_timeout{io, milliseconds{std::max(outro1_len_ms, outro2_len_ms)}};
    auto start_outro_timeout = [&outro_timeout, &stream]() {
        outro_timeout.async_wait([&stream](const std::error_code&) {
            stream.sink().push(amaze::Outro_ends{});
        });
    };

    amaze::Input_source input{std::chrono::milliseconds{16}, stream.sink()};

    amaze::Context ctx(cfg->display.fullscreen);
    amaze::make_current(ctx);
    fmt::print("context: {}\n", ctx.name());

    fmt::print("loading shaders...\n");
    amaze::Draw_walls walls{program("basic")};
    amaze::Draw_portals portals{program("portals")};
    amaze::Draw_intro intro{program("intro")};

    fmt::print("build game renderer...\n");
    amaze::Hint_textures hints{amaze::render::Config::tile_hints_texture0, amaze::render::Config::hint_size};
    for (auto& fname : find_hints(level_file)) {
        fmt::print("loading atlas... {}\n", fname);
        hints.add_atlas(amaze::png::read<amaze::rgb>(fname));
    }
    amaze::Game_renderer renderer(world, std::move(hints), std::move(walls), std::move(portals), std::move(intro));

    world.camera().proj(glm::perspective(45.0f, ctx.aspect_ratio(), 0.1f, 10000.0f));

    if (cfg->replay.filename.empty()) {
        stream.sink().push(amaze::Intro_starts{});
        start_intro_timeout();
    }
    // this is the render function called for every frame.
    auto game_loop = [&updater, & source = stream.source(), &renderer, player_id, &net_client, &start_outro_timeout ]() {
        while (auto event = source.pull()) {
            updater.process(*event);
            if (amaze::event_player(*event) == player_id) {
                net_client.broadcast(*event);
            }
            if (amaze::event_code(*event) == 'Y') {
                start_outro_timeout();
            }
            if (amaze::event_code(*event) == 'Z') {
                return false;
            }
        }
        renderer.draw();
        return true;
    };

    draw_forever(ctx, game_loop, input);

    // End Simple-SDL2-Audio
    endAudio();
    SDL_Quit();
    return 0;
}
