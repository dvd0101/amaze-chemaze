#pragma once
#include <chrono>
#include <string>
#include <format/format.h>

namespace amaze {

    template <typename CLOCK>
    struct Time_track_base {
        using time_point = std::chrono::time_point<CLOCK>;

        Time_track_base() : start{CLOCK::now()} {}
        Time_track_base(std::string s) : message{s}, start{CLOCK::now()} {}
        ~Time_track_base() {
            auto elasped = std::chrono::duration_cast<std::chrono::microseconds>(CLOCK::now() - start);
            if (message.size() > 0) {
                fmt::print("{} ", message);
            }
            fmt::print("{} micro\n", elasped.count());
        }

        std::string message;
        time_point start;
    };

    using Time_track = Time_track_base<std::chrono::high_resolution_clock>;

    template <typename F, typename... TArgs>
    auto timeit(std::string msg, F& func, TArgs&&... args) {
        Time_track t{msg};
        return func(std::forward<TArgs>(args)...);
    }
}
