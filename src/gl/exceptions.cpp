#include "exceptions.h"

namespace amaze {
    Shader_build_error::Shader_build_error() : Shader_error{"shader creation error"} {}
    Program_build_error::Program_build_error() : Shader_error{"program creation error"} {}
    Shader_cannot_be_attached::Shader_cannot_be_attached() : Shader_error{""} {}
}
