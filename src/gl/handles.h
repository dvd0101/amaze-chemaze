#pragma once
#include "handle.h"

namespace amaze {
    struct Shader_h : Uint_h {
        Shader_h(GLenum);
        Shader_h(Shader_h&&);
        // OpenGL keeps a ref counting of the used shaders, so it is safe to
        // destroy it even if it is used by something else (like a program)
        ~Shader_h();
        Shader_h& operator=(Shader_h&&);
    };

    struct Program_h : Uint_h {
        Program_h();
        Program_h(Program_h&&);
        ~Program_h();
        Program_h& operator=(Program_h&&);
    };

    struct Texture_h : Uint_h {
        Texture_h();
        Texture_h(Texture_h&&);
        ~Texture_h();
        Texture_h& operator=(Texture_h&&);
    };

    struct Vertex_array_h : Uint_h {
        Vertex_array_h();
        Vertex_array_h(Vertex_array_h&&);
        ~Vertex_array_h();
        Vertex_array_h& operator=(Vertex_array_h&&);
    };

    struct Buffer_h : Uint_h {
        Buffer_h();
        Buffer_h(Buffer_h&&);
        ~Buffer_h();
        Buffer_h& operator=(Buffer_h&&);
    };

}
