#pragma once
#include "gl.h"
#include "handles.h"
#include <format/format.h>

namespace amaze {
    struct Texture_data {
        size_t width;
        size_t height;
        GLenum data_format;
        const uint8_t* data;
    };

    enum TextureMode { Data, Graphics };

    template <GLint InternalFormat, GLenum Type>
    class Texture2D {
      public:
        GLuint id() const {
            return handle;
        }

        void load(size_t width, size_t height, GLenum data_format, const void* data, TextureMode mode = Data) {
            // Make sure the texture is bound before uploading the texture.
            // Note: we still assume that the client code is selecting the
            // correct slot via glActiveTexture.
            glBindTexture(GL_TEXTURE_2D, handle);
            glTexImage2D(GL_TEXTURE_2D, 0, InternalFormat, width, height, 0, data_format, Type, data);
            if (mode == Data) {
                // Nearest filtering for data access
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            } else {
                // Nice trilinear filtering with mipmapping
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glGenerateMipmap(GL_TEXTURE_2D);
            }
        }

        void load(const Texture_data& t, TextureMode mode = Data) {
            load(t.width, t.height, t.data_format, t.data, mode);
        }

      private:
        Texture_h handle;
    };

    enum class TextureCube_faces {
        right  = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
        left   = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        top    = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
        bottom = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        back   = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
        front  = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };

    template <GLint InternalFormat>
    class TextureCube {
      public:
        GLuint id() const {
            return handle;
        }

        void load(TextureCube_faces face, size_t width, size_t height, GLenum data_format, const void* data) {
            // Make sure the texture is bound before uploading the texture.
            // Note: we still assume that the client code is selecting the
            // correct slot via glActiveTexture.
            glBindTexture(GL_TEXTURE_CUBE_MAP, handle);
            glTexImage2D(
                static_cast<GLenum>(face), 0, InternalFormat, width, height, 0, data_format, GL_UNSIGNED_BYTE, data);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        }

        void load(TextureCube_faces face, const Texture_data& t) {
            load(face, t.width, t.height, t.data_format, t.data);
        }

      private:
        Texture_h handle;
    };
}
