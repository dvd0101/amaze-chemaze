#include "../context.h"
#include <cstdlib>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <format/format.h>
#include "../../world/input_source.h"

namespace amaze {

    namespace {
        struct Glfw_subsystem {
            Glfw_subsystem() {
                fmt::print("Trying to initialize GLFW: {}...", glfwGetVersionString());
                if (!glfwInit()) {
                    fmt::print("failed\n");
                    exit(1);
                }
                fmt::print("done\n");
            }

            ~Glfw_subsystem() {
                fmt::print("terminating GLFW\n");
                glfwTerminate();
            }
        };
    }

    static Glfw_subsystem glfw_handle;

    Context::Context(bool fullscreen) : Context::Context(640, 480, fullscreen) {
    }

    Context::Context(int width, int height, bool fullscreen) : handle{nullptr} {
        glfwWindowHint(GLFW_SAMPLES, 4);
#ifndef __APPLE__
        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
#else
        // Creating an OpenGL ES context on desktop seems to be a Mesa-only
        // feature. On OS X we select the closest match, OpenGL desktop 4.1,
        // which is the best available on 10.10; note, the GL desktop version
        // offering feature parity with ES 3.0 would be 4.3 .
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
        int w = width;
        int h = height;
        GLFWmonitor *monitor = NULL;
        if (fullscreen) {
            monitor = glfwGetPrimaryMonitor();
            const GLFWvidmode* mode = glfwGetVideoMode(monitor);

            w = mode->width;
            h = mode->height;

            glfwWindowHint(GLFW_RED_BITS, mode->redBits);
            glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
            glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
            glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
        }
        GLFWwindow* window = glfwCreateWindow(w, h, "Amaze", monitor, NULL);
        if (!window) {
            throw Context_build_error{};
        }
        window_width = w;
        window_height = h;
        handle = reinterpret_cast<Context_native_handle*>(window);
        make_current(*this);
        gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    };

    Context::~Context() {
        auto window = reinterpret_cast<GLFWwindow*>(handle);
        glfwDestroyWindow(window);
    }

    std::string Context::name() const {
        return reinterpret_cast<const char*>(glGetString(GL_VERSION));
    }

    Context_native_handle* Context::native_handle() {
        return handle;
    }

    float Context::aspect_ratio() const {
        return window_width / (float)window_height;
    }

    void make_current(Context& ctx) {
        auto window = reinterpret_cast<GLFWwindow*>(ctx.native_handle());
        glfwMakeContextCurrent(window);
    }

    namespace {
        struct Debug_message {
            GLenum source_;
            GLenum type_;
            GLuint id_;
            GLenum severity_;

            char const* source() const {
                switch (source_) {
                case GL_DEBUG_SOURCE_API:
                    return "GL_DEBUG_SOURCE_API";
                case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
                    return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
                case GL_DEBUG_SOURCE_SHADER_COMPILER:
                    return "GL_DEBUG_SOURCE_SHADER_COMPILER";
                case GL_DEBUG_SOURCE_THIRD_PARTY:
                    return "GL_DEBUG_SOURCE_THIRD_PARTY";
                case GL_DEBUG_SOURCE_APPLICATION:
                    return "GL_DEBUG_SOURCE_APPLICATION";
                case GL_DEBUG_SOURCE_OTHER:
                    return "GL_DEBUG_SOURCE_OTHER";
                default:
                    return "Unknown";
                }
            }

            char const* type() const {
                switch (type_) {
                case GL_DEBUG_TYPE_ERROR:
                    return "GL_DEBUG_TYPE_ERROR";
                case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
                    return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
                case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
                    return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
                case GL_DEBUG_TYPE_PORTABILITY:
                    return "GL_DEBUG_TYPE_PORTABILITY";
                case GL_DEBUG_TYPE_PERFORMANCE:
                    return "GL_DEBUG_TYPE_PERFORMANCE";
                case GL_DEBUG_TYPE_MARKER:
                    return "GL_DEBUG_TYPE_MARKER";
                case GL_DEBUG_TYPE_PUSH_GROUP:
                    return "GL_DEBUG_TYPE_PUSH_GROUP";
                case GL_DEBUG_TYPE_POP_GROUP:
                    return "GL_DEBUG_TYPE_POP_GROUP";
                case GL_DEBUG_TYPE_OTHER:
                    return "GL_DEBUG_TYPE_OTHER";
                default:
                    return "Unknown";
                }
            }

            char const* severity() const {
                switch (severity_) {
                case GL_DEBUG_SEVERITY_HIGH:
                    return "GL_DEBUG_SEVERITY_HIGH";
                case GL_DEBUG_SEVERITY_MEDIUM:
                    return "GL_DEBUG_SEVERITY_MEDIUM";
                case GL_DEBUG_SEVERITY_LOW:
                    return "GL_DEBUG_SEVERITY_LOW";
                case GL_DEBUG_SEVERITY_NOTIFICATION:
                    return "GL_DEBUG_SEVERITY_NOTIFICATION";
                default:
                    return "Unknown";
                }
            }
        };

        std::ostream& operator<<(std::ostream& os, Debug_message const& msg) {
            os << "source=" << msg.source() << " type=" << msg.type() << " id=" << msg.id_
               << " severity=" << msg.severity();
            return os;
        }

        void context_debug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const GLchar* message,
                           const void*) {
            Debug_message msg{source, type, id, severity};
            fmt::print("{} - {}\n", msg, message);
        }
    }

    namespace {
        const double pi = std::acos(-1);

        struct Mouse_tracker {
            // arbitraty conversion from pixels to a rads
            const double pixel_to_rad = pi / 800;

            Mouse_tracker(GLFWwindow* w) : window{w} {
                glfwGetCursorPos(window, &x0, &y0);
            }

            void poll() {
                glfwGetCursorPos(window, &x, &y);
            }

            void reset() {
                x0 = x;
                y0 = y;
            }

            double delta_phi() const {
                return (x - x0) * pixel_to_rad;
            }

            double delta_theta() const {
                return (y0 - y) * pixel_to_rad;
            }

            GLFWwindow* window;
            double x0, y0;
            double x, y;
        };

        struct Handle_input {
            Handle_input(GLFWwindow* w, Input_source& i) : window{w}, input{i}, mouse{w} {}

            void poll() {
                mouse.poll();
                if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
                    input.move_forward();
                }
                if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
                    input.move_right();
                }
                if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
                    input.move_backward();
                }
                if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
                    input.move_left();
                }
                if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
                    input.fire_pressed();
                }
                input.change_direction(
                    static_cast<float>(mouse.delta_phi()),
                    static_cast<float>(mouse.delta_theta()));
                if (input.touch()) {
                    mouse.reset();
                }
            }

            GLFWwindow* window;
            Input_source& input;
            Mouse_tracker mouse;
        };
    }

    void draw_forever(Context& ctx, render_function draw, Input_source& input_source) {
        auto window = reinterpret_cast<GLFWwindow*>(ctx.native_handle());
        make_current(ctx);

        if (glDebugMessageCallback != nullptr) {
            glEnable(GL_DEBUG_OUTPUT);
            glDebugMessageCallback(context_debug, nullptr);
        }

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        Handle_input input{window, input_source};

        glfwSwapInterval(1);
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();
            if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
                glfwSetWindowShouldClose(window, GL_TRUE);
            }
            input.poll();

            if (!draw()) {
                break;
            }
            glfwSwapBuffers(window);
        }
    }
}
