#pragma once
#include <string>
#include <functional>
#include <stdexcept>

namespace amaze {
    class Context_native_handle;

    class Context_error : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    class Context_build_error : public Context_error {
      public:
        Context_build_error();
    };

    class Context {
      public:
        Context(bool);
        Context(int, int, bool);
        Context(Context const&) = delete;
        ~Context();

        std::string name() const;
        Context_native_handle* native_handle();

        float aspect_ratio() const;
        int width() {
            return window_width;
        }
        int height() {
            return window_height;
        }

      private:
        Context_native_handle* handle;
        int window_width;
        int window_height;
    };

    void make_current(Context&);

    class Input_source;

    using render_function = std::function<bool(void)>;

    // draw_forever starts the main draw loop. The render_function is
    // repeatedly called to render every frame, user inputs are pushed through
    // the `Input_source`
    void draw_forever(Context&, render_function, Input_source&);
};
