#pragma once
#include "gl.h"

namespace amaze {
    template <typename T>
    class Handle_ {
        /*
         * Base class for non-copyable handle (unique owner) of a GL resource.
         *
         * `T` is the concrete type expected by the GL api.
         */
      public:
        Handle_() : value{} {};
        Handle_(T v) : value{v} {};
        Handle_(const Handle_<T>&) = delete;
        Handle_(Handle_<T>&& other) : value{other.value} {
            other.value = T{};
        }
        ~Handle_() = default;

        Handle_& operator=(const Handle_<T>&) = delete;
        Handle_& operator=(Handle_<T>&& other) {
            value        = other.value;
            other.value  = T{};
            return *this;
        }

        operator T() const {
            return value;
        }

        T id() const {
            return value;
        }

      protected:
        T value;
    };

    using Int_h  = Handle_<GLint>;
    using Uint_h = Handle_<GLuint>;
}
