#pragma once
#include "gl.h"
#include "handles.h"
#include "exceptions.h"
#include <istream>
#include <vector>
#include <string>

namespace amaze {
    class Shader {
      public:
        Shader(GLenum);
        void add_source(std::string);
        void add_source(std::istream const&);

      public:
        GLuint id() const;
        bool compile();
        std::string compilation_log();

        // returns the source stored in the shader (available after the
        // compilation)
        std::string source();

      private:
        // these are the sources not yet compiled, `compile()` calls
        // `glShaderSource` that make a copy of the sources
        std::vector<std::string> temp_sources;
        Shader_h handle;
    };

    class Vertex_shader : public Shader {
      public:
        Vertex_shader() : Shader{GL_VERTEX_SHADER} {};
    };

    class Fragment_shader : public Shader {
      public:
        Fragment_shader() : Shader{GL_FRAGMENT_SHADER} {};
    };

    class Program {
      public:
        GLuint id() const;
        template <typename Shader>
        void attach(Shader const& s) {
            glAttachShader(handle, s.id());
            if (glGetError() == GL_INVALID_OPERATION) {
                throw Shader_cannot_be_attached();
            }
        }
        // after the link, successful or not, all the shaders are detached.
        bool link();

        std::string link_log();

      private:
        std::vector<GLuint> attached_shaders();

      private:
        Program_h handle;
    };

    // helper function that create, compile and link a program from the two given
    // file names (a vertex and a fragment shader)
    Program build(std::string const&, std::string const&);
}
