#include "shaders.h"
#include <sstream>
#include <fstream>

namespace amaze {
    namespace {
        std::string read_stream(std::istream const& in) {
            std::stringstream stream;
            stream << in.rdbuf();
            return stream.str();
        }
    }

    Shader::Shader(GLenum type) : handle{type} {}

    void Shader::add_source(std::string src) {
#if defined(__APPLE__)
        src = "#version 410\n" + src;
#else
        src = "#version 300 es\n" + src;
#endif
        temp_sources.push_back(std::move(src));
    }

    void Shader::add_source(std::istream const& src) {
        std::string source(read_stream(src));
#if defined(__APPLE__)
        source = "#version 410\n" + source;
#else
        source = "#version 300 es\n" + source;
#endif
        temp_sources.push_back(source);
    }

    GLuint Shader::id() const {
        return handle;
    }

    bool Shader::compile() {
        std::vector<char const*> ptrs(temp_sources.size());
        for (size_t ix = 0; ix < temp_sources.size(); ix++) {
            ptrs[ix] = temp_sources[ix].c_str();
        }
        glShaderSource(handle, ptrs.size(), ptrs.data(), nullptr);
        // safe to clear, glShaderSource make a copy of the sources
        temp_sources.clear();

        glCompileShader(handle);
        GLint result;
        glGetShaderiv(handle, GL_COMPILE_STATUS, &result);
        return result == GL_TRUE;
    }

    std::string Shader::source() {
        GLint result;
        glGetShaderiv(handle, GL_SHADER_SOURCE_LENGTH, &result);

        std::string buffer;
        if (result > 0) {
            buffer.resize(result);
            glGetShaderSource(handle, buffer.size(), nullptr, &buffer[0]);
        }
        return buffer;
    }

    std::string Shader::compilation_log() {
        GLint result;
        glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &result);

        std::string buffer;
        if (result > 0) {
            buffer.resize(result);
            glGetShaderInfoLog(handle, buffer.size(), nullptr, &buffer[0]);
        }
        return buffer;
    }

    GLuint Program::id() const {
        return handle;
    }

    bool Program::link() {
        glLinkProgram(handle);
        GLint result;
        glGetProgramiv(handle, GL_LINK_STATUS, &result);

        auto shaders = attached_shaders();
        for (auto shader_id : shaders) {
            glDetachShader(handle, shader_id);
        }
        return result == GL_TRUE;
    }

    std::string Program::link_log() {
        GLint result;
        glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &result);

        std::string buffer;
        if (result > 0) {
            buffer.resize(result);
            glGetProgramInfoLog(handle, buffer.size(), nullptr, &buffer[0]);
        }
        return buffer;
    }

    std::vector<GLuint> Program::attached_shaders() {
        GLint result;
        glGetProgramiv(handle, GL_ATTACHED_SHADERS, &result);

        std::vector<GLuint> buffer;
        if (result > 0) {
            buffer.resize(result);
            glGetAttachedShaders(handle, buffer.size(), nullptr, &buffer[0]);
        }
        return buffer;
    }

    namespace {
        template <typename Shader>
        Shader compile(std::istream& stream) {
            Shader sh;
            sh.add_source(stream);
            if (!sh.compile()) {
                throw Compile_error{sh.compilation_log()};
            }
            return sh;
        }
    }

    Program build(std::istream& vertex_stream, std::istream& fragment_stream) {
        auto vertex   = compile<Vertex_shader>(vertex_stream);
        auto fragment = compile<Fragment_shader>(fragment_stream);
        Program p;
        p.attach(vertex);
        p.attach(fragment);
        if (!p.link()) {
            throw Link_error{p.link_log()};
        }
        return p;
    }

    Program build(std::string const& vertex_file, std::string const& fragment_file) {
        std::ifstream vertex_stream{vertex_file};
        if (vertex_stream.fail()) {
            throw File_not_found(vertex_file);
        }

        std::ifstream fragment_stream{fragment_file};
        if (fragment_stream.fail()) {
            throw File_not_found(fragment_file);
        }

        return build(vertex_stream, fragment_stream);
    }
}
