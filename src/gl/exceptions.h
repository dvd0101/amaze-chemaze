#pragma once
#include <stdexcept>

namespace amaze {
    class Shader_error : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    class Shader_build_error : public Shader_error {
      public:
        Shader_build_error();
    };

    class Program_build_error : public Shader_error {
      public:
        Program_build_error();
    };

    class Shader_cannot_be_attached : public Shader_error {
      public:
        Shader_cannot_be_attached();
    };

    class Compile_error : public Shader_error {
        using Shader_error::Shader_error;
    };

    class Link_error : public Shader_error {
        using Shader_error::Shader_error;
    };

    class File_not_found : public Shader_error {
        using Shader_error::Shader_error;
    };
}
