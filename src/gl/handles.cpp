#include "handles.h"
#include "exceptions.h"

namespace amaze {
    Shader_h::Shader_h(GLenum type) : Uint_h{glCreateShader(type)} {
        if (id() == 0) {
            throw Shader_build_error{};
        }
    }
    Shader_h::Shader_h(Shader_h&&) = default;
    Shader_h::~Shader_h() {
        if (id() != 0) {
            glDeleteShader(id());
        }
    }
    Shader_h& Shader_h::operator=(Shader_h&&) = default;

    Program_h::Program_h() : Uint_h{glCreateProgram()} {
        if (id() == 0) {
            throw Program_build_error();
        }
    }
    Program_h::Program_h(Program_h&&) = default;
    Program_h::~Program_h() {
        if (id() != 0) {
            glDeleteProgram(id());
        }
    }
    Program_h& Program_h::operator=(Program_h&&) = default;

    Texture_h::Texture_h() {
        glGenTextures(1, &value);
        if (value == 0) {
            throw std::runtime_error{"glGenTextures error"};
        }
    }
    Texture_h::Texture_h(Texture_h&&) = default;
    Texture_h::~Texture_h() {
        if (id() != 0) {
            glDeleteTextures(1, &value);
        }
    }
    Texture_h& Texture_h::operator=(Texture_h&&) = default;

    Vertex_array_h::Vertex_array_h() {
        glGenVertexArrays(1, &value);
        if (value == 0) {
            throw std::runtime_error{"glGenVertexArrays error"};
        }
    }
    Vertex_array_h::Vertex_array_h(Vertex_array_h&&) = default;
    Vertex_array_h::~Vertex_array_h() {
        if (id() != 0) {
            glDeleteVertexArrays(1, &value);
        }
    }
    Vertex_array_h& Vertex_array_h::operator=(Vertex_array_h&&) = default;

    Buffer_h::Buffer_h() {
        glGenBuffers(1, &value);
        if (value == 0) {
            throw std::runtime_error{"glGenBuffers error"};
        }
    }
    Buffer_h::Buffer_h(Buffer_h&&) = default;
    Buffer_h::~Buffer_h() {
        if (id() != 0) {
            glDeleteBuffers(1, &value);
        }
    }
    Buffer_h& Buffer_h::operator=(Buffer_h&&) = default;
}
