#include "catch/catch.hpp"
#include "level.h"
#include "builder.h"
#include <limits>
#include <boost/variant/get.hpp>

using namespace amaze::level;
struct M : amaze::World_config {
    static constexpr float tile_size = 0.5;
};

SCENARIO("A level looks like a sparse map", "[level]") {
    GIVEN("an empty level") {
        Level lvl;

        REQUIRE(lvl.cols() == 0);
        REQUIRE(lvl.rows() == 0);
        REQUIRE(lvl.stats().tiles() == 0);

        WHEN("a tile is added to an arbitrary position") {
            lvl.add(Wall{}, {5, 1});

            THEN("the level is resized to accommodate it") {
                REQUIRE(lvl.cols() == 6);
                REQUIRE(lvl.rows() == 2);
                REQUIRE(lvl.stats().tiles() == 12);

                THEN("the tiles added to accommodate the new one are instances of Level::default_tile") {
                    for (auto& it : lvl.iterate()) {
                        auto& tile  = std::get<0>(it);
                        auto& coord = std::get<1>(it);
                        if (coord.x == 5 && coord.y == 1) {
                            REQUIRE(boost::get<Wall>(&tile) != nullptr);
                        } else {
                            REQUIRE(boost::get<Level::default_tile>(&tile) != nullptr);
                        }
                    };
                    REQUIRE(lvl.stats().floors() == 11);
                    REQUIRE(lvl.stats().walls() == 1);
                }
            }

            WHEN("another tile is added to a position already inside the level") {
                lvl.add(Wall{}, {0, 0});

                THEN("the level size does not change") {
                    REQUIRE(lvl.cols() == 6);
                    REQUIRE(lvl.rows() == 2);
                    REQUIRE(lvl.stats().tiles() == 12);
                    REQUIRE(lvl.stats().floors() == 10);
                    REQUIRE(lvl.stats().walls() == 2);
                }
            }
        }
    }
}

SCENARIO("A level can be iterated", "[level]") {
    GIVEN("a level 3x3") {
        Level lvl;
        lvl.add(Wall{}, {2, 2});

        REQUIRE(lvl.cols() == 3);
        REQUIRE(lvl.rows() == 3);

        WHEN("the level is iterated") {
            THEN("the tiles are returned in a row/column order") {
                uint16_t x = 0;
                uint16_t y = 0;
                for (auto& it : lvl.iterate()) {
                    auto& coord = std::get<1>(it);
                    REQUIRE(coord.x == x);
                    REQUIRE(coord.y == y);
                    if (x == 2) {
                        x = 0;
                        y++;
                    } else {
                        x++;
                    }
                }
            };
        }

        WHEN("the level is block iterated") {
            THEN("the tiles are returned surrounded with the adjacent tiles") {
                uint16_t x = 0;
                uint16_t y = 0;
                for (auto& it : lvl.block_iterate()) {
                    auto& block = std::get<0>(it);
                    auto& coord = std::get<1>(it);
                    REQUIRE(coord.x == x);
                    REQUIRE(coord.y == y);

                    if (y == 0) {
                        REQUIRE(!block.nw);
                        REQUIRE(!block.n);
                        REQUIRE(!block.ne);
                    } else if (y == 1) {
                        REQUIRE(block.n);
                        REQUIRE(block.s);
                    } else if (y == 2) {
                        REQUIRE(!block.sw);
                        REQUIRE(!block.s);
                        REQUIRE(!block.se);
                    }

                    if (x == 0) {
                        REQUIRE(!block.nw);
                        REQUIRE(!block.w);
                        REQUIRE(!block.sw);
                    } else if (x == 1) {
                        REQUIRE(block.w);
                        REQUIRE(block.e);
                    } else if (x == 2) {
                        REQUIRE(!block.ne);
                        REQUIRE(!block.e);
                        REQUIRE(!block.se);
                    }

                    if (y == 1 && x == 1) {
                        REQUIRE(block.nw);
                        REQUIRE(block.n);
                        REQUIRE(block.ne);
                        REQUIRE(block.w);
                        REQUIRE(block.e);
                        REQUIRE(block.sw);
                        REQUIRE(block.s);
                        REQUIRE(block.se);
                    }

                    if (x == 2) {
                        x = 0;
                        y++;
                    } else {
                        x++;
                    }
                }
            }
        }
    }
}

SCENARIO("A cube can be constructed", "[level]") {
    GIVEN("A World_measure that defines the size of a tile") {

        WHEN("the logical coordinates are 0,0,0") {
            std::vector<amaze::Cube_triangle> output;
            details::place_cube<M>({0, 0, 0}, {}, 0, std::back_inserter(output));
            REQUIRE(output.size() == 12);

            const int bottom_face = 0;
            const int top_face    = 2;
            const int left_face   = 4;
            const int right_face  = 6;
            const int rear_face   = 8;
            const int front_face  = 10;

            THEN("the resulting cube is placed with the bottom-left angle of the rear faces at the zero") {
                REQUIRE(output[bottom_face + 0].face == amaze::cube_face::bottom);
                REQUIRE(output[bottom_face + 0].a == amaze::Point(0.0, 0.0, 0.0));
                REQUIRE(output[bottom_face + 0].b == amaze::Point(0.0, 0.5, 0.0));
                REQUIRE(output[bottom_face + 0].c == amaze::Point(0.5, 0.0, 0.0));
                REQUIRE(output[bottom_face + 1].face == amaze::cube_face::bottom);
                REQUIRE(output[bottom_face + 1].a == amaze::Point(0.5, 0.5, 0.0));
                REQUIRE(output[bottom_face + 1].b == amaze::Point(0.5, 0.0, 0.0));
                REQUIRE(output[bottom_face + 1].c == amaze::Point(0.0, 0.5, 0.0));

                REQUIRE(output[top_face + 0].face == amaze::cube_face::top);
                REQUIRE(output[top_face + 0].a == amaze::Point(0.0, 0.0, 0.5));
                REQUIRE(output[top_face + 0].b == amaze::Point(0.5, 0.0, 0.5));
                REQUIRE(output[top_face + 0].c == amaze::Point(0.0, 0.5, 0.5));
                REQUIRE(output[top_face + 1].face == amaze::cube_face::top);
                REQUIRE(output[top_face + 1].a == amaze::Point(0.5, 0.5, 0.5));
                REQUIRE(output[top_face + 1].b == amaze::Point(0.0, 0.5, 0.5));
                REQUIRE(output[top_face + 1].c == amaze::Point(0.5, 0.0, 0.5));

                REQUIRE(output[left_face + 0].face == amaze::cube_face::left);
                REQUIRE(output[left_face + 0].a == amaze::Point(0.0, 0.0, 0.0));
                REQUIRE(output[left_face + 0].b == amaze::Point(0.0, 0.0, 0.5));
                REQUIRE(output[left_face + 0].c == amaze::Point(0.0, 0.5, 0.0));
                REQUIRE(output[left_face + 1].face == amaze::cube_face::left);
                REQUIRE(output[left_face + 1].a == amaze::Point(0.0, 0.5, 0.5));
                REQUIRE(output[left_face + 1].b == amaze::Point(0.0, 0.5, 0.0));
                REQUIRE(output[left_face + 1].c == amaze::Point(0.0, 0.0, 0.5));

                REQUIRE(output[right_face + 0].face == amaze::cube_face::right);
                REQUIRE(output[right_face + 0].a == amaze::Point(0.5, 0.0, 0.0));
                REQUIRE(output[right_face + 0].b == amaze::Point(0.5, 0.5, 0.0));
                REQUIRE(output[right_face + 0].c == amaze::Point(0.5, 0.0, 0.5));
                REQUIRE(output[right_face + 1].face == amaze::cube_face::right);
                REQUIRE(output[right_face + 1].a == amaze::Point(0.5, 0.5, 0.5));
                REQUIRE(output[right_face + 1].b == amaze::Point(0.5, 0.0, 0.5));
                REQUIRE(output[right_face + 1].c == amaze::Point(0.5, 0.5, 0.0));

                REQUIRE(output[rear_face + 0].face == amaze::cube_face::rear);
                REQUIRE(output[rear_face + 0].a == amaze::Point(0.0, 0.0, 0.0));
                REQUIRE(output[rear_face + 0].b == amaze::Point(0.5, 0.0, 0.0));
                REQUIRE(output[rear_face + 0].c == amaze::Point(0.0, 0.0, 0.5));
                REQUIRE(output[rear_face + 1].face == amaze::cube_face::rear);
                REQUIRE(output[rear_face + 1].a == amaze::Point(0.5, 0.0, 0.5));
                REQUIRE(output[rear_face + 1].b == amaze::Point(0.0, 0.0, 0.5));
                REQUIRE(output[rear_face + 1].c == amaze::Point(0.5, 0.0, 0.0));

                REQUIRE(output[front_face + 0].face == amaze::cube_face::front);
                REQUIRE(output[front_face + 0].a == amaze::Point(0.0, 0.5, 0.0));
                REQUIRE(output[front_face + 0].b == amaze::Point(0.0, 0.5, 0.5));
                REQUIRE(output[front_face + 0].c == amaze::Point(0.5, 0.5, 0.0));
                REQUIRE(output[front_face + 1].face == amaze::cube_face::front);
                REQUIRE(output[front_face + 1].a == amaze::Point(0.5, 0.5, 0.5));
                REQUIRE(output[front_face + 1].b == amaze::Point(0.5, 0.5, 0.0));
                REQUIRE(output[front_face + 1].c == amaze::Point(0.0, 0.5, 0.5));
            }

            THEN("The normals of the triangles point toward the outside of the cube") {
                REQUIRE(output[bottom_face + 0].normal() == amaze::Direction(0, 0, -1));
                REQUIRE(output[bottom_face + 1].normal() == amaze::Direction(0, 0, -1));

                REQUIRE(output[top_face + 0].normal() == amaze::Direction(0, 0, 1));
                REQUIRE(output[top_face + 1].normal() == amaze::Direction(0, 0, 1));

                REQUIRE(output[left_face + 0].normal() == amaze::Direction(-1, 0, 0));
                REQUIRE(output[left_face + 1].normal() == amaze::Direction(-1, 0, 0));

                REQUIRE(output[right_face + 0].normal() == amaze::Direction(1, 0, 0));
                REQUIRE(output[right_face + 1].normal() == amaze::Direction(1, 0, 0));

                REQUIRE(output[rear_face + 0].normal() == amaze::Direction(0, -1, 0));
                REQUIRE(output[rear_face + 1].normal() == amaze::Direction(0, -1, 0));

                REQUIRE(output[front_face + 0].normal() == amaze::Direction(0, 1, 0));
                REQUIRE(output[front_face + 1].normal() == amaze::Direction(0, 1, 0));
            }
        }
    }
}

/*
 * utility class that records the minimum and maximum `z` of the cubes placed
 * in a (2d) tile
 */
template <typename MEASURE>
struct Z_monitor {
    Z_monitor(amaze::optional<uint16_t> c, amaze::optional<uint16_t> r) : col{c}, row{r}, counter{0} {
        const float size = MEASURE::tile_size;
        if (col) {
            col_limits = std::make_tuple(size * (*c), size * (*c + 1));
        }
        if (row) {
            row_limits = std::make_tuple(size * (*r), size * (*r + 1));
        }
    }

    bool watch(const amaze::Cube_triangle& t) {
        using std::get;
        float min_x = std::min({t.a.x, t.b.x, t.c.x});
        float max_x = std::max({t.a.x, t.b.x, t.c.x});
        float min_y = std::min({t.a.y, t.b.y, t.c.y});
        float max_y = std::max({t.a.y, t.b.y, t.c.y});
        bool hit    = true;
        // gross test, just check if the triangle is inside our tile
        if (col) {
            bool in_col = min_x >= std::get<0>(col_limits) && max_x <= std::get<1>(col_limits);
            hit &= in_col;
        }
        if (row) {
            bool in_row = min_y >= std::get<0>(row_limits) && max_y <= std::get<1>(row_limits);
            hit &= in_row;
        }
        if (hit) {
            const auto n = t.normal();
            switch (t.face) {
            case amaze::cube_face::left:
                hit = min_x == get<0>(col_limits) && n.x < 0;
                break;
            case amaze::cube_face::right:
                hit = max_x == get<1>(col_limits) && n.x > 0;
                break;
            case amaze::cube_face::rear:
                hit = min_y == get<0>(row_limits) && n.y < 0;
                break;
            case amaze::cube_face::front:
                hit = max_y == get<1>(row_limits) && n.y > 0;
                break;
            default:
                break;
            }
        }
        if (hit) {
            counter++;
            max_z = std::max({max_z, t.a.z, t.b.z, t.c.z});
            min_z = std::min({min_z, t.a.z, t.b.z, t.c.z});
        }
        return hit;
    }

    amaze::optional<uint16_t> col;
    amaze::optional<uint16_t> row;

    std::tuple<float, float> col_limits;
    std::tuple<float, float> row_limits;

    uint32_t counter;
    float max_z = std::numeric_limits<float>::lowest();
    float min_z = std::numeric_limits<float>::max();
};

SCENARIO("A floor can be constructed", "[level]") {
    GIVEN("A World_measure that defines the size of a tile") {
        const float tile_size = M::tile_size;

        WHEN("the tile is placed at 0") {
            Tile_block block{Floor{}};
            std::vector<amaze::Cube_triangle> output;
            details::place_floor<M>(block, {0, 0}, std::back_inserter(output));

            THEN("the resulting cube is placed beneath the zero-level") {
                Z_monitor<M> mon(0, 0);
                for (auto& t : output) {
                    mon.watch(t);
                }
                REQUIRE(mon.max_z == 0.0f);
                REQUIRE(mon.min_z == -tile_size);
            }
        }
    }
}

SCENARIO("A wall can be constructed", "[level]") {
    GIVEN("A World_measure that defines the size of a tile") {
        const float tile_size = M::tile_size;

        WHEN("the tile is placed at 0,0") {
            Tile_block block{Wall{}};
            std::vector<amaze::Cube_triangle> output;
            details::place_wall<M>(block, {0, 0}, std::back_inserter(output));

            THEN("the resulting cube is placed on the zero-level") {
                Z_monitor<M> mon(0, 0);
                for (auto& t : output) {
                    mon.watch(t);
                }
                REQUIRE(mon.max_z == tile_size);
                REQUIRE(mon.min_z == 0.0f);
            }
        }
    }
}

SCENARIO("A pit can be constructed", "[level]") {
    GIVEN("A World_measure that defines the size of a tile and the depth of a pit") {
        const float tile_size   = M::tile_size;
        const uint8_t pit_depth = M::pit_depth;

        WHEN("the tile is placed at 0,0") {
            Tile_block block{Pit{}};
            std::vector<amaze::Cube_triangle> output;
            details::place_pit<M>(block, {0, 0}, std::back_inserter(output));

            THEN("the resulting cube is 3 cubes below the zero-level") {
                Z_monitor<M> mon(0, 0);
                for (auto& t : output) {
                    mon.watch(t);
                }
                REQUIRE(mon.max_z == -(pit_depth - 1) * tile_size);
                REQUIRE(mon.min_z == -(pit_depth)*tile_size);
            }
        }
        WHEN("the tile is placed at 1,0 with a wall at 0,0") {
            Tile_block block{Pit{}};
            block.w = Tile{Wall{}};

            std::vector<amaze::Cube_triangle> output;
            details::place_pit<M>(block, {1, 0}, std::back_inserter(output));

            THEN("the column under the wall is filled with cubes") {
                REQUIRE(output.size() == 12 * 4);
                Z_monitor<M> wall_mon(0, 0);
                Z_monitor<M> pit_mon(1, 0);

                for (auto& t : output) {
                    wall_mon.watch(t);
                    pit_mon.watch(t);
                }
                REQUIRE(wall_mon.max_z == 0);
                REQUIRE(wall_mon.min_z == -(pit_depth)*tile_size);
                REQUIRE(pit_mon.max_z == -(pit_depth - 1) * tile_size);
                REQUIRE(pit_mon.min_z == -(pit_depth)*tile_size);
            }
        }
        WHEN("two pits are adjacent") {
            Tile_block block{Pit{}};
            block.w = Tile{Wall{}};
            block.e = Tile{Pit{}};

            std::vector<amaze::Cube_triangle> output;
            details::place_pit<M>(block, {1, 0}, std::back_inserter(output));

            THEN("the column over the pits are not filled with cubes") {
                REQUIRE(output.size() == 12 * 4);
                Z_monitor<M> wall_mon(0, 0);
                Z_monitor<M> pit1_mon(1, 0);
                Z_monitor<M> pit2_mon(2, 0);

                for (auto& t : output) {
                    wall_mon.watch(t);
                    pit1_mon.watch(t);
                    pit2_mon.watch(t);
                }
                REQUIRE(wall_mon.max_z == 0);
                REQUIRE(wall_mon.min_z == -(pit_depth)*tile_size);

                REQUIRE(pit1_mon.max_z == -(pit_depth - 1) * tile_size);
                REQUIRE(pit1_mon.min_z == -(pit_depth)*tile_size);

                REQUIRE(pit2_mon.counter == 0);
            }
        }
    }
}

SCENARIO("A world can be built from a level", "[level]") {
    GIVEN("A world builder") {
        Walls_builder<M> builder;

        WHEN("A very simple level, made of one floor, is used as model") {
            Level lvl;
            lvl.add(Floor{}, {0, 0});

            THEN("the builder lazily generates the world") {
                uint16_t triangles = 0;
                for (auto& t : builder.construct(lvl)) {
                    triangles++;
                }
                REQUIRE(triangles == 12);
            }
        }
    }
}
