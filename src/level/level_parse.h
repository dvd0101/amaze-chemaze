#pragma once
#include "level.h"
#include "../images/formats.h"
#include <istream>

namespace amaze {
    namespace level {
        Level parse(std::istream&);
        Level parse(Image<rgb>);
        void serialize(Level&, std::ostream&);
    }
}
