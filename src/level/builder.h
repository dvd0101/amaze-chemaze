#pragma once
#include <bitset>
#include <cstdint>
#include <boost/variant/get.hpp>
#include "inline_variant_visitor/inline_variant.hpp"
#include "../models/geom.h"
#include "level.h"
#include "../world/config.h"

namespace amaze {
    namespace level {
        using Cube_faces = std::bitset<6>;

        namespace details {
            struct Triangle_information {
                uint8_t cube_face;
                Point cube_center;
            };

            template <typename T, typename MEASURE, typename OUT>
            void place_cube_base(const Cube_coord& coord, OUT sink)
            // place a cube of size MEASURE::tile_size to the coordinates specified by coord
            {
                const auto size = MEASURE::tile_size;
                //
                // The origin of a cube is the bottom-left angle of the rear face
                const Point origin{coord.x * size, coord.y * size, coord.z * size};
                T t;
                Triangle_information info;
                info.cube_center = origin + Point{size/2, size/2, size/2};

                // bottom face
                {
                    const Point o{origin};
                    info.cube_face = cube_face::bottom;
                    t.a = o;
                    t.b = o;
                    t.b.y += size;
                    t.c = o;
                    t.c.x += size;
                    sink(t, info);
                    t.a.x += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    sink(t, info);
                }

                // top face
                {
                    const Point o{origin.x, origin.y, origin.z + size};
                    info.cube_face = cube_face::top;
                    t.a = o;
                    t.b = o;
                    t.b.x += size;
                    t.c = o;
                    t.c.y += size;
                    sink(t, info);
                    t.a.x += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    sink(t, info);
                }

                // left face
                {
                    const Point o{origin};
                    info.cube_face = cube_face::left;
                    t.a = o;
                    t.b = o;
                    t.b.z += size;
                    t.c = o;
                    t.c.y += size;
                    sink(t, info);
                    t.a.z += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    sink(t, info);
                }

                // right face
                {
                    const Point o{origin.x + size, origin.y, origin.z};
                    info.cube_face = cube_face::right;
                    t.a = o;
                    t.b = o;
                    t.b.y += size;
                    t.c = o;
                    t.c.z += size;
                    sink(t, info);
                    t.a.z += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    sink(t, info);
                }

                // rear face
                {
                    const Point o{origin};
                    info.cube_face = cube_face::rear;
                    t.a = o;
                    t.b = o;
                    t.b.x += size;
                    t.c = o;
                    t.c.z += size;
                    sink(t, info);
                    t.a.z += size;
                    t.a.x += size;
                    std::swap(t.b, t.c);
                    sink(t, info);
                }

                // front face
                {
                    const Point o{origin.x, origin.y + size, origin.z};
                    info.cube_face = cube_face::front;
                    t.a = o;
                    t.b = o;
                    t.b.z += size;
                    t.c = o;
                    t.c.x += size;
                    sink(t, info);
                    t.a.z += size;
                    t.a.x += size;
                    std::swap(t.b, t.c);
                    sink(t, info);
                }
            }

            template <typename MEASURE, typename OUT>
            void place_cube(const Cube_coord& coord, Cube_faces visible, uint8_t hint_index, OUT sink)
            // place a cube of size MEASURE::tile_size to the coordinates specified by coord
            {
                const auto size = MEASURE::tile_size;
                //
                // The origin of a cube is the bottom-left angle of the rear face
                const Point origin{coord.x * size, coord.y * size, coord.z * size};
                Cube_triangle t;

                // bottom face
                {
                    t.face    = cube_face::bottom;
                    t.visible = (visible & Cube_faces(cube_face::bottom)) != 0;
                    t.hint    = 0;
                    const Point o{origin};
                    t.a = o;
                    t.b = o;
                    t.b.y += size;
                    t.c = o;
                    t.c.x += size;
                    *sink = t;
                    t.a.x += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    *sink = t;
                }

                // top face
                {
                    t.face    = cube_face::top;
                    t.visible = (visible & Cube_faces(cube_face::top)) != 0;
                    t.hint    = 0;
                    const Point o{origin.x, origin.y, origin.z + size};
                    t.a = o;
                    t.b = o;
                    t.b.x += size;
                    t.c = o;
                    t.c.y += size;
                    *sink = t;
                    t.a.x += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    *sink = t;
                }

                // left face
                {
                    t.face    = cube_face::left;
                    t.visible = (visible & Cube_faces(cube_face::left)) != 0;
                    t.hint    = hint_index;
                    const Point o{origin};
                    t.a = o;
                    t.b = o;
                    t.b.z += size;
                    t.c = o;
                    t.c.y += size;
                    *sink = t;
                    t.a.z += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    *sink = t;
                }

                // right face
                {
                    t.face    = cube_face::right;
                    t.visible = (visible & Cube_faces(cube_face::right)) != 0;
                    t.hint    = hint_index;
                    const Point o{origin.x + size, origin.y, origin.z};
                    t.a = o;
                    t.b = o;
                    t.b.y += size;
                    t.c = o;
                    t.c.z += size;
                    *sink = t;
                    t.a.z += size;
                    t.a.y += size;
                    std::swap(t.b, t.c);
                    *sink = t;
                }

                // rear face
                {
                    t.face    = cube_face::rear;
                    t.visible = (visible & Cube_faces(cube_face::rear)) != 0;
                    t.hint    = hint_index;
                    const Point o{origin};
                    t.a = o;
                    t.b = o;
                    t.b.x += size;
                    t.c = o;
                    t.c.z += size;
                    *sink = t;
                    t.a.z += size;
                    t.a.x += size;
                    std::swap(t.b, t.c);
                    *sink = t;
                }

                // front face
                {
                    t.face    = cube_face::front;
                    t.visible = (visible & Cube_faces(cube_face::front)) != 0;
                    t.hint    = hint_index;
                    const Point o{origin.x, origin.y + size, origin.z};
                    t.a = o;
                    t.b = o;
                    t.b.z += size;
                    t.c = o;
                    t.c.x += size;
                    *sink = t;
                    t.a.z += size;
                    t.a.x += size;
                    std::swap(t.b, t.c);
                    *sink = t;
                }
            }

            template <typename MEASURE, typename OUT>
            void place_floor(const Tile_block&, Tile_coord tile, OUT sink) {
                Cube_coord cube{tile, -1};
                place_cube<MEASURE>(cube, cube_face::top, 0, sink);
            }

            inline Cube_faces wall_visibility(const Tile_block& block) {
                Cube_faces visibility{0};
                auto check = [](optional<Tile> el, uint8_t value) -> uint8_t {
                    if (!el || boost::get<Wall>(&*el) != nullptr || boost::get<Portal>(&*el) != nullptr) {
                        return 0;
                    }
                    return value;
                };
                visibility |= check(block.n, cube_face::rear);
                visibility |= check(block.e, cube_face::right);
                visibility |= check(block.s, cube_face::front);
                visibility |= check(block.w, cube_face::left);
                return visibility;
            }

            template <typename MEASURE, typename OUT>
            void place_wall(const Tile_block& block, Tile_coord tile, OUT sink) {
                Cube_coord cube{tile, 0};
                place_cube<MEASURE>(cube, wall_visibility(block), boost::get<Wall>(block.center).hint, sink);
            }

            template <typename MEASURE, typename OUT>
            void place_pit(const Tile_block& block, Tile_coord tile, OUT sink) {
                Cube_coord cube{tile, -MEASURE::pit_depth};
                place_cube<MEASURE>(cube, cube_face::top, 0, sink);

                // for every tiles in the neighborhood that is not a pit
                // additional cubes must be created;
                auto place_column = [&sink](optional<Tile> el, uint16_t x, uint16_t y, uint8_t face) {
                    if (!el || boost::get<Pit>(&*el) != nullptr) {
                        return;
                    }
                    for (int z = -1; z >= -MEASURE::pit_depth; z--) {
                        Cube_coord cube{x, y, z};
                        place_cube<MEASURE>(cube, face, 0, sink);
                    };
                };

                place_column(block.n, tile.x, tile.y - 1, cube_face::front);
                place_column(block.w, tile.x - 1, tile.y, cube_face::right);
                place_column(block.e, tile.x + 1, tile.y, cube_face::left);
                place_column(block.s, tile.x, tile.y + 1, cube_face::rear);
            }

            template <typename MEASURE, typename OUT>
            void place_portal(const Tile_block& block, Tile_coord tile, OUT sink) {
                Cube_coord cube{tile, 0};
                place_cube<MEASURE>(cube, wall_visibility(block), 0, sink);
            }
        }

        using walls_emitter = boost::coroutines2::coroutine<const Cube_triangle&>;

        template <typename MEASURE = World_config>
        class Walls_builder {
            // A Walls_builder transforms a 2d level in a 3d representation.
            //
            // Our world is made of cubes, all of the same size.
            //
            // Every tile in the level is represented by one or more cubes.
          public:
            walls_emitter::pull_type construct(const Level&) const;
            walls_emitter::pull_type construct(block_tile_iterator::pull_type) const;

            template <typename OUT>
            void single_tile(const Level& lvl, Tile_coord coord, OUT sink) const {
                auto tile = lvl.what(coord);
                if (!tile) {
                    throw std::runtime_error{"no tile"};
                }
                add(Tile_block{*tile}, coord, sink);
            }

          private:
            // add a tile (the center of the block) to the world
            template <typename OUT>
            void add(const Tile_block&, Tile_coord, OUT) const;
        };

        template <typename MEASURE>
        walls_emitter::pull_type Walls_builder<MEASURE>::construct(const Level& lvl) const {
            return construct(lvl.block_iterate());
        }

        template <typename MEASURE>
        walls_emitter::pull_type Walls_builder<MEASURE>::construct(block_tile_iterator::pull_type it) const {
            using coro = walls_emitter;
            return coro::pull_type([ this, it = std::move(it) ](coro::push_type & sink) mutable {
                using std::begin;
                for (auto& el : it) {
                    auto& block = std::get<0>(el);
                    auto coord = std::get<1>(el);
                    add(block, coord, begin(sink));
                }
            });
        }

        template <typename MEASURE>
        template <typename OUT>
        void Walls_builder<MEASURE>::add(const Tile_block& block, Tile_coord coord, OUT sink) const {
            match(
                block.center,
                [this, &block, coord, &sink](const Floor&) { details::place_floor<MEASURE, OUT>(block, coord, sink); },
                [this, &block, coord, &sink](const Pit&) { details::place_pit<MEASURE, OUT>(block, coord, sink); },
                [this, &block, coord, &sink](const Start&) { details::place_floor<MEASURE, OUT>(block, coord, sink); },
                [this, &block, coord, &sink](const Wall&) { details::place_wall<MEASURE, OUT>(block, coord, sink); },
                [this, &block, coord, &sink](const Portal&) {
                    details::place_portal<MEASURE, OUT>(block, coord, sink);
                });
        };

        template <typename MEASURE = World_config>
        std::vector<Cube_triangle> build_single_tile(const Level& lvl, Tile_coord coord) {
            Walls_builder<MEASURE> builder;
            std::vector<Cube_triangle> output;
            builder.single_tile(lvl, coord, std::back_inserter(output));
            return output;
        }

        using portals_emitter = boost::coroutines2::coroutine<const Portal_triangle&>;

        template <typename MEASURE = World_config>
        class Portals_builder {
          public:
            portals_emitter::pull_type construct(const Level& lvl) const {
                return construct(lvl.block_iterate());
            }
            portals_emitter::pull_type construct(block_tile_iterator::pull_type it) const {
                using coro = portals_emitter;
                return coro::pull_type([ this, it = std::move(it) ](coro::push_type & sink) mutable {
                    using std::begin;
                    for (auto& el : it) {
                        auto& block = std::get<0>(el);
                        auto coord = std::get<1>(el);
                        add(block, coord, begin(sink));
                    }
                });
            }

          private:
            template <typename OUT>
            void add(const Tile_block& block, Tile_coord coord, OUT sink) const {
                match(block.center,
                      [this, &block, coord, &sink](const Floor&) {},
                      [this, &block, coord, &sink](const Pit&) {},
                      [this, &block, coord, &sink](const Start&) {},
                      [this, &block, coord, &sink](const Wall&) {},
                      [this, &block, coord, &sink](const Portal& tile) {
                          Cube_coord cube{coord, 0};
                          auto f      = [&sink, &tile, &cube](Portal_triangle t, details::Triangle_information info) {
                              t.image = tile.image;
                              t.level = cube.z;
                              t.cube_center = info.cube_center;
                              *sink   = t;
                          };
                          details::place_cube_base<Portal_triangle, MEASURE>(cube, f);
                          cube.z = 2;
                          details::place_cube_base<Portal_triangle, MEASURE>(cube, f);
                          cube.z = 4;
                          details::place_cube_base<Portal_triangle, MEASURE>(cube, f);
                      });
            }
        };
    }
}
