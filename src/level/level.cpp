#include "level.h"
#include <algorithm>
#include <format/format.h>
#include "inline_variant_visitor/inline_variant.hpp"

namespace amaze {
    namespace level {
        std::ostream& operator<<(std::ostream& os, Floor) {
            os << "floor";
            return os;
        }

        std::ostream& operator<<(std::ostream& os, Pit) {
            os << "pit";
            return os;
        }

        std::ostream& operator<<(std::ostream& os, Start) {
            os << "start";
            return os;
        }

        std::ostream& operator<<(std::ostream& os, Wall w) {
            os << "wall";
            if (w.hint > 0) {
                os << "[" << w.hint << "]";
            }
            return os;
        }

        std::ostream& operator<<(std::ostream& os, Portal p) {
            fmt::print(os, "portal[{};{};{}]", p.dest, p.image, p.score);
            return os;
        }

        Tile_block::Tile_block(Tile t) : center{t} {}

        void Tiles_stats::inc(const Tile& t) {
            inc(t, 1);
        }

        void Tiles_stats::inc(const Tile& t, uint16_t count) {
            match(t,
                  [this, count](Floor) { _floors += count; },
                  [this, count](Pit) { _pits += count; },
                  [this, count](Start) { _starts += count; },
                  [this, count](Wall w) {
                      _walls += count;
                      if (w.hint != 0) {
                          _walls_hints += count;
                      }
                  },
                  [this, count](Portal) { _portals += count; });
        }

        void Tiles_stats::dec(const Tile& t) {
            match(t,
                  [this](Floor) { _floors -= 1; },
                  [this](Pit) { _pits -= 1; },
                  [this](Start) { _starts -= 1; },
                  [this](Wall w) {
                      _walls -= 1;
                      if (w.hint != 0) {
                          _walls_hints -= 1;
                      }
                  },
                  [this](Portal) { _portals -= 1; });
        }

        uint16_t Tiles_stats::tiles() const {
            return _floors + _pits + _starts + _walls;
        }

        uint16_t Tiles_stats::floors() const {
            return _floors;
        }

        uint16_t Tiles_stats::pits() const {
            return _pits;
        }

        uint16_t Tiles_stats::starts() const {
            return _starts;
        }

        uint16_t Tiles_stats::walls() const {
            return _walls;
        }

        uint16_t Tiles_stats::walls_hints() const {
            return _walls_hints;
        }

        uint16_t Tiles_stats::portals() const {
            return _portals;
        }

        uint16_t Level::rows() const {
            return map_.size();
        }

        uint16_t Level::cols() const {
            return map_.size() > 0 ? map_[0].size() : 0;
        }

        void Level::add(Tile t, Tile_coord coord) {
            resize(coord);

            stats_.dec(map_[coord.y][coord.x]);
            map_[coord.y][coord.x] = t;
            stats_.inc(t);
        }

        const Tiles_stats& Level::stats() const {
            return stats_;
        }

        void Level::resize(Tile_coord coord) {
            if (cols() <= coord.x) {
                const default_tile t{};
                const uint16_t to_add = coord.x + 1 - cols();
                for (auto& row : map_) {
                    row.resize(coord.x + 1, t);
                    stats_.inc(t, to_add);
                }
            }

            if (rows() <= coord.y) {
                const uint16_t to_add = coord.y - rows() + 1;
                const uint16_t size   = std::max(cols(), static_cast<uint16_t>(coord.x + 1));
                const Tile d          = default_tile{};

                for (uint16_t ix = 0; ix < to_add; ix++) {
                    map_.emplace_back(size, d);
                    stats_.inc(d, size);
                }
            }
        }

        tile_iterator::pull_type Level::iterate() const {
            using coro = tile_iterator;
            return coro::pull_type{[this](coro::push_type& sink) {
                Tile_coord c{0, 0};
                for (auto& row : map_) {
                    c.x = 0;
                    for (const auto& col : row) {
                        sink(std::tie(col, c));
                        c.x++;
                    }
                    c.y++;
                }
            }};
        }

        block_tile_iterator::pull_type Level::block_iterate() const {
            using coro = block_tile_iterator;
            return coro::pull_type{[this](coro::push_type& sink) {
                Tile_coord c{0, 0};
                Tile_block b;
                auto fill = [this, &c, &b](const Tile& t) {
                    using std::experimental::nullopt;

                    b.center = t;
                    b.nw = b.n = b.ne = nullopt;
                    b.sw = b.s = b.se = nullopt;
                    b.w = b.e = nullopt;

                    const bool enable_north = c.y > 0;
                    const bool enable_south = c.y < rows() - 1;
                    const bool enable_west  = c.x > 0;
                    const bool enable_east  = c.x < cols() - 1;

                    if (enable_north) {
                        b.n = map_[c.y - 1][c.x];
                        if (enable_west) {
                            b.nw = map_[c.y - 1][c.x - 1];
                        }
                        if (enable_east) {
                            b.ne = map_[c.y - 1][c.x + 1];
                        }
                    }

                    if (enable_west) {
                        b.w = map_[c.y][c.x - 1];
                    }
                    if (enable_east) {
                        b.e = map_[c.y][c.x + 1];
                    }

                    if (enable_south) {
                        b.s = map_[c.y + 1][c.x];
                        if (enable_west) {
                            b.sw = map_[c.y + 1][c.x - 1];
                        }
                        if (enable_east) {
                            b.se = map_[c.y + 1][c.x + 1];
                        }
                    }

                };
                for (auto& row : map_) {
                    c.x = 0;
                    for (const auto& col : row) {
                        fill(col);
                        sink(std::tie(b, c));
                        c.x++;
                    }
                    c.y++;
                }
            }};
        }

        optional<Tile> Level::what(Tile_coord c) const {
            if (c.y >= rows() || c.x >= cols()) {
                return {};
            }
            return map_[c.y][c.x];
        }

        optional<Tile> Level::what(Cube_coord c) const {
            if (c.y < 0 || c.y >= rows() || c.x < 0 || c.x >= cols()) {
                return {};
            }
            return map_[c.y][c.x];
        }
    }
}
