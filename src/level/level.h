#pragma once
#include <cstdint>
#include <tuple>
#include <vector>
#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>
#include <boost/coroutine2/coroutine.hpp>
#include "../common.h"
#include "../models/geom.h"

namespace amaze {
    namespace level {
        // a tile can be one of these types
        struct Floor {};
        struct Pit {};
        struct Start {};
        struct Wall {
            uint8_t hint = 0;
        };
        struct Portal {
            Tile_coord dest;
            uint8_t image;
            int8_t score;
        };

        std::ostream& operator<<(std::ostream&, Floor);
        std::ostream& operator<<(std::ostream&, Pit);
        std::ostream& operator<<(std::ostream&, Start);
        std::ostream& operator<<(std::ostream&, Wall);
        std::ostream& operator<<(std::ostream&, Portal);

        using Tile = boost::variant<Floor, Wall, Pit, Start, Portal>;

        struct Tile_block {
            /*
             * Tile_block is a block of 3x3 tiles. It's used when the level is
             * iterated by blocks;`central` is the tile currently iterated and
             * the eight others its adjacent elements.
             */
            optional<Tile> nw;
            optional<Tile> n;
            optional<Tile> ne;
            optional<Tile> w;
            Tile center;
            optional<Tile> e;
            optional<Tile> sw;
            optional<Tile> s;
            optional<Tile> se;

            Tile_block() = default;
            Tile_block(Tile);
        };

        using tile_iterator_t = std::tuple<const Tile&, Tile_coord>;
        using tile_iterator   = boost::coroutines2::coroutine<tile_iterator_t>;

        template <typename T>
        using specific_tile_iterator_t = std::tuple<const T&, Tile_coord>;
        template <typename T>
        using specific_tile_iterator = boost::coroutines2::coroutine<specific_tile_iterator_t<T>>;

        using block_tile_iterator_t = std::tuple<const Tile_block&, Tile_coord>;
        using block_tile_iterator   = boost::coroutines2::coroutine<block_tile_iterator_t>;

        class Tiles_stats {
          public:
            void inc(const Tile&);
            void inc(const Tile&, uint16_t);
            void dec(const Tile&);

            uint16_t tiles() const;
            uint16_t floors() const;
            uint16_t pits() const;
            uint16_t starts() const;
            uint16_t walls() const;
            uint16_t walls_hints() const;
            uint16_t portals() const;

          private:
            uint16_t _floors      = 0;
            uint16_t _pits        = 0;
            uint16_t _starts      = 0;
            uint16_t _walls       = 0;
            uint16_t _walls_hints = 0;
            uint16_t _portals     = 0;
        };

        class Level {
            /*
             * A Level is a collection of tiles laid out on a 2d surface.
             *
             * The shape of the level is a rectangle, every tile has a
             * coordinate (see `Tile_coord`) and the origin is the left top
             * corner.
             */
          public:
            using default_tile = Floor;

          public:
            uint16_t rows() const;
            uint16_t cols() const;

            /*
             * iterate returns a coroutine (that looks like an iterator)
             * that iterate over every tile in the level.
             */
            tile_iterator::pull_type iterate() const;
            block_tile_iterator::pull_type block_iterate() const;

            template <typename T>
            typename specific_tile_iterator<T>::pull_type iterate() const {
                using coro = specific_tile_iterator<T>;

                return typename coro::pull_type([this](typename coro::push_type& sink) {
                    for (const auto& el : iterate()) {
                        const Tile& tile = std::get<0>(el);
                        if (const T* ptr = boost::get<T>(&tile)) {
                            sink(std::tie(*ptr, std::get<1>(el)));
                        }
                    }
                });
            }

          public:
            void add(Tile, Tile_coord);
            const Tiles_stats& stats() const;

            optional<Tile> what(Tile_coord) const;
            optional<Tile> what(Cube_coord) const;

          private:
            void resize(Tile_coord);

          private:
            using row_t = std::vector<Tile>;
            std::vector<row_t> map_;
            Tiles_stats stats_;
        };
    }
}
