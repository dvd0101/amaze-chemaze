#include "level_parse.h"
#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/include/support_line_pos_iterator.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>
#include <boost/fusion/include/std_tuple.hpp>
#include <boost/fusion/include/at_c.hpp>
#include <boost/coroutine2/coroutine.hpp>
#include <boost/variant/static_visitor.hpp>
#include <format/format.h>

namespace amaze {
    namespace level {
        namespace x3 = boost::spirit::x3;

        namespace {
            template <typename T>
            Level parse(T begin, T end) {
                Level lvl;

                Tile_coord coord;

                auto hint = x3::lit('[') >> x3::int_ >> ']';
                auto dest = ( //
                    // this strange code is a workaround for a bug in spirit (boost 1.6[01])
                    // see: http://stackoverflow.com/questions/38213729/
                    x3::rule<struct dest_type, std::tuple<int, int>>{} = '[' >> x3::int_ >> ';' >> x3::int_ >> ']'); //
                auto score = '[' >> x3::int_ >> ']';

                auto pos = [&coord](auto& ctx) {

                    auto range = x3::_where(ctx);
                    auto line = boost::spirit::get_line(range.begin()) - 1;
                    if (line != coord.y) {
                        coord.y = line;
                        coord.x = 0;
                    } else {
                        coord.x++;
                    }
                    return coord;
                };

                auto on_wall = [&](auto& ctx) {
                    Wall tile;

                    if (auto hint = x3::_attr(ctx)) {
                        tile.hint = *hint;
                    }
                    lvl.add(tile, pos(ctx));
                };
                auto wall = ('-' >> -hint)[on_wall];

                auto on_portal = [&](auto& ctx) {
                    Portal tile;

                    auto attrs = x3::_attr(ctx);

                    auto tup = boost::fusion::at_c<0>(attrs);
                    std::tie(tile.dest.x, tile.dest.y) = tup;

                    tile.image = boost::fusion::at_c<1>(attrs);
                    tile.score = boost::fusion::at_c<2>(attrs);

                    lvl.add(tile, pos(ctx));
                };
                auto portal = ('P' > dest > hint > score)[on_portal];

                auto on_floor = [&](auto& ctx) { lvl.add(Floor{}, pos(ctx)); };
                auto floor    = x3::lit(' ')[on_floor];

                auto on_start = [&](auto& ctx) { lvl.add(Start{}, pos(ctx)); };
                auto start    = x3::lit('S')[on_start];

                auto tile  = wall | floor | start | portal;
                auto tiles = +tile;
                x3::phrase_parse(begin, end, tiles, x3::eol);
                return lvl;
            }
        }

        Level parse(std::istream& input) {
            input.unsetf(std::ios::skipws);
            boost::spirit::istream_iterator f{input}, l;

            using source_iterator = boost::spirit::line_pos_iterator<boost::spirit::istream_iterator>;
            auto first            = source_iterator{f};
            auto last = source_iterator{l};
            return parse(first, last);
        }

        namespace {

            struct image_token {
                rgb first;
                rgb second;
                rgb third;
                rgb fourth;
            };

            using image_block_iterator_t = std::tuple<Tile_coord, image_token>;
            using image_block_iterator   = boost::coroutines2::coroutine<image_block_iterator_t>;

            image_block_iterator::pull_type tokenize(Image<rgb>& img, uint8_t size) {
                if (img.width() % size || img.height() % size) {
                    throw std::runtime_error{"invalid block size"};
                }

                using coro = image_block_iterator;
                return coro::pull_type([&img, size](coro::push_type& sink) {
                    const size_t w        = img.width();
                    const size_t h        = img.height();
                    const size_t last_row = h / size;
                    const size_t last_col = w / size;

                    if (last_row > std::numeric_limits<uint16_t>::max()) {
                        throw std::runtime_error{"image height too big"};
                    }
                    if (last_col > std::numeric_limits<uint16_t>::max()) {
                        throw std::runtime_error{"image width too big"};
                    }

                    auto pixels = img.data();
                    Tile_coord coord;
                    for (coord.y = 0; coord.y < last_row; coord.y++) {
                        for (coord.x = 0; coord.x < last_col; coord.x++) {
                            int32_t x0 = coord.y * size * w + coord.x * size;
                            int32_t x1 = x0 + size - 1;
                            int32_t x2 = x1 + (size - 1) * w;
                            int32_t x3 = x0 + (size - 1) * w;
                            image_token info{pixels[x0], pixels[x1], pixels[x2], pixels[x3]};
                            sink(std::make_tuple(coord, info));
                        }
                    }
                });
            }

            using image_decode_iterator_t = std::tuple<Tile, Tile_coord>;
            using image_decode_iterator   = boost::coroutines2::coroutine<image_decode_iterator_t>;

            const rgb wall{0x00, 0x00, 0x00};
            const rgb hint{0x67, 0xbc, 0x29};
            const rgb floor{0xc5, 0xed, 0xbb};
            const rgb start{0xff, 0xff, 0x00};
            const rgb portal{0x00, 0x00, 0xff};
            template <typename T>
            image_decode_iterator::pull_type decode(T& source) {
                using coro = image_decode_iterator;
                return coro::pull_type([&source](coro::push_type& sink) {
                    struct portal_data {
                        Tile_coord pos;
                        uint8_t image;
                        int8_t score;
                        rgb dest;
                    };
                    std::vector<portal_data> portals;
                    std::map<rgb, Tile_coord> destinations;

                    for (auto& tile : source) {
                        Tile_coord coord;
                        image_token info;
                        std::tie(coord, info) = tile;

                        Tile decoded;
                        if (info.first == wall) {
                            decoded = Wall{};
                        } else if (info.first == floor) {
                            decoded = Floor{};
                            if (info.second != floor) {
                                auto result = destinations.insert({info.second, coord});
                                if (!result.second) {
                                    throw std::runtime_error{
                                        fmt::format("multiple floors with the same destination: {}", info.second)};
                                }
                            }
                        } else if (info.first == start) {
                            decoded = Start{};
                        } else if (info.first == hint) {
                            decoded = Wall{info.second.b()};
                        } else if (info.first == portal) {
                            portals.push_back({coord, info.third.b(), static_cast<int8_t>(static_cast<int16_t>(info.fourth.b()) - 128), info.second});
                            continue;
                        } else {
                            continue;
                        }
                        sink(std::make_tuple(decoded, coord));
                    }
                    for (auto& p : portals) {
                        auto dest = destinations.find(p.dest);
                        if (dest == destinations.end()) {
                            throw std::runtime_error{fmt::format("destination not found: {}", p.dest)};
                        }
                        Portal tile{dest->second, p.image, p.score};
                        sink(std::make_tuple(tile, p.pos));
                    }
                });
            }
        }

        Level parse(Image<rgb> img) {
            Level lvl;
            auto parsed = tokenize(img, 5);
            for (auto& t : decode(parsed)) {
                Tile_coord coord;
                Tile tile;
                std::tie(tile, coord) = t;
                lvl.add(tile, coord);
            }
            return lvl;
        }

        namespace {
            struct tile_visitor : public boost::static_visitor<void> {
                tile_visitor(std::ostream& o) : os{o} {}

                void operator()(Wall w) {
                    fmt::print(os, "-");
                    if (w.hint != 0) {
                        fmt::print(os, "[{}]", w.hint);
                    }
                }

                void operator()(Floor) {
                    fmt::print(os, " ");
                }

                void operator()(Pit) {
                    fmt::print(os, "*");
                }

                void operator()(Start) {
                    fmt::print(os, "S");
                }

                void operator()(Portal p) {
                    fmt::print(os, "P[{};{}][{}][{}]", p.dest.x, p.dest.y, p.image, p.score);
                }

                std::ostream& os;
            };
        }

        void serialize(Level& lvl, std::ostream& os) {
            tile_visitor v{os};
            uint16_t row = 0;
            for (auto& t : lvl.iterate()) {
                Tile tile;
                Tile_coord coord;
                std::tie(tile, coord) = t;
                if (coord.y != row) {
                    row = coord.y;
                    fmt::print(os, "\n");
                }
                boost::apply_visitor(v, tile);
            }
        };
    }
}
