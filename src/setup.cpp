#include "main.h"
#include "world/stream_adapters.h"
#include <fstream>

namespace details {
    class Sink_on_stream {
      public:
        Sink_on_stream(amaze::Sink& original, std::ofstream copy)
            : _original{original}, _fs_stream{std::move(copy)}, _copy{_fs_stream}, _tee{_original, _copy} {}

        amaze::Sink& sink() {
            return _tee;
        }

      private:
        amaze::Sink& _original;
        std::ofstream _fs_stream;
        amaze::Sink_stream _copy;
        amaze::Sink_tee _tee;
    };

    class Source_from_stream {
      public:
        Source_from_stream(std::ifstream prologue, amaze::Source& original, float speedup)
            : _original{original},
              _fs_prologue{std::move(prologue)},
              _prologue{_fs_prologue},
              _cat{_prologue, _original},
              _replay{_cat, speedup} {}

        amaze::Source& source() {
            return _replay;
        }

      private:
        amaze::Source& _original;
        std::ifstream _fs_prologue;
        amaze::Source_stream _prologue;
        amaze::Source_cat _cat;
        amaze::Replay _replay;
    };
}

App_stream::App_stream(const config& cfg, int32_t player_id)
    : _stream{player_id}, _sink_stream{nullptr}, _source_stream{nullptr} {
    if (cfg.record.filename.empty()) {
        _sink = &_stream;
    } else {
        std::ofstream f{cfg.record.filename};
        _sink_stream = std::make_unique<details::Sink_on_stream>(_stream, std::move(f));
        _sink        = &(_sink_stream->sink());
    }

    if (cfg.replay.filename.empty()) {
        _source = &_stream;
    } else {
        std::ifstream f{cfg.replay.filename};
        _source_stream = std::make_unique<details::Source_from_stream>(std::move(f), _stream, cfg.replay.speedup);
        _source        = &(_source_stream->source());
    }
}

App_stream::~App_stream() = default;

amaze::Sink& App_stream::sink() {
    return *_sink;
}

amaze::Source& App_stream::source() {
    return *_source;
}

amaze::Program program(const std::string& prefix) {
    return amaze::build(fmt::format("../shaders/{}.vsh", prefix), fmt::format("../shaders/{}.fsh", prefix));
}

asio::steady_timer timeout(asio::io_service& io, std::chrono::milliseconds ms, std::function<void()> callback) {
    asio::steady_timer t{io, ms};
    t.async_wait([callback](const std::error_code& ec) {
        if (ec == asio::error::operation_aborted) {
            return;
        }
        callback();
    });
    return std::move(t);
}
