#include <boost/program_options.hpp>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <format/format.h>
#include "common.h"
#include "level/level_parse.h"
#include "level/builder.h"
#include "timeit.h"

using amaze::optional;
using namespace amaze;

struct config {
    std::string input;
    std::string output;
    bool only_stats;
};

optional<config> parse_cmdline(int argc, char* argv[]) {
    namespace po = boost::program_options;
    using std::string;
    using std::vector;
    config cfg;

    po::options_description desc("Allowed options");
    desc.add_options()                                                                                       //
        ("help", "produce help message")                                                                     //
        ("only-stats", po::value<bool>(&cfg.only_stats)->default_value(false), "just show the level stats"); //

    po::options_description files("Files");
    files.add_options()                                                        //
        ("input", po::value<string>(&cfg.input)->required(), "level file")     //
        ("output", po::value<string>(&cfg.output)->required(), "output file"); //

    po::positional_options_description pos;
    pos.add("input", 1);
    pos.add("output", 1);

    po::options_description all("Allowed options");
    all.add(desc).add(files);

    bool force_help = false;
    po::variables_map vm;
    try {
        po::store(                              //
            po::command_line_parser(argc, argv) //
                .options(all)                   //
                .positional(pos)                //
                .run(),                         //
            vm);                                //
        po::notify(vm);
    } catch (po::unknown_option const&) {
        force_help = true;
    } catch (po::required_option const&) {
        force_help = true;
    }

    if (force_help || vm.count("help")) {
        fmt::print("Usage: {} [OPTION] input_file output_file\n", argv[0]);
        fmt::print("{}\n", desc);
        return {};
    }
    return cfg;
}

std::string output_name(std::string prefix, size_t counter) {
    size_t dot = prefix.find_last_of('.');
    if (dot == std::string::npos) {
        return fmt::format("{}-{}.ppm", prefix, counter);
    }
    return fmt::format("{}-{}{}", prefix.substr(0, dot), counter, prefix.substr(dot));
}

void dump_stats(const level::Level& lvl) {
    level::Walls_builder<> builder;

    struct {
        size_t counter;
        size_t visible;
    } triangles{0, 0};

    {
        Time_track t{"T: building the world ->"};
        for (auto& t : builder.construct(lvl)) {
            triangles.counter++;
            if (t.visible) {
                triangles.visible++;
            }
        }
    }

    fmt::print("I: level {}x{}\n", lvl.rows(), lvl.cols());
    fmt::print("I:     tiles breakdown\n");
    fmt::print("I:     floors    : {:>4}\n", lvl.stats().floors());
    fmt::print("I:     pits      : {:>4}\n", lvl.stats().pits());
    fmt::print("I:     starts    : {:>4}\n", lvl.stats().starts());
    fmt::print("I:     walls     : {:>4}\n", lvl.stats().walls());
    fmt::print("I:       w/ hints: {:>4}\n", lvl.stats().walls_hints());
    fmt::print("I:     portals   : {:>4}\n", lvl.stats().portals());
    fmt::print("I:     ================\n", lvl.stats().tiles());
    fmt::print("I:     total     : {:>4}\n", lvl.stats().tiles());
    fmt::print("I: triangles {}\n", triangles.counter);
    fmt::print("I: visible triangles {}\n", triangles.visible);
}

std::vector<std::string> hint_filenames(const level::Level& lvl, const std::string& input) {
    std::vector<uint8_t> indexes;

    for (auto& tile : lvl.iterate<level::Portal>()) {
        auto& portal = std::get<0>(tile);
        if (portal.image == 0) {
            continue;
        }
        indexes.push_back(portal.image);
    }

    for (auto& tile : lvl.iterate<level::Wall>()) {
        auto& wall = std::get<0>(tile);
        if (wall.hint == 0) {
            continue;
        }
        indexes.push_back(wall.hint);
    }

    std::sort(std::begin(indexes), std::end(indexes));
    auto last = std::unique(std::begin(indexes), std::end(indexes));
    indexes.erase(last, indexes.end());

    // XXX: temporary patch which forces all hint indices to be used
    indexes.clear();
    for (uint8_t ix=1; ix<=61; ix++) {
        indexes.push_back(ix);
    }

    std::vector<std::string> sources;
    auto hint_name = [&input](uint8_t hint) {
        size_t dot = input.find_last_of('.');
        if (dot == std::string::npos) {
            return fmt::format("{}{:03}", input, hint);
        }
        return fmt::format("{}{:03}{}", input.substr(0, dot), hint, input.substr(dot));
    };
    std::transform(std::begin(indexes), std::end(indexes), std::back_inserter(sources), hint_name);
    return sources;
}

std::tuple<size_t, size_t> hint_size(const std::vector<std::string>& sources) {
    using dim = std::tuple<size_t, size_t>;

    std::vector<dim> buffer;

    bool poisoned = false;
    for (auto& fname : sources) {
        fmt::print("H: {} ", fname);
        try {
            auto img = png::read<rgb>(fname);
            fmt::print("-> {}x{}\n", img.width(), img.height());
            buffer.push_back(std::make_tuple(img.width(), img.height()));
        } catch (std::runtime_error&) {
            fmt::print("-> error\n");
            poisoned = true;
            continue;
        }
    }

    if (poisoned) {
        throw std::runtime_error{"some files are missing"};
    }

    auto s0         = buffer[0];
    bool all_equals = std::all_of(std::begin(buffer), std::end(buffer), [&s0](const dim& s) { return s0 == s; });
    if (!all_equals) {
        throw std::runtime_error{"images with different sizes"};
    }
    return s0;
}

struct Atlas {
    Atlas(const std::string& fname, uint16_t atlas_sz, uint16_t hint_sz)
        : name{fname},
          size{atlas_sz},
          hint_size{hint_sz},
          position{0},
          last_position{static_cast<uint16_t>(atlas_sz / hint_sz * atlas_sz / hint_sz)},
          canvas{atlas_sz, atlas_sz} {}

    ~Atlas() {
        try {
            png::write(name, canvas);
        } catch (std::runtime_error& e) {
            fmt::print(std::cerr, "!! error writing the atlas\n");
        }
    }

    bool attach(const std::string& image) {
        auto img = png::read<rgb>(image);
        return attach([&img](int index) { return img.data()[index]; });
    }

    bool attach(rgb color) {
        return attach([&color](int) { return color; });
    }

    template <typename Func>
    bool attach(Func f) {
        if (position >= last_position) {
            return false;
        }
        struct {
            int x;
            int y;
        } attach;
        const int hints_per_row = size / hint_size;

        attach.y = position / hints_per_row * hint_size;
        attach.x = hint_size * (position % hints_per_row);

        rgb* canvas_ptr = canvas.data();
        for (int y = 0; y < hint_size; y++) {
            int offset = (attach.y + y) * canvas.width() + attach.x;
            int iy = y * hint_size;
            for (int x = 0; x < hint_size; x++) {
                canvas_ptr[offset++] = f(iy + x);
            }
        }
        position++;
        return true;
    }

    bool skip() {
        if (position >= last_position) {
            return false;
        }
        position++;
        return true;
    }

    std::string name;
    uint16_t size;
    uint16_t hint_size;

    uint16_t position;
    const uint16_t last_position;

    Image<rgb> canvas;
};

int make_atlases(std::vector<std::string> sources, const std::string& output, uint16_t atlas_size, uint16_t hint_size) {
    auto atlas_name = [&output](uint8_t index) {
        size_t dot = output.find_last_of('.');
        return fmt::format("{}-{:03}.png", output.substr(0, dot), index);
    };

    int counter    = 0;
    auto new_atlas = [&]() {
        auto name = atlas_name(counter++);
        fmt::print("A: {}\n", name);
        return std::make_unique<Atlas>(name, atlas_size, hint_size);
    };
    auto atlas = new_atlas();
    // the first element is an all-white square; it is used as the default
    // texture for the splats without an underlying hint.
    atlas->attach(rgb{255, 255, 255});
    while (!sources.empty()) {
        if (atlas->attach(sources.front())) {
            fmt::print("\t{}\n", sources.front());
            sources.erase(sources.begin());
        } else {
            atlas = new_atlas();
        }
    }
    return counter;
}

void collect_hints(const level::Level& lvl, std::string input, std::string output) {
    const uint16_t atlas_size = 4096;

    auto sources = hint_filenames(lvl, input);
    if (sources.size() == 0) {
        return;
    }
    auto size = hint_size(sources);
    if (std::get<0>(size) != std::get<1>(size)) {
        throw std::runtime_error{"hint images must be squared"};
    }
    uint16_t hint_size = static_cast<uint16_t>(std::get<0>(size));
    if (atlas_size % hint_size != 0) {
        throw std::runtime_error{
            fmt::format("hint images cannot be placed in an atlas of {}x{}", atlas_size, atlas_size)};
    }
    make_atlases(sources, output, atlas_size, hint_size);
}

int main(int argc, char* argv[]) {
    auto cfg = parse_cmdline(argc, argv);
    if (!cfg) {
        return 1;
    }

    auto img  = png::read<rgb>(cfg->input);
    auto f    = [](Image<rgb> i) { return level::parse(std::move(i)); };
    auto lvl1 = timeit("T: parsing ->", f, std::move(img));

    // the level is first serialized in memory and then reparsed to exercise
    // all the parsing paths
    std::stringstream temp;
    level::serialize(lvl1, temp);
    auto lvl2 = level::parse(temp);

    dump_stats(lvl2);
    if (cfg->only_stats) {
        return 0;
    }

    collect_hints(lvl2, cfg->input, cfg->output);

    std::ofstream out{cfg->output};
    if (!out) {
        fmt::print(std::cerr, "error opening output file");
        return 1;
    }
    level::serialize(lvl2, out);
}
