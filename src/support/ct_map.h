#pragma once

namespace amaze {
    namespace support {
        struct key_error;

        template <typename k, typename v>
        struct kv {
            using key   = k;
            using value = v;
        };

        template <typename...>
        struct ct_map;

        template <>
        struct ct_map<> {
            template <typename>
            struct get {
                using value = key_error;
            };
        };

        template <typename k, typename v, typename... rest>
        struct ct_map<kv<k, v>, rest...> {
            template <typename kk>
            struct get {
                using others = ct_map<rest...>;
                using g      = typename others::template get<kk>;
                using value  = typename std::conditional<std::is_same<kk, k>::value, v, typename g::value>::type;
            };
        };

        template <typename... rest>
        struct ct_map_no_default {
            using map = ct_map<rest...>;

            template <typename key>
            using lookup = typename map::template get<key>::value;

            template <typename key, typename SFINAE = std::enable_if_t<!std::is_same<lookup<key>, key_error>::value>>
            struct get {
                using value = lookup<key>;
            };
        };

        template <typename dflt, typename... rest>
        struct ct_map_default {
            using map = ct_map<rest...>;

            template <typename kk>
            struct get {
                using lookup = typename map::template get<kk>::value;
                using value  = typename std::conditional<!std::is_same<lookup, key_error>::value, lookup, dflt>::type;
            };
        };
    }
}
