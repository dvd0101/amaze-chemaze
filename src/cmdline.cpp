#include "main.h"
#include "world/config.h"
#include <cstdint>
#include <boost/program_options.hpp>
#include <format/format.h>
#include <asio/ip/address.hpp>

namespace {
    std::string events_log_name() {
        auto t = amaze::now();
        return fmt::format("record_{}.log", t.time_since_epoch().count());
    }
}

optional<config> parse_cmdline(int argc, char* argv[]) {
    namespace po = boost::program_options;
    using std::string;
    using std::vector;
    config cfg;

    po::options_description replay("Replay options");
    replay.add_options()                                            //
        ("help", "show help")                                       //
        ("replay",                                                  //
         po::value<string>(&cfg.replay.filename),                   //
         "replay the given file")                                   //
        ("replay-player",                                           //
         po::value<int32_t>(&cfg.replay.player)->default_value(0),  //
         "replay the given player")                                 //
        ("replay-speedup",                                          //
         po::value<float>(&cfg.replay.speedup)->default_value(2.f), //
         "replay speedup (1 means normal speed)");                  //

    po::options_description server("Server address");
    server.add_options()                                   //
        ("address", po::value<string>(), "server address") //
        ("port", po::value<uint16_t>(), "server port");    //

    po::options_description display("Display options");
    display.add_options()                                                //
        ("fullscreen",                                                   //
         po::value<bool>(&cfg.display.fullscreen)->default_value(false), //
         "start client in fullscreen mode");                             //

    po::options_description all("Allowed options");
    all.add(replay).add(server).add(display);

    po::positional_options_description pos;
    pos.add("address", 1);
    pos.add("port", 1);

    bool force_help = false;
    po::variables_map vm;
    try {
        po::store(                              //
            po::command_line_parser(argc, argv) //
                .options(all)                   //
                .positional(pos)                //
                .run(),                         //
            vm);                                //
        po::notify(vm);
    } catch (po::unknown_option const&) {
        force_help = true;
    } catch (po::required_option const&) {
        force_help = true;
    }

    if (force_help || vm.count("help")) {
        fmt::print("Usage: {} [OPTION] [server_address server_port]\n", argv[0]);
        fmt::print("{}\n", replay);
        fmt::print("{}\n", server);
        fmt::print("{}\n", display);
        return {};
    }

    if (vm.count("replay") == 0) {
        cfg.record.filename = events_log_name();
    }

    if (vm.count("address") == 1 && vm.count("port") == 1) {
        string address = vm["address"].as<string>();
        uint16_t port  = vm["port"].as<uint16_t>();

        using namespace asio::ip;
        cfg.server = udp::endpoint{make_address(address), port};
    }
    return cfg;
}
