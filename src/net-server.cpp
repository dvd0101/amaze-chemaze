#include "models/entities.h"
#include "level/level_parse.h"
#include "world/stream.h"
#include "world/updater.h"
#include "world/world.h"
#include "net/thread.h"
#include "audio/audio.h"
#include <boost/program_options.hpp>
#include <asio/steady_timer.hpp>
#include <fstream>
#include <format/format.h>

using amaze::optional;

namespace a = amaze;

struct config {
    int duration;
};

optional<config> parse_cmdline(int argc, char* argv[]) {
    namespace po = boost::program_options;
    using std::string;
    config cfg;

    po::options_description desc("Allowed options");
    desc.add_options()                                                                                       //
        ("help", "produce help message");                                                                    //

    po::options_description play_duration("Play duration");
    play_duration.add_options()                                                                    //
        ("duration", po::value<int>(&cfg.duration)->default_value(10), "Play duration (seconds)"); //

    po::options_description all("Allowed options");
    all.add(desc).add(play_duration);

    bool force_help = false;
    po::variables_map vm;
    try {
        po::store(                              //
            po::command_line_parser(argc, argv) //
                .options(all)                   //
                .run(),                         //
            vm);                                //
        po::notify(vm);
    } catch (po::unknown_option const&) {
        force_help = true;
    } catch (po::required_option const&) {
        force_help = true;
    }

    if (force_help || vm.count("help")) {
        fmt::print("Usage: {} [OPTION]\n", argv[0]);
        fmt::print("{}\n", desc);
        fmt::print("{}\n", play_duration);
        return {};
    }
    return cfg;
}

asio::steady_timer timeout(asio::io_service& io, std::chrono::milliseconds ms, std::function<void()> callback) {
    asio::steady_timer t{io, ms};
    t.async_wait([callback](const std::error_code& ec) {
        if (ec == asio::error::operation_aborted) {
            return;
        }
        callback();
    });
    return std::move(t);
}

uint8_t select_outro(const a::World& world) {
    int32_t total = 0;
    for (auto& entry : world.scores()) {
        total += entry.second;
    }
    return total < 0 ? 1 : 2;
}

const a::player_id_t player_id = 0;
const uint16_t server_port     = 5005;

const uint32_t intro_duration = soundLength("../assets/intro.wav");
const uint32_t outro_duration = std::max(soundLength("../assets/outro_1.wav"), soundLength("../assets/outro_2.wav"));
const uint32_t wakeup_duration = std::max(soundLength("../assets/wake_up.wav"), soundLength("../assets/wake_up_easter.wav"));

uint32_t play_duration;
uint32_t whole_duration;

int main(int argc, char **argv) {
    auto cfg = parse_cmdline(argc, argv);
    if (!cfg) {
        return 1;
    }

    play_duration = cfg->duration * 1000;
    whole_duration = intro_duration + play_duration;

    const std::chrono::milliseconds game_duration{whole_duration};
    const std::chrono::milliseconds game_almost_over_duration{whole_duration - wakeup_duration};

    fmt::print("setting play duration to {} ms\n", play_duration);

    a::Stream stream{player_id};
    a::World world;
    a::Updater<> updater{world, player_id};

    fmt::print("loading level...\n");
    {
        std::ifstream f{"../levels/level1.txt"};
        world.level() = amaze::level::parse(f);
    }

    asio::io_service io;
    auto server = a::net::make_server(io, server_port, world, stream);
    auto net = a::net::start_server_thread(io);

    bool running      = true;
    bool game_started = false;
    asio::steady_timer game_timeout{io};
    asio::steady_timer game_almost_timeout{io};

    server.start_listening();
    std::chrono::milliseconds d{16};
    a::Source& source = stream;
    while (running) {
        std::this_thread::sleep_for(d);
        while (auto event = source.pull()) {
            updater.process(*event);
            server.broadcast(*event);
            fmt::print("-> {}\n", *event);
            if (!game_started && a::event_code(*event) == 'S') {
                game_started = true;
                fmt::print("game begin, start countdown\n");
                game_timeout = timeout(io,
                        game_duration,
                        [&running, &server, &world]() {
                            fmt::print("game ends\n");
                            running = false;
                            // an event with player_id == is broadcasted to everyone
                            auto evt = a::Outro_starts{select_outro(world)};
                            server.broadcast(evt);
                        });
                game_almost_timeout = timeout(io,
                        game_almost_over_duration,
                        [&running, &server]() {
                            fmt::print("hurry, time is running out!\n");
                            server.broadcast(a::Hurry_up{});
                        });
            }
        }
    }

    io.stop();
    net.join();
    return 0;
}
