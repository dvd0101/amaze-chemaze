#pragma once
#include <string>
#include <ostream>
#include "geom.h"
#include "../level/level.h"
#include "../world/config.h"

namespace amaze {
    using player_id_t = int32_t;

    struct Player {
        Player();

        player_id_t id;

        // the position in the world coords
        Point position;
        // the position in the level coords
        Tile_coord tile;
        // where the player is looking to
        Sphere_point looks_to;
    };

    std::ostream& operator<<(std::ostream&, const Player&);

    struct Splat {
        static const uint8_t types = 4;

        Splat();
        Splat(Point, Direction);
        Splat(Point, Direction, time_point);

        Point center;
        Direction normal;
        time_point timestamp;
        uint8_t type;

        int64_t t_ms() const;
    };

    std::ostream& operator<<(std::ostream&, const Splat&);
}
