#pragma once
#include <ostream>
#include <glm/glm.hpp>
#include "../common.h"
#include "../gl/gl.h"
#include "../images/pixels.h"

namespace amaze {
    struct Direction : glm::vec3 {
        using glm::vec3::vec3;

        bool zero() const;
        explicit operator bool() const;
    };
    std::ostream& operator<<(std::ostream&, const Direction&);

    // a Point is just a vec of 3 floats
    struct Point : glm::vec3 {
        using glm::vec3::vec3;
    };
    static_assert(sizeof(Point) == 3 * sizeof(GLfloat), "Point size does not match expectations");
    std::ostream& operator<<(std::ostream&, const Point&);

    // a texture coordinate is just two floats
    struct Tex_coord : glm::vec2 {
        using glm::vec2::vec2;
    };
    static_assert(sizeof(Tex_coord) == 2 * sizeof(GLfloat), "Tex_coord size does not match expectations");

    struct Triangle {
        Point a, b, c;

        Direction normal() const;
    };
    std::ostream& operator<<(std::ostream&, const Triangle&);

    namespace cube_face {
        // these constants represents the faces of a cube;
        // the front/rear direction is along the y axis (front is the face with the greater y)
        // the bottom/top direction is along the z axis (top is the face with the greater z)
        // the left/right direction is along the x axis (right is the face with the greater x)
        static constexpr uint8_t front  = 1 << 0;
        static constexpr uint8_t rear   = 1 << 1;
        static constexpr uint8_t bottom = 1 << 2;
        static constexpr uint8_t top    = 1 << 3;
        static constexpr uint8_t left   = 1 << 4;
        static constexpr uint8_t right  = 1 << 5;

        //             +---------------+                     |
        //             |\              |\                    |
        //             | \             | \                   |
        //             |  \            |  \                  |
        //   Left      |   +---------------+                 |
        //   ------>   |   |           |   |                 |
        //             |   |           |   |     Right       |
        //             +---|-----------+   |  <-------       |
        //              \  |            \  |                 |
        //               \ |  Front      \ |                 |
        //                \|              \|                 |
        //                 +---------------+                 |
        //                                                   |
        //                         ^                         |
        //                         |                         |
        //                         |                         |
        //                         | Bottom                  |
    }

    struct Cube_triangle : Triangle {
        bool visible;
        uint8_t face;
        uint8_t hint;
    };
    std::ostream& operator<<(std::ostream&, const Cube_triangle&);

    struct Textured_point {
        Point p;
        Tex_coord t;
        GLint texture_id;
        Direction n;
    };

    static_assert(sizeof(Textured_point) == 5 * sizeof(GLfloat) + 1 * sizeof(GLint) + 1 * sizeof(glm::vec3),
                  "Textured_point size does not match expectations");

    struct Textured_triangle {
        Textured_point a, b, c;

        void setTexture(GLint tex_id) {
            a.texture_id = tex_id;
            b.texture_id = tex_id;
            c.texture_id = tex_id;
        }
    };
    std::ostream& operator<<(std::ostream&, const Textured_triangle&);

    struct Portal_triangle : Triangle {
        uint8_t image;
        rgb color;
        uint8_t level;
        Point cube_center;
    };
    std::ostream& operator<<(std::ostream&, const Portal_triangle&);

    struct Portal_textured_triangle {
        struct P : Textured_point {
            Point cube_center;
            GLint level;
        } a, b, c;

        void setTexture(GLint tex_id) {
            a.texture_id = tex_id;
            b.texture_id = tex_id;
            c.texture_id = tex_id;
        }
    };

    static_assert(sizeof(Portal_textured_triangle::P) ==
                      sizeof(Textured_point) + 3 * sizeof(GLfloat) + 1 * sizeof(GLint),
                  "Portal_textured_triangle::P size does not match expectations");

    static_assert(sizeof(Portal_textured_triangle) == 3 * sizeof(Portal_textured_triangle::P),
                  "Portal_textured_triangle size does not match expectations");

    struct Sphere_point {
        Sphere_point(float phi, float theta);

        // the azimuthal angle
        float phi;
        // the polar angle
        float theta;

        Direction dir() const;
        Direction dir_xy(float plane_z = 0) const;
    };
    std::ostream& operator<<(std::ostream&, const Sphere_point&);

    struct Tile_coord {
        /*
         * The logical coordinate of a tile inside a 2d level.
         *
         * `x` goes from left to right
         * `y` goes from top to bottom
         */
        uint16_t x;
        uint16_t y;
    };

    std::ostream& operator<<(std::ostream&, Tile_coord);

    struct Cube_coord {
        // The logical coordinate of a cube in a 3d world;
        //
        //                       /----/----/----/----/----/
        //                      /    /    /    /    /    /
        //                     /    /    /    /    /    /
        //                   +----+/----/----/----/----/
        //                  /    /|    /    /    /    /
        //                 /    / |   /    /    /    /
        //              +-+----+  +- /----/----/----/
        //             /  | 2  | /  /    /    /    /
        //            /   |    |/  /    /    /    /
        //           +----+----+ -/----/----/----/
        //           | 1  | /    /    /    /    /
        //           |    |/    /    /    /    /
        //           +----+----/----/----/----/
        //          /    /    /    /    /    /
        //         /    /    /    /    /    /
        //        /----/----/----/----/----/
        //       /    /    /    /    /    /
        //      /    /    /    /    /    /
        //     /----/----/----/----/----/
        //    O
        //
        // O is the origin of both the 2d and the 3d world
        //
        // The coordinates of the two cubes are the following:
        //
        // Cube #1
        // 2d (Tile_coord): 2, 0
        // 3d (Cube_coord): 2, 0, 0
        //
        // Cube #2
        // 2d (Tile_coord): 2, 1
        // 3d (Cube_coord): 2, 1, 1
        int32_t x;
        int32_t y;
        int32_t z;

        Cube_coord(int32_t a, int32_t b, int32_t c) : x{a}, y{b}, z{c} {}
        Cube_coord(Tile_coord coord, int32_t c) : x{coord.x}, y{coord.y}, z{c} {}
    };

    std::ostream& operator<<(std::ostream&, Cube_coord);

    struct Segment {
        Point a, b;
    };

    std::ostream& operator<<(std::ostream&, Segment);

    struct Plane {
        Point p;
        Direction n;
    };

    bool inside(const Triangle&, const Point&);
    optional<Point> intersect(const Segment&, const Plane&);
    optional<Point> intersect(const Segment&, const Triangle&);
}
