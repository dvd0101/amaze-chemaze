#pragma once
#include "../net/serdes.h"
#include "geom.h"

namespace amaze {
    template <>
    struct SerDes<Point> {
        using type = Point;

        template <typename Writer>
        void serialize(const Point& p, Writer& sink) const {
            sink << p.x;
            sink << p.y;
            sink << p.z;
        }

        template <typename Reader>
        Point deserialize(Reader& source) const {
            Point p;
            source >> p.x;
            source >> p.y;
            source >> p.z;
            return p;
        }
    };

    template <>
    struct SerDes<Direction> {
        using type = Direction;

        template <typename Writer>
        void serialize(const Direction& d, Writer& sink) const {
            sink << d.x;
            sink << d.y;
            sink << d.z;
        }

        template <typename Reader>
        Direction deserialize(Reader& source) const {
            Direction d;
            source >> d.x;
            source >> d.y;
            source >> d.z;
            return d;
        }
    };

    template <>
    struct SerDes<Tile_coord> {
        using type = Tile_coord;

        template <typename Writer>
        void serialize(const Tile_coord& p, Writer& sink) const {
            sink << p.x;
            sink << p.y;
        }

        template <typename Reader>
        Tile_coord deserialize(Reader& source) const {
            Tile_coord p;
            source >> p.x;
            source >> p.y;
            return p;
        }
    };

    template <>
    struct SerDes<Sphere_point> {
        using type = Sphere_point;

        template <typename Writer>
        void serialize(const Sphere_point& p, Writer& sink) const {
            sink << p.phi;
            sink << p.theta;
        }

        template <typename Reader>
        Sphere_point deserialize(Reader& source) const {
            Sphere_point p{0, 0};
            source >> p.phi;
            source >> p.theta;
            return p;
        }
    };
}
