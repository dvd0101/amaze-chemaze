#include "entities.h"
#include "format/format.h"

namespace amaze {
    Player::Player() : id{0}, position{0}, tile{0, 0}, looks_to{0, 0} {}

    std::ostream& operator<<(std::ostream& os, const Player& p) {
        fmt::print(os, "Player({})", p.id);
        return os;
    }

    Splat::Splat() = default;
    Splat::Splat(Point p, Direction d) : center{p}, normal{d}, type{0} {}
    Splat::Splat(Point p, Direction d, amaze::time_point tp) : center{p}, normal{d}, timestamp{tp}, type{0} {}

    int64_t Splat::t_ms() const {
        using namespace std::chrono;
        return duration_cast<milliseconds>(timestamp.time_since_epoch()).count();
    }

    std::ostream& operator<<(std::ostream& os, const Splat& s) {
        fmt::print(os, "Splat(center={}, normal{}, t={}, type={})", s.center, s.normal, s.t_ms(), s.type);
        return os;
    }
}
