#include "geom.h"
#include <cmath>
#include <format/format.h>
#include <glm/gtx/normal.hpp>

namespace amaze {
    bool Direction::zero() const {
        return x == 0 && y == 0 && z == 0;
    }

    Direction::operator bool() const {
        return !zero();
    }

    std::ostream& operator<<(std::ostream& os, const Direction& p) {
        fmt::print(os, "D({}, {}, {})", p.x, p.y, p.z);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Point& p) {
        fmt::print(os, "P({}, {}, {})", p.x, p.y, p.z);
        return os;
    }

    Direction Triangle::normal() const {
        return glm::triangleNormal(a, b, c);
    }

    std::ostream& operator<<(std::ostream& os, const Triangle& t) {
        fmt::print(os,
                   "T(A=[{},{},{}] B=[{},{},{}] C=[{},{},{}])",
                   t.a.x,
                   t.a.y,
                   t.a.z,
                   t.b.x,
                   t.b.y,
                   t.b.z,
                   t.c.x,
                   t.c.y,
                   t.c.z);
        return os;
    }

    namespace {
        const char* face_name(uint8_t f) {
            switch (f) {
            case cube_face::front:
                return "front";
            case cube_face::rear:
                return "rear";
            case cube_face::bottom:
                return "bottom";
            case cube_face::top:
                return "top";
            case cube_face::left:
                return "left";
            case cube_face::right:
                return "right";
            default:
                throw std::runtime_error{"unexpected value"};
            };
        }
    }

    std::ostream& operator<<(std::ostream& os, const Cube_triangle& t) {
        fmt::print(os,
                   "CT(A=[{},{},{}] B=[{},{},{}] C=[{},{},{}], visible={}, face={})",
                   t.a.x,
                   t.a.y,
                   t.a.z,
                   t.b.x,
                   t.b.y,
                   t.b.z,
                   t.c.x,
                   t.c.y,
                   t.c.z,
                   t.visible,
                   face_name(t.face));
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Textured_triangle& t) {
        fmt::print(os,
                   "TT(A=[{},{},{}; {},{}; {}] B=[{},{},{}; {},{}; {}] C=[{},{},{}; {},{}; {}])",
                   t.a.p.x,
                   t.a.p.y,
                   t.a.p.z,
                   t.a.t.x,
                   t.a.t.y,
                   t.a.texture_id,

                   t.b.p.x,
                   t.b.p.y,
                   t.b.p.z,
                   t.b.t.x,
                   t.b.t.y,
                   t.b.texture_id,

                   t.c.p.x,
                   t.c.p.y,
                   t.c.p.z,
                   t.c.t.x,
                   t.c.t.y,
                   t.c.texture_id);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Portal_triangle& t) {
        fmt::print(os,
                   "PortalTriangle(A=[{},{},{}] B=[{},{},{}] C=[{},{},{}], image={}, color={})",
                   t.a.x,
                   t.a.y,
                   t.a.z,
                   t.b.x,
                   t.b.y,
                   t.b.z,
                   t.c.x,
                   t.c.y,
                   t.c.z,
                   t.image,
                   t.color);
        return os;
    }

    Sphere_point::Sphere_point(float p, float t) : phi{p}, theta{t} {}

    Direction Sphere_point::dir() const {
        using namespace std;
        return {
            cos(phi) * cos(theta), //
            sin(phi) * cos(theta), //
            sin(theta)             //
        };
    }

    Direction Sphere_point::dir_xy(float plane_z) const {
        auto d = dir();
        d.z    = plane_z;

        // This is still a direction, the rest of the code expects it to have
        // length 1.
        d = glm::normalize(static_cast<glm::vec3>(d));

        return d;
    }

    std::ostream& operator<<(std::ostream& os, const Sphere_point& p) {
        fmt::print(os, "SP(phi={:.3} theta={:.3})", p.phi, p.theta);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, Tile_coord c) {
        fmt::print(os, "({}x{})", c.x, c.y);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, Segment s) {
        fmt::print(os, "({}, {})", s.a, s.b);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, Cube_coord cc) {
        fmt::print(os, "({}, {}, {})", cc.x, cc.y, cc.z);
        return os;
    }

    /*
     * bool inside(Triangle, Point);
     * Point intersect(Segment, Plane);
     * Point intersect(Segment, Triangle);
     *
     * These function are an implementation of the algorithm exposed here:
     * http://geomalgorithms.com/a06-_intersect-2.html
     */

    bool inside(const Triangle& T, const Point& p) {
        auto u = T.b - T.a;
        auto v = T.c - T.a;

        float uu = glm::dot(u, u);
        float uv = glm::dot(u, v);
        float vv = glm::dot(v, v);

        auto w   = p - T.a;
        float wu = glm::dot(w, u);
        float wv = glm::dot(w, v);
        float D  = uv * uv - uu * vv;

        // get and test parametric coords
        float s, t;
        s = (uv * wv - vv * wu) / D;
        if (s < 0.0 || s > 1.0) // I is outside T
            return false;
        t = (uv * wu - uu * wv) / D;
        if (t < 0.0 || (s + t) > 1.0) // I is outside T
            return false;

        return true;
    }

    optional<Point> intersect(const Segment& s, const Plane& q) {
        const double epsilon = 0.00001;

        glm::vec3 ray_dir = s.b - s.a;

        float a, b;
        {
            auto w0 = s.a - q.p;

            a = -1 * glm::dot(static_cast<glm::vec3>(q.n), w0);
            b = glm::dot(static_cast<glm::vec3>(q.n), ray_dir);
            if (std::abs(b) < epsilon) {
                // ray is parallel to plane
                // if (std::abs(a) < epsilon) {
                //  ray lies on the plane
                // }
                return {};
            }
        }

        float r = a / b;
        if (r < 0) {
            // ray goes away from the plane
            return {};
        }

        // intersection between the ray and plane
        Point I = s.a + r * ray_dir;
        return I;
    }

    optional<Point> intersect(const Segment& s, const Triangle& t) {
        Direction n = t.normal();
        if (!n) {
            // triangle is degenerate
            return {};
        }
        // intersection between the ray and the triangle plane
        auto I = intersect(s, Plane{t.a, n});
        if (!I) {
            return {};
        }
        if (!inside(t, *I)) {
            return {};
        }
        return I;
    }
}
