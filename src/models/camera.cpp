#include "camera.h"
#include "format/format.h"
#include <glm/gtc/matrix_transform.hpp>

namespace amaze {
    // clang-format off
    const glm::mat4 Camera::view_transform{
        -1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 1, 0,
         0, 0, 0, 1
    };
    // clang-format on

    Camera::Camera() : position_{0}, looks_to_{1, 0, 0}, view_{1}, proj_{1} {
    }

    void Camera::position(const Point& v) {
        position_ = v;
        update();
    }

    const Point& Camera::position() const {
        return position_;
    }

    void Camera::looks_to(const Direction& v) {
        looks_to_ = glm::normalize(static_cast<glm::vec3>(v));
        update();
    }

    const Direction& Camera::looks_to() const {
        return looks_to_;
    }

    void Camera::update(const Point& p, const Direction& d) {
        position_ = p;
        looks_to_ = d;
        update();
    }

    void Camera::proj(glm::mat4 const& m) {
        proj_ = m;
        update();
    }

    const glm::mat4& Camera::proj() const {
        return proj_;
    }

    const glm::mat4& Camera::view() const {
        return view_;
    }

    const glm::mat4& Camera::vp() const {
        return vp_;
    }

    void Camera::update() {
        view_ = view_transform * glm::lookAt(position_, position_ + looks_to_, glm::vec3{0, 0, 1});
        vp_ = proj_ * view_;
    }

    std::ostream& operator<<(std::ostream& os, const Camera& c) {
        fmt::print(os, "C({} -> {})", c.position(), c.looks_to());
        return os;
    }
}
