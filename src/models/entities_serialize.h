#pragma once
#include "../net/serdes.h"
#include "entities.h"
#include "geom_serialize.h"

namespace amaze {
    template <>
    struct SerDes<Player> {
        using type = Player;

        template <typename Writer>
        void serialize(const Player& p, Writer& sink) const {
            sink << p.id;
            sink << p.position;
            sink << p.tile;
            sink << p.looks_to;
        }

        template <typename Reader>
        Player deserialize(Reader& source) const {
            Player p;
            source >> p.id;
            source >> p.position;
            source >> p.tile;
            source >> p.looks_to;
            return p;
        }
    };

    template <>
    struct SerDes<Splat> {
        using type = Splat;

        template <typename Writer>
        void serialize(const Splat& s, Writer& sink) const {
            sink << s.center;
            sink << s.normal;
            sink << s.timestamp;
            sink << s.type;
        }

        template <typename Reader>
        Splat deserialize(Reader& source) const {
            Splat s;
            source >> s.center;
            source >> s.normal;
            source >> s.timestamp;
            source >> s.type;
            return s;
        }
    };
}
