#pragma once
#include <ostream>
#include <glm/glm.hpp>
#include "geom.h"

namespace amaze {
    class Camera {
      public:
        Camera();

        // transform matrix applied to the view matrix to adapt the world axis
        // to the camera
        static const glm::mat4 view_transform;

      public:
        void position(const Point&);
        const Point& position() const;

        void looks_to(const Direction&);
        const Direction& looks_to() const;

        void update(const Point&, const Direction&);

        void proj(const glm::mat4&);
        const glm::mat4& proj() const;

        const glm::mat4& view() const;

        const glm::mat4& vp() const;

      private:
        void update();

        Point position_;
        Direction looks_to_;
        glm::mat4 view_;
        glm::mat4 proj_;
        glm::mat4 vp_;
    };

    std::ostream& operator<<(std::ostream&, const Camera&);
}
