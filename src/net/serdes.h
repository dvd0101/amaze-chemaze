#pragma once
#include <cstdint>
#include <type_traits>

namespace amaze {

    template <typename T>
    using base_type = std::remove_reference_t<std::remove_cv_t<T>>;

    template <typename>
    struct must_fail : std::false_type {};

    template <typename T>
    struct SerDes {
        // A SerDes specialization is needed for every (non basic) type that
        // can be serialized and deserialized.
        //
        // This implementation implements matches every type and implements the
        // default behaviour: disable the ser/des.

        // the alias `type` must be equals to the specialization type
        // struct SerDes<Custome> {
        //   using type = Custome;
        //   ...
        // }
        using type = std::false_type;

        template <typename Writer>
        void serialize(const T&, Writer&) const {
            static_assert(must_fail<T>::value, "SerDes not specialized");
        }

        template <typename Reader>
        T deserialize(Reader&) const {
            static_assert(must_fail<T>::value, "SerDes not specialized");
        }
    };

    // has_serdes_t and has_serdes are SFINAE friendly templates that can be
    // used to determine if a type supports the ser/des protocol
    template <typename T>
    using has_serdes_t = typename std::is_same< //
        typename SerDes<T>::type,               //
        base_type<T>>;                          //

    template <typename T>
    constexpr bool has_serdes = has_serdes_t<T>::value;

    template <typename T, typename Writer, typename SFINAE = typename Writer::encoder_t>
    void serialize(const T& value, Writer& sink)
    // serialize the given value into the sink
    {
        SerDes<T> codec;
        codec.serialize(value, sink);
    }

    template <typename T, typename Reader, typename SFINAE = typename Reader::decoder_t>
    T deserialize(Reader& source)
    // deserialize a type T from the source
    {
        SerDes<T> codec;
        return codec.deserialize(source);
    }
}
