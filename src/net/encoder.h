#pragma once
#include <stdexcept>
#include <type_traits>
#include <vector>
#include <boost/endian/buffers.hpp>
#include "../world/config.h"
#include "../support/ct_map.h"
#include "serdes.h"

/*
 * This file provides an encoding/decoding facility defining the wire format of
 * our basic types.
 *
 * More complex types (like the events) are encoded as a sequence of basic
 * types.
 *
 * The encoder/decoder class uses an iterator to write/read from a generic
 * container::
 *
 *      std::vector<uint8_t> buffer;
 *      auto it = std::back_inserter(storage);
 *      auto w = encoder::Writer<decltype(it)>{it};
 *      w << 0.1f;
 *
 * two shortcut function are also provided:
 *
 *  auto w = make_writer(buffer);
 *  auto r = make_reader(buffer);
 *
 * The Reader class can be constructed (and the make_reader does it) specifying
 * a maximum length; as a consequence an exception is raised if a past-end read
 * is attempted.
 */

namespace amaze {
    namespace encoder {

        namespace details
            // this namespace is used to not pollute the public api with the
            // `using` and `namespace` declarations
            {

            using support::ct_map_no_default;
            using support::kv;
            namespace e = boost::endian;

            // XXX fix the float endianess
            struct float_no_endian {
                uint8_t buffer[4];

                float_no_endian() = default;

                explicit float_no_endian(float x) {
                    auto ptr  = reinterpret_cast<const uint8_t*>(&x);
                    buffer[0] = ptr[0];
                    buffer[1] = ptr[1];
                    buffer[2] = ptr[2];
                    buffer[3] = ptr[3];
                }

                float value() const {
                    return *(reinterpret_cast<const float*>(buffer));
                }

                const uint8_t* data() const {
                    return buffer;
                }
            };

            static_assert(sizeof(float_no_endian) == sizeof(float), "unexpected size");

            using basic_types = ct_map_no_default< //
                kv<char, e::big_int8_buf_t>,       //
                kv<uint8_t, e::big_uint8_buf_t>,   //
                kv<uint16_t, e::big_uint16_buf_t>, //
                kv<int32_t, e::big_int32_buf_t>,   //
                kv<int64_t, e::big_int64_buf_t>,   //
                kv<float, float_no_endian>         //
                >;                                 //
        }

        template <typename Basic_types, typename Iterator>
        class Basic_reader {
            // Basic_reader must be used to decode the single components of a message
            //
            // The list of the supported primitive types is specified with the
            // template argument `Basic_types`; a compile time map that assign
            // to every type an endianess class.
            //
            // The endianess class is a type that expose the interface of a
            // boost endian buffer.
            //
            // The `>>' operator is used to extract the value; it is
            // implemented as a template method to prevent unwanted casts.
            //
            // An attempt to read more than a certain amount of bytes
            // (specified in the constructor) raise an exception
          public:
            using decoder_t = std::true_type;

          public:
            Basic_reader(Iterator it, size_t c) : _it{it}, _counter{c} {}

            template <typename T, typename SFINAE = typename Basic_types::template get<T>::value>
            Basic_reader& operator>>(T& value) {
                using endian_type = typename Basic_types::template get<T>::value;

                endian_type raw;
                auto ptr = reinterpret_cast<uint8_t*>(&raw);
                for (size_t ix = 0; ix < sizeof(T); ix++) {
                    ptr[ix] = next();
                }
                value = raw.value();
                return *this;
            }

            Basic_reader& operator>>(std::vector<uint8_t>& value) {
                uint16_t size;
                *this >> size;
                value.resize(value.size() + size);
                for (uint16_t ix = 0; ix < size; ix++) {
                    uint8_t byte;
                    *this >> byte;
                    value.push_back(byte);
                }
                return *this;
            }

            template <typename T, typename SFINAE = typename Basic_types::template get<T>::value>
            Basic_reader& discard() {
                for (size_t ix = 0; ix < sizeof(T); ix++) {
                    next();
                }
                return *this;
            }

          private:
            uint8_t next() {
                if (_counter == 0) {
                    throw std::runtime_error{"[encoder] buffer underflow"};
                }
                --_counter;
                return *_it++;
            }

            Iterator _it;
            size_t _counter;
        };

        template <typename Basic_types, typename Iterator>
        class Basic_writer {
            // Basic_writer must be used to encode the single components of a message
            //
            // see Basic_reader
          public:
            using encoder_t = std::true_type;

          public:
            Basic_writer(Iterator it) : _it{it} {}

            template <typename T, typename SFINAE = typename Basic_types::template get<T>::value>
            Basic_writer& operator<<(const T& value) {
                using endian_type = typename Basic_types::template get<T>::value;

                endian_type buff{value};
                auto ptr = buff.data();
                for (size_t ix = 0; ix < sizeof(T); ix++) {
                    next(ptr[ix]);
                }
                return *this;
            }

            Basic_writer& operator<<(const std::vector<uint8_t>& value) {
                if (value.size() >= 65536) {
                    throw std::runtime_error{"vector too big"};
                }
                *this << static_cast<uint16_t>(value.size());
                for (auto byte : value) {
                    *this << byte;
                }
                return *this;
            }

          private:
            void next(uint8_t v) {
                *_it++ = v;
            }

            Iterator _it;
        };

        template <typename Iterator>
        class Reader : public Basic_reader<details::basic_types, Iterator> {
            // just an alias of our long base
            using base = Basic_reader<details::basic_types, Iterator>;

          public:
            using Basic_reader<details::basic_types, Iterator>::Basic_reader;

            Reader& operator>>(amaze::time_point& val) {
                using namespace std::chrono;

                int64_t ms_from_epoch;
                *this >> ms_from_epoch;

                std::chrono::milliseconds ms{ms_from_epoch};
                val = amaze::time_point{ms};
                return *this;
            }

            template <typename T>
            Reader& operator>>(T& val) {
                dispatch(val, std::integral_constant<bool, has_serdes<T>>{});
                return *this;
            }

          private:
            template <typename T>
            void dispatch(T& val, std::true_type) {
                val = deserialize<T>(*this);
            }

            template <typename T>
            void dispatch(T& val, std::false_type) {
                base::operator>>(val);
            }
        };

        template <typename Iterator>
        class Writer : public Basic_writer<details::basic_types, Iterator> {
            using base = Basic_writer<details::basic_types, Iterator>;

          public:
            using Basic_writer<details::basic_types, Iterator>::Basic_writer;

            Writer& operator<<(const amaze::time_point& val) {
                using namespace std::chrono;

                int64_t ms_from_epoch = duration_cast<milliseconds>(val.time_since_epoch()).count();
                *this << ms_from_epoch;

                return *this;
            }

            template <typename T>
            Writer& operator<<(const T& val) {
                dispatch(val, std::integral_constant<bool, has_serdes<T>>{});
                return *this;
            }

          private:
            template <typename T>
            void dispatch(const T& val, std::true_type) {
                serialize(val, *this);
            }

            template <typename T>
            void dispatch(const T& val, std::false_type) {
                base::operator<<(val);
            }
        };
    }

    template <typename Container>
    auto make_reader(const Container& storage) {
        using std::cbegin;

        auto it = cbegin(storage);
        return encoder::Reader<decltype(it)>{it, storage.size()};
    }

    template <typename Container>
    auto make_writer(Container& storage) {
        auto it = std::back_inserter(storage);
        return encoder::Writer<decltype(it)>{it};
    }

    template <typename T, template <typename...> class Container>
    void serialize(const T& value, Container<uint8_t>& sink)
    // serialize the given value into a container of uint8_t
    {
        auto w = make_writer(sink);
        serialize(value, w);
    }

    template <typename T, template <typename...> class Container>
    T deserialize(const Container<uint8_t>& source)
    // deserialize a type T from the container of uint8_t
    {
        auto r = make_reader(source);
        return deserialize<T>(r);
    }
}
