#include "thread.h"
#include "format/format.h"

namespace amaze {
    namespace net {
        std::thread start_server_thread(asio::io_service& io) {
            std::thread t{[&io]() {
                fmt::print("all your sockets are belong to us\n");
                auto w = asio::make_work(io);
                io.run();
                fmt::print("exiting the network thread\n");
            }};
            return std::move(t);
        }

        std::thread start_client_thread(asio::io_service& io) {
            std::thread t{[&io]() {
                fmt::print("reticulating splines\n");
                auto w = asio::make_work(io);
                io.run();
                fmt::print("exiting the network thread\n");
            }};
            return std::move(t);
        }
    }
}
