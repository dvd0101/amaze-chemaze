#include "commands.h"

namespace amaze {
    namespace net {
        namespace commands {
            using std::ostream;

            ostream& operator<<(ostream& os, Ping) {
                fmt::print(os, "Ping()");
                return os;
            }

            ostream& operator<<(ostream& os, Pong) {
                fmt::print(os, "Pong()");
                return os;
            }

            Event::Event() = default;
            Event::Event(const amaze::Event& e) : evt{e} {}

            ostream& operator<<(ostream& os, const Event& cmd) {
                fmt::print(os, "Event({})", event_code(cmd.evt));
                return os;
            }

            Join::Join() = default;
            Join::Join(player_id_t v) : id{v} {}

            ostream& operator<<(ostream& os, const Join& cmd) {
                fmt::print(os, "Join({})", cmd.id);
                return os;
            }

            ostream& operator<<(ostream& os, Kicked) {
                fmt::print(os, "Kicked()");
                return os;
            }

            World_copy::World_copy() = default;
            World_copy::World_copy(const World& w) {
                world.players = w.players();
            }
            World_copy::World_copy(World_status ws) : world{std::move(ws)} {}

            ostream& operator<<(ostream& os, const World_copy&) {
                fmt::print(os, "WorldCopy()");
                return os;
            }
        }

        namespace {
            struct code_visitor : boost::static_visitor<char> {
                template <typename T>
                char operator()(const T&) const {
                    return T::code;
                }
            };
        }

        char command_code(const Command& cmd) {
            return boost::apply_visitor(code_visitor{}, cmd);
        }
    }
}
