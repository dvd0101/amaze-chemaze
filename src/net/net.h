#pragma once
#include "commands.h"
#include "commands_serialize.h"
#include "encoder.h"
#include "../models/entities.h"
#include "inline_variant_visitor/inline_variant.hpp"
#include <asio.hpp>
#include <cstdint>
#include <functional>
#include <map>
#include <vector>
#include <type_traits>

namespace amaze {
    class World;
    class Sink;
    namespace net {
        using asio::ip::udp;
        const size_t message_max_size = 4000;

        template <typename Derived, typename Recipient>
        class Process_message {
          public:
            void process_message(const std::vector<uint8_t>& message) {
                Command cmd;
                try {
                    cmd = deserialize<Command>(message);
                } catch (std::runtime_error& e) {
                    fmt::print("error decoding the message({}), discarded\n", e.what());
                    return;
                }
                fmt::print("message decoded: {}\n", cmd);
                forward(cmd);
            }

          private:
            /*
             * `forward()` forwards a Command to the appropriate
             * `process()` taking care of all the checking regarding the peer
             * type (if it must be known or unknown) and the recipient type (if
             * the command is intended for a server or a client);
             */
            void forward(const Command& cmd) {
                using namespace commands;
                match(cmd,
                      [this](Ping cmd) { forward(cmd); },
                      [this](Pong cmd) { forward(cmd); },
                      [this](const Event& cmd) { forward(cmd); },
                      [this](const Join& cmd) { forward(cmd); },
                      [this](const World_copy& cmd) { forward(cmd); },
                      [this](Kicked cmd) { forward(cmd); });
            }

            template <typename Cmd>
            void forward(const Cmd& cmd) {
                forward(cmd, typename Cmd::recipient{});
            }

            template <typename T>
            using check = typename std::enable_if<std::is_same<typename T::recipient, Recipient>::value>::type;

            template <typename Cmd, typename T = check<Cmd>>
            void forward(const Cmd& cmd, commands::only_client) {
                static_cast<Derived*>(this)->process_command(cmd);
            }

            template <typename Cmd, typename T = check<Cmd>>
            void forward(const Cmd& cmd, commands::only_server) {
                static_cast<Derived*>(this)->process_command(cmd);
            }

            template <typename Cmd>
            void forward(const Cmd& cmd, commands::both_server_client) {
                static_cast<Derived*>(this)->process_command(cmd);
            }

            template <typename Cmd, typename T>
            void forward(const Cmd&, T) {
                fmt::print("error this command its not intended for me\n");
            }
        };

        class Server : protected Process_message<Server, commands::only_server> {
          public:
            Server(udp::socket, const World&, Sink&);

            const udp::socket& socket() const;

            void start_listening();

            void broadcast(const Event&);

          private:
            void send_command(player_id_t, const Command&);
            void send_command(const udp::endpoint&, const Command&);

          private:
            template <typename Cmd>
            void process_command(const Cmd&);
            friend class Process_message<Server, commands::only_server>;

            template <typename Cmd>
            void process_command(const Cmd&, commands::known_peer);
            template <typename Cmd>
            void process_command(const Cmd&, commands::unknown_peer);

            void process(commands::Ping);
            void process(commands::Pong);
            void process(const commands::Event&);
            void process(const commands::Join&);

          private:
            player_id_t find_remote_player(const udp::endpoint&) const;

          private:
            udp::socket _socket;
            bool _listening;

            struct {
                player_id_t id;
                udp::endpoint peer;
            } _remote;
            std::map<player_id_t, udp::endpoint> _peers;

          private:
            const World& _world;
            Sink& _sink;
        };

        class Client : protected Process_message<Client, commands::only_client> {
          public:
            using reset_world_t = std::function<void(const World_status&)>;

          public:
            Client(asio::io_service&, Sink&, reset_world_t);

            void join(amaze::player_id_t, const udp::endpoint&);
            bool joined() const;

            void broadcast(const Event&);

          private:
            void start_listening();

            template <typename T>
            void broadcast(const T&, Natural_tag);

            template <typename T>
            void broadcast(const T&, Synthetic_tag);

          private:
            template <typename Cmd>
            void process_command(const Cmd&);
            friend class Process_message<Client, commands::only_client>;

            void process(commands::Ping);
            void process(commands::Pong);
            void process(const commands::Event&);
            void process(const commands::World_copy&);
            void process(commands::Kicked);

          private:
            asio::io_service& _io;
            udp::socket _socket;

          private:
            Sink& _sink;
            reset_world_t _reset_world;
        };

        Server make_server(asio::io_service&, uint16_t port, const World&, Sink&);
    }
}
