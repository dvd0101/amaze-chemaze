#pragma once
#include <asio.hpp>
#include <thread>
#include "net.h"

namespace amaze {
    class World;
    class Sink;

    namespace net {
        std::thread start_server_thread(asio::io_service&);
        std::thread start_client_thread(asio::io_service&);
    }
}
