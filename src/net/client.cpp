#include "net.h"
#include "commands_serialize.h"
#include "encoder.h"
#include "../world/stream.h"
#include <asio/spawn.hpp>
#include <asio/steady_timer.hpp>
#include "format/format.h"

namespace amaze {
    namespace net {
        namespace {
            using ms      = std::chrono::milliseconds;
            using timeout = asio::steady_timer;

            timeout make_timeout(udp::socket& socket) {
                timeout t{socket.get_io_service(), ms{5000}};
                t.async_wait([&socket](const std::error_code& ec) {
                    if (ec == asio::error::operation_aborted) {
                        return;
                    }
                    fmt::print("timer expired: {}\n", ec);
                    socket.cancel();
                });
                return std::move(t);
            }

            void send_command(udp::socket& socket, const Command& cmd, asio::yield_context yield) {
                std::vector<uint8_t> buffer;

                serialize(cmd, buffer);
                socket.async_send(asio::buffer(buffer), yield);
            }

            void send_command(udp::socket& socket, const Command& cmd) {
                auto f = [&socket, cmd](asio::yield_context yield) { //
                    send_command(socket, cmd, yield);                //
                };                                                   //
                asio::spawn(socket.get_executor(), std::move(f));
            }

            Command receive_command(udp::socket& socket, asio::yield_context yield) {
                std::vector<uint8_t> buffer(4000);

                auto t       = make_timeout(socket);
                size_t bytes = socket.async_receive(asio::buffer(buffer), yield);
                buffer.resize(bytes);

                return deserialize<Command>(buffer);
            }

            Command send_receive(udp::socket& socket, const Command& cmd, asio::yield_context yield) {
                send_command(socket, cmd, yield);
                return receive_command(socket, yield);
            }
        }

        Client::Client(asio::io_service& io, Sink& sink, reset_world_t r)
            : _io{io}, _socket{io}, _sink{sink}, _reset_world{r} {}

        void Client::join(amaze::player_id_t local_id, const udp::endpoint& remote) {
            auto f = [this, local_id, remote](asio::yield_context yield) {
                fmt::print("connecting... {}\n", remote);

                udp::socket socket{_io};
                socket.connect(remote);
                Command resp;
                try {
                    resp = send_receive(socket, commands::Join(local_id), yield);
                } catch (const std::system_error& e) {
                    fmt::print("connection failed: {}/{}\n", e.what(), e.code());
                    return;
                }

                auto ptr = boost::get<commands::World_copy>(&resp);
                if (ptr == nullptr) {
                    fmt::print("unexpected response: {}. connection failed\n", resp);
                    return;
                }
                _socket = std::move(socket);
                process(*ptr);
                start_listening();
            };
            asio::spawn(_io, std::move(f));
        }

        bool Client::joined() const {
            return _socket.is_open();
        }

        void Client::broadcast(const Event& evt) {
            if (!joined()) {
                return;
            }
            match(evt,
                [this](Clock_ticks evt) { broadcast(evt, Clock_ticks::action_tag{}); },
                [this](Player_moves evt) { broadcast(evt, Player_moves::action_tag{}); },
                [this](Player_turns evt) { broadcast(evt, Player_turns::action_tag{}); },
                [this](Player_starts evt) { broadcast(evt, Player_starts::action_tag{}); },
                [this](Player_fires evt) { broadcast(evt, Player_fires::action_tag{}); },
                [this](Intro_starts evt) { broadcast(evt, Intro_starts::action_tag{}); },
                [this](Intro_ends evt) { broadcast(evt, Intro_ends::action_tag{}); },
                [this](Hurry_up evt) { broadcast(evt, Hurry_up::action_tag{}); },
                [this](Outro_starts evt) { broadcast(evt, Outro_starts::action_tag{}); },
                [this](Outro_ends evt) { broadcast(evt, Outro_ends::action_tag{}); });
        }

        template <typename T>
        void Client::broadcast(const T& evt, Natural_tag) {
            send_command(_socket, commands::Event{evt});
        }

        template <typename T>
        void Client::broadcast(const T&, Synthetic_tag) {
            // do nothing
        }

        void Client::start_listening() {
            auto f = [this](asio::yield_context yield) {
                std::vector<uint8_t> buffer(message_max_size);
                std::error_code ec;

                fmt::print("start listening\n");
                while (joined()) {
                    buffer.resize(message_max_size);
                    size_t bytes = _socket.async_receive(asio::buffer(buffer), yield[ec]);

                    if (ec) {
                        fmt::print("udp read error: {}\n", ec);
                        continue;
                    }

                    if (bytes == 0) {
                        fmt::print("empty udp message discarded\n");
                        continue;
                    }

                    buffer.resize(bytes);

                    process_message(buffer);
                }
                fmt::print("udp read loop ended\n");
            };

            asio::spawn(_socket.get_executor(), std::move(f));
        }

        template <typename Cmd>
        void Client::process_command(const Cmd& cmd) {
            process(cmd);
        }

        void Client::process(commands::Ping) {}
        void Client::process(commands::Pong) {}
        void Client::process(const commands::Event& cmd) {
            _sink.push(cmd.evt);
        }
        void Client::process(const commands::World_copy& cmd) {
            _reset_world(cmd.world);
        }
        void Client::process(commands::Kicked) {
            _socket.close();
        }
    }
}
