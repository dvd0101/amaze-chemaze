#include "catch/catch.hpp"
#include "encoder.h"
#include <cstdint>
#include <vector>

using namespace std::chrono;
using namespace amaze;

SCENARIO("encoder::Reader is used to decode a message field", "[net]") {

    GIVEN("A buffer that holds the message bytes in big endian order") {

        WHEN("the buffer contains a char") {
            std::vector<uint8_t> buffer = {0x41};

            THEN("the value is correctly decoded") {
                char val;
                auto r = make_reader(buffer);
                r >> val;
                REQUIRE(val == 'A');
            }
        }

        WHEN("the buffer contains an int32") {
            std::vector<uint8_t> buffer = {0x1, 0x0, 0x0, 0x0};

            THEN("the value is correctly decoded") {
                int32_t val;
                auto r = make_reader(buffer);
                r >> val;
                REQUIRE(val == 16777216);
            }
        }

        WHEN("the buffer contains an int64") {
            std::vector<uint8_t> buffer = {0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};

            THEN("the value is correctly decoded") {
                int64_t val;
                auto r = make_reader(buffer);
                r >> val;
                REQUIRE(val == 72057594037927936);
            }
        }

        WHEN("the buffer contains a float") {
            std::vector<uint8_t> buffer = {0x0, 0x0, 0xc0, 0x3f};

            THEN("the value is correctly decoded") {
                float val;
                auto r = make_reader(buffer);
                r >> val;
                REQUIRE(val == 1.5f);
            }
        }

        WHEN("the buffer contains a time point (as an int64)") {
            // ten milliseconds after the epoch
            std::vector<uint8_t> buffer = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0a};

            THEN("the value is correctly decoded") {
                amaze::time_point val;
                auto r = make_reader(buffer);
                r >> val;

                auto ms = duration_cast<milliseconds>(val.time_since_epoch());
                REQUIRE(ms.count() == 10);
            }
        }
    }

    GIVEN("a buffer that holds a fixed amount of data") {
        std::vector<uint8_t> buffer = {0x41};

        WHEN("an attempt is made to read more bytes than are available") {

            THEN("an exception is raised") {
                char val;
                auto r = make_reader(buffer);
                r >> val;
                REQUIRE_THROWS_AS(r >> val, std::runtime_error);
            }
        }
    }
}

SCENARIO("encoder::Writer is used to encode a message field", "[net]") {
    GIVEN("an empty buffer and a writer") {
        std::vector<uint8_t> buffer;
        auto w = make_writer(buffer);

        WHEN("a char is encoded") {
            w << 'A';
            THEN("the buffer contains the value correctly encoded") {
                REQUIRE(buffer.size() == 1);
                REQUIRE(buffer[0] == 'A');
            }
        }

        WHEN("an int32 is encoded") {
            int32_t val;
            uint8_t* ptr = (uint8_t*)&val;
            ptr[0]       = 0x01;
            ptr[1]       = 0x02;
            ptr[2]       = 0x03;
            ptr[3]       = 0x04;
            w << val;
            THEN("the buffer contains the value correctly encoded") {
                REQUIRE(buffer.size() == 4);
                REQUIRE(buffer[0] == 0x04);
                REQUIRE(buffer[1] == 0x03);
                REQUIRE(buffer[2] == 0x02);
                REQUIRE(buffer[3] == 0x01);
            }
        }

        WHEN("an int64 is encoded") {
            int64_t val;
            uint8_t* ptr = (uint8_t*)&val;
            ptr[0]       = 0x01;
            ptr[1]       = 0x02;
            ptr[2]       = 0x03;
            ptr[3]       = 0x04;
            ptr[4]       = 0x05;
            ptr[5]       = 0x06;
            ptr[6]       = 0x07;
            ptr[7]       = 0x08;
            w << val;
            THEN("the buffer contains the value correctly encoded") {
                REQUIRE(buffer.size() == 8);
                REQUIRE(buffer[0] == 0x08);
                REQUIRE(buffer[1] == 0x07);
                REQUIRE(buffer[2] == 0x06);
                REQUIRE(buffer[3] == 0x05);
                REQUIRE(buffer[4] == 0x04);
                REQUIRE(buffer[5] == 0x03);
                REQUIRE(buffer[6] == 0x02);
                REQUIRE(buffer[7] == 0x01);
            }
        }

        WHEN("a time_point is encoded") {
            amaze::time_point val{milliseconds{10}};
            w << val;
            THEN("the buffer contains the value correctly encoded") {
                REQUIRE(buffer.size() == 8);
                REQUIRE(buffer[0] == 0x00);
                REQUIRE(buffer[1] == 0x00);
                REQUIRE(buffer[2] == 0x00);
                REQUIRE(buffer[3] == 0x00);
                REQUIRE(buffer[4] == 0x00);
                REQUIRE(buffer[5] == 0x00);
                REQUIRE(buffer[6] == 0x00);
                REQUIRE(buffer[7] == 0x0a);
            }
        }
    }
}
