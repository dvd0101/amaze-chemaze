#include "net.h"
#include "commands_serialize.h"
#include "encoder.h"
#include "../world/events_serialize.h"
#include "../world/stream.h"
#include "../world/world.h"
#include "../world/world_serialize.h"
#include <system_error>
#include <asio/spawn.hpp>
#include "format/format.h"

using std::begin;
using std::end;

namespace amaze {
    namespace net {
        Server::Server(udp::socket s, const World& world, Sink& sink)
            : _socket{std::move(s)}, _listening{false}, _world{world}, _sink{sink} {}

        const udp::socket& Server::socket() const {
            return _socket;
        }

        void Server::start_listening() {
            auto f = [this](asio::yield_context yield) {
                std::vector<uint8_t> buffer(message_max_size);
                std::error_code ec;

                std::string host = _socket.local_endpoint().address().to_string();
                uint16_t port = _socket.local_endpoint().port();
                fmt::print("server starts listening on {}:{}\n", host, port);

                while (_listening) {
                    fmt::print("waiting for an udp message\n");

                    // use all the buffer capacity to store the next message
                    buffer.resize(message_max_size);
                    size_t bytes = _socket.async_receive_from(asio::buffer(buffer), _remote.peer, yield[ec]);

                    if (ec) {
                        fmt::print("udp read error: {}\n", ec);
                        continue;
                    }

                    if (bytes == 0) {
                        fmt::print("empty udp message discarded\n");
                        continue;
                    }

                    // resizes the buffer to have the size (not the capacity)
                    // equals to the number of bytes read. It never requires an
                    // heap allocation.
                    buffer.resize(bytes);

                    fmt::print("packet received. sender={} bytes={}\n", _remote.peer, bytes);
                    process_message(buffer);
                }
                fmt::print("udp read loop ended\n");
            };

            _listening = true;
            asio::spawn(_socket.get_executor(), std::move(f));
        };

        void Server::send_command(player_id_t id, const Command& cmd) {
            return send_command(_peers.at(id), cmd);
        }

        void Server::send_command(const udp::endpoint& remote, const Command& cmd) {
            auto f = [this, &remote, cmd](asio::yield_context yield) {

                std::vector<uint8_t> buffer;
                serialize(cmd, buffer);

                fmt::print("sending {} to {} ({} bytes)\n", cmd, remote, buffer.size());
                std::error_code ec;
                _socket.async_send_to(asio::buffer(buffer), remote, yield[ec]);
                if (ec) {
                    fmt::print("sending message failed: {}\n", ec);
                }
            };
            asio::spawn(_socket.get_executor(), std::move(f));
        }

        void Server::broadcast(const Event& evt) {
            auto player = event_player(evt);
            for (auto& it : _peers) {
                if (it.first != player) {
                    send_command(it.second, commands::Event{evt});
                }
            }
        }

        template <typename Cmd>
        void Server::process_command(const Cmd& cmd) {
            process_command(cmd, typename Cmd::peer_type{});
        }

        template <typename Cmd>
        void Server::process_command(const Cmd& cmd, commands::known_peer) {
            _remote.id = find_remote_player(_remote.peer);
            if (_remote.id == 0) {
                fmt::print("error this command requires a known peer, discarded\n");
                return;
            }
            process(cmd);
            _remote.id = 0;
        }

        template <typename Cmd>
        void Server::process_command(const Cmd& cmd, commands::unknown_peer) {
            player_id_t id = find_remote_player(_remote.peer);
            if (id != 0) {
                fmt::print("error this command requires an unknown peer, discarded\n");
                return;
            }
            process(cmd);
        }

        void Server::process(commands::Ping) {}
        void Server::process(commands::Pong) {}
        void Server::process(const commands::Event& cmd) {
            broadcast(cmd.evt);
        }
        void Server::process(const commands::Join& cmd) {
            try {
                _world.player(cmd.id);
                // a player with the same id is already registered (but it
                // isn't listed in the _peers map)
                fmt::print("a player with the same id is already registered\n");
                send_command(_remote.peer, commands::Kicked{});
                return;
            } catch (std::runtime_error&) {
            }

            if (_world.full()) {
                fmt::print("world is full\n");
                send_command(_remote.peer, commands::Kicked{});
                return;
            }
            _peers.insert({cmd.id, _remote.peer});
            _sink.push(Player_starts{cmd.id});
            send_command(_remote.peer, commands::World_copy{_world});
        }

        player_id_t Server::find_remote_player(const udp::endpoint& peer) const {
            auto cmp = [&peer](const auto& value) { return value.second == peer; };
            auto it = std::find_if(begin(_peers), end(_peers), cmp);
            return it == end(_peers) ? 0 : it->first;
        }

        Server make_server(asio::io_service& io, uint16_t port, const World& world, Sink& sink) {
            udp::socket s{io, {udp::v4(), port}};
            return Server{std::move(s), world, sink};
        }
    }
}
