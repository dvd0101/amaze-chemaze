#pragma once
#include "serdes.h"
#include "commands.h"
#include "../world/events_serialize.h"
#include "../world/world_serialize.h"
#include <boost/variant/static_visitor.hpp>
#include "format/format.h"

namespace amaze {
    namespace net {
        namespace details {
            template <typename Writer>
            struct Serialize_visitor : boost::static_visitor<> {
                Serialize_visitor(Writer& o) : sink{o} {}

                void operator()(commands::Ping) {}
                void operator()(commands::Pong) {}
                void operator()(const commands::Event& cmd) {
                    amaze::serialize(cmd.evt, sink);
                }
                void operator()(const commands::Join& cmd) {
                    sink << cmd.id;
                }
                void operator()(const commands::World_copy& cmd) {
                    amaze::serialize(cmd.world, sink);
                }
                void operator()(commands::Kicked) {}

                Writer& sink;
            };

            template <typename Reader>
            class Deserialize_visitor : boost::static_visitor<> {
              public:
                Deserialize_visitor(Reader& i) : source{i} {}

                void operator()(commands::Ping&) {}
                void operator()(commands::Pong&) {}
                void operator()(commands::Event& cmd) {
                    cmd.evt = deserialize<amaze::Event>(source);
                }
                void operator()(commands::Join& cmd) {
                    source >> cmd.id;
                }
                void operator()(commands::World_copy& cmd) {
                    cmd.world = deserialize<amaze::World_status>(source);
                }
                void operator()(commands::Kicked&) {}

                Reader& source;
            };
        }
    }

    template <>
    struct SerDes<net::Command> {
        template <typename Writer>
        void serialize(const net::Command& cmd, Writer& sink) const {
            sink << net::command_code(cmd);
            net::details::Serialize_visitor<Writer> v{sink};
            boost::apply_visitor(v, cmd);
        }

        template <typename Reader>
        net::Command deserialize(Reader& source) const {
            namespace commands = net::commands;

            char marker;
            source >> marker;

            net::Command cmd;
            switch (marker) {
            case '0':
                cmd = commands::Ping{};
                break;
            case '1':
                cmd = commands::Pong{};
                break;
            case 'E':
                cmd = commands::Event{};
                break;
            case 'J':
                cmd = commands::Join{};
                break;
            case 'W':
                cmd = commands::World_copy{};
                break;
            case 'K':
                cmd = commands::Kicked{};
                break;
            default:
                throw std::runtime_error{fmt::format("unexpected marker: {}\n", marker)};
            };
            net::details::Deserialize_visitor<Reader> v{source};
            boost::apply_visitor(v, cmd);
            return cmd;
        }
    };
}
