#pragma once
#include "../models/entities.h"
#include "../world/events.h"
#include "../world/world.h"
#include <boost/variant/variant.hpp>
#include <iostream>
#include <type_traits>

namespace amaze {
    namespace net {
        namespace commands {
            struct unknown_peer {};
            struct known_peer {};
            struct only_server {};
            struct only_client {};
            struct both_server_client {};

            namespace details {
                template <char Code, typename Peer, typename Recipient>
                struct command {
                    static constexpr char code = Code;

                    using peer_type = Peer;
                    using recipient = Recipient;
                };
            }

            struct Ping : details::command<'0', known_peer, both_server_client> {};
            std::ostream& operator<<(std::ostream&, Ping);

            struct Pong : details::command<'1', known_peer, both_server_client> {};
            std::ostream& operator<<(std::ostream&, Pong);

            struct Event : details::command<'E', known_peer, both_server_client> {
                Event();
                Event(const amaze::Event&);

                amaze::Event evt;
            };
            std::ostream& operator<<(std::ostream&, const Event&);

            struct Join : details::command<'J', unknown_peer, only_server> {
                Join();
                Join(player_id_t);

                player_id_t id;
            };
            std::ostream& operator<<(std::ostream&, const Join&);

            struct Kicked : details::command<'K', known_peer, only_client> {};
            std::ostream& operator<<(std::ostream&, Kicked);

            struct World_copy : details::command<'W', known_peer, only_client> {
                World_copy();
                World_copy(const World&);
                World_copy(World_status);

                World_status world;
            };
            std::ostream& operator<<(std::ostream&, const World_copy&);
        }

        using Command = boost::variant< //
            commands::Ping,             //
            commands::Pong,             //
            commands::Event,            //
            commands::Join,             //
            commands::World_copy,       //
            commands::Kicked>;          //

        char command_code(const Command&);
    }
}
