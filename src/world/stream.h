#pragma once
#include "../common.h"
#include "events.h"
#include <concurrentqueue/concurrentqueue.h>

namespace amaze {
    namespace details{
        class Sink_access;
        class Source_access;
    };

    class Source {
      public:
        virtual ~Source() = default;
        optional<Event> pull();

      private:
        virtual optional<Event> dequeue() = 0;

      private:
        friend class details::Source_access;
    };

    class Sink {
      public:
        virtual ~Sink() = default;
        void push(Event);

      private:
        virtual void process(Event&);
        virtual void enqueue(const Event&) = 0;

      private:
        friend class details::Sink_access;
    };

    class Stream : public Source, public Sink {
        /*
         * A Stream is a thread-safe implementation of both a source and a sink
         */
      public:
        Stream(int32_t);
        ~Stream() override = default;

      private:
        optional<Event> dequeue() override;

        void process(Event&) override;
        void enqueue(const Event&) override;

      private:
        int32_t local_player_id;
        moodycamel::ConcurrentQueue<Event> queue;
    };
}
