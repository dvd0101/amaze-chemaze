#pragma once
#include "config.h"
#include <chrono>
#include <boost/variant/variant.hpp>
#include <ostream>
#include <format/format.h>

namespace amaze {
    using player_id_t = int32_t;

    struct Natural_tag {
        // Denotes an action initiated by the user
    };
    struct Synthetic_tag {
        // Denotes an action initiated by the game itself
    };

    template <typename TAG, char CODE>
    struct Action_base {
        // the kind of this action
        using action_tag = TAG;

        static constexpr char action_code = CODE;

        Action_base(player_id_t p) : t{amaze::now()}, player{p} {}
        Action_base() : Action_base{0} {}

        // when this action has been created
        // the_clock::time_point t;
        amaze::time_point t;

        // the player that originated this action
        player_id_t player;

        int64_t t_ms() const {
            using namespace std::chrono;
            return duration_cast<milliseconds>(t.time_since_epoch()).count();
        }

        bool operator==(const Action_base<TAG, CODE>& other) const {
            return t == other.t && player == other.player;
        }
    };

    template <typename TAG, char CODE>
    constexpr char Action_base<TAG, CODE>::action_code;

    template <char CODE>
    using Natural_action = Action_base<Natural_tag, CODE>;

    template <char CODE>
    using Synthetic_action = Action_base<Synthetic_tag, CODE>;

    enum class Move_dir : uint8_t { forward = 0, right = 1, backward = 2, left = 3 };
    std::ostream& operator<<(std::ostream&, Move_dir);

    struct Player_moves : Natural_action<'M'> {
        using parent = Natural_action<'M'>;

        Player_moves();
        Player_moves(player_id_t);
        Player_moves(Move_dir);

        Move_dir direction;

        bool operator==(const Player_moves&) const;
    };
    std::ostream& operator<<(std::ostream&, const Player_moves&);

    struct Player_turns : Natural_action<'D'> {
        using parent = Natural_action<'D'>;

        Player_turns();
        Player_turns(player_id_t);
        Player_turns(float, float);

        float delta_phi;
        float delta_theta;

        bool operator==(const Player_turns&) const;
    };
    std::ostream& operator<<(std::ostream&, const Player_turns&);

    struct Player_fires : Natural_action<'F'> {
        using parent = Natural_action<'F'>;
    };

    template <typename OUT>
    OUT& operator<<(OUT& os, const Player_fires& v) {
        fmt::print(os, "Player_fires<{}>(player={} t={}ms)", Player_fires::action_code, v.player, v.t_ms());
        return os;
    }

    struct Clock_ticks : Synthetic_action<'T'> {
        using parent = Synthetic_action<'T'>;

        // tempus fugit
    };
    std::ostream& operator<<(std::ostream&, const Clock_ticks&);

    struct Player_starts : Natural_action<'S'> {
        using parent = Natural_action<'S'>;
        using parent::Action_base;
    };
    std::ostream& operator<<(std::ostream&, const Player_starts&);

    struct Intro_starts : Synthetic_action<'I'> {
        using parent = Synthetic_action<'I'>;
    };
    std::ostream& operator<<(std::ostream&, const Intro_starts&);

    struct Intro_ends : Synthetic_action<'J'> {
        using parent = Synthetic_action<'J'>;
    };
    std::ostream& operator<<(std::ostream&, const Intro_ends&);

    struct Hurry_up : Synthetic_action<'H'> {
        using parent = Synthetic_action<'H'>;
    };
    std::ostream& operator<<(std::ostream&, const Hurry_up&);

    struct Outro_starts : Synthetic_action<'Y'> {
        using parent = Synthetic_action<'Y'>;

        Outro_starts();
        Outro_starts(player_id_t);
        Outro_starts(uint8_t);

        uint8_t index;
    };
    std::ostream& operator<<(std::ostream&, const Outro_starts&);

    struct Outro_ends : Synthetic_action<'Z'> {
        using parent = Synthetic_action<'Z'>;
    };
    std::ostream& operator<<(std::ostream&, const Outro_ends&);

    using Event = boost::variant< //
        Clock_ticks,              //
        Player_moves,             //
        Player_turns,             //
        Player_starts,            //
        Player_fires,             //
        Intro_starts,             //
        Intro_ends,               //
        Hurry_up,                 //
        Outro_starts,             //
        Outro_ends>;              //

    char event_code(const Event&);
    int32_t event_player(const Event&);
    time_point event_time(const Event&);
    void event_timewarp(Event&, std::chrono::milliseconds);
}
