#include "input_source.h"
#include "stream.h"
#include <format/format.h>

namespace amaze {
    using interval_t = std::chrono::milliseconds;

    // Player is only allowed to shoot twice per second.
    static constexpr long weapon_reload = 500;

    Input_source::Input_source(interval_t i, Sink& s) : interval{i}, sink{s} {
        reset();
    }

    void Input_source::move_forward() {
        forward = true;
    }

    void Input_source::move_right() {
        right = true;
    }

    void Input_source::move_backward() {
        backward = true;
    }

    void Input_source::move_left() {
        left = true;
    }

    void Input_source::change_direction(float dp, float dt) {
        delta_phi   = dp;
        delta_theta = dt;
    }

    void Input_source::fire_pressed() {
        fire = true;
    }

    bool Input_source::touch() {
        auto t1 = clock::now();
        if (t1 - t < interval) {
            return false;
        }
        generate_events();
        reset();
        return true;
    }

    void Input_source::generate_events() {
        if (forward) {
            sink.push(Player_moves{Move_dir::forward});
        }
        if (right) {
            sink.push(Player_moves{Move_dir::right});
        }
        if (backward) {
            sink.push(Player_moves{Move_dir::backward});
        }
        if (left) {
            sink.push(Player_moves{Move_dir::left});
        }
        if (delta_phi || delta_theta) {
            sink.push(Player_turns{delta_phi, delta_theta});
        }
        if (fire) {
            // Before emitting the event we check the reload time
            auto now = clock::now();
            auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(now - last_shot);

            if (delta.count() > weapon_reload) {
                sink.push(Player_fires{});
                last_shot = now;
            }
        }
    }

    void Input_source::reset() {
        t           = clock::now();
        forward     = false;
        right       = false;
        backward    = false;
        left        = false;
        delta_phi   = 0;
        delta_theta = 0;
        fire        = false;
    }
}
