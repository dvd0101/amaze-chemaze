#include "events.h"
#include <boost/variant/static_visitor.hpp>

namespace amaze {
    std::ostream& operator<<(std::ostream& os, Move_dir v) {
        switch (v) {
        case Move_dir::forward:
            os << "forward";
            break;
        case Move_dir::right:
            os << "right";
            break;
        case Move_dir::backward:
            os << "backward";
            break;
        case Move_dir::left:
            os << "left";
            break;
        }
        return os;
    }

    Player_moves::Player_moves() : Player_moves{0} {}
    Player_moves::Player_moves(player_id_t p) : parent{p}, direction{Move_dir::forward} {}
    Player_moves::Player_moves(Move_dir d) : parent{}, direction{d} {}

    bool Player_moves::operator==(const Player_moves& other) const {
        return parent::operator==(other) && direction == other.direction;
    }

    std::ostream& operator<<(std::ostream& os, const Player_moves& v) {
        fmt::print(os,
                   "Player_moves<{}>(player={} t={}ms dir={})",
                   Player_moves::action_code,
                   v.player,
                   v.t_ms(),
                   v.direction);
        return os;
    }

    Player_turns::Player_turns() : Player_turns{0} {}
    Player_turns::Player_turns(player_id_t p) : parent{p}, delta_phi{0}, delta_theta{0} {}
    Player_turns::Player_turns(float p, float t) : parent{}, delta_phi{p}, delta_theta{t} {}

    bool Player_turns::operator==(const Player_turns& other) const {
        return parent::operator==(other) && delta_phi == other.delta_phi && delta_theta == other.delta_theta;
    }

    std::ostream& operator<<(std::ostream& os, const Player_turns& v) {
        fmt::print(os,
                   "Player_turns<{}>(player={} t={}ms delta_phi={} delta_theta={})",
                   Player_turns::action_code,
                   v.player,
                   v.t_ms(),
                   v.delta_phi,
                   v.delta_theta);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Clock_ticks& v) {
        fmt::print(os, "Clock_ticks<{}>(player={} t={}ms)", Clock_ticks::action_code, v.player, v.t_ms());
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Player_starts& v) {
        fmt::print(os, "Player_starts<{}>(player={} t={}ms)", Player_starts::action_code, v.player, v.t_ms());
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Intro_starts& v) {
        fmt::print(os, "Intro_starts<{}>(player={} t={}ms)", Intro_starts::action_code, v.player, v.t_ms());
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Intro_ends& v) {
        fmt::print(os, "Intro_ends<{}>(player={} t={}ms)", Intro_ends::action_code, v.player, v.t_ms());
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Hurry_up& v) {
        fmt::print(os, "Hurry_up<{}>(player={} t={}ms)", Hurry_up::action_code, v.player, v.t_ms());
        return os;
    }

    Outro_starts::Outro_starts() : Outro_starts{0} {}
    Outro_starts::Outro_starts(player_id_t p) : parent{p}, index{0} {}
    Outro_starts::Outro_starts(uint8_t i) : parent{}, index{i} {}

    std::ostream& operator<<(std::ostream& os, const Outro_starts& v) {
        fmt::print(os, "Outro_starts<{}>(player={} t={}ms)", Outro_starts::action_code, v.player, v.t_ms());
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Outro_ends& v) {
        fmt::print(os, "Outro_ends<{}>(player={} t={}ms)", Outro_ends::action_code, v.player, v.t_ms());
        return os;
    }

    namespace {
        struct code_visitor : boost::static_visitor<char> {
            template <typename T>
            char operator()(const T&) const {
                return T::action_code;
            }
        };

        struct player_visitor : boost::static_visitor<player_id_t> {
            template <typename T>
            player_id_t operator()(const T& evt) const {
                return evt.player;
            }
        };

        struct time_visitor : boost::static_visitor<time_point> {
            template <typename T>
            time_point operator()(const T& evt) const {
                return evt.t;
            }
        };

        struct timewarp_visitor : boost::static_visitor<void> {
            std::chrono::milliseconds warp;
            timewarp_visitor(std::chrono::milliseconds x) {
                warp = x;
            }
            template <typename T>
            void operator()(T& evt) const {
                evt.t += warp;
            }
        };
    }

    char event_code(const Event& evt) {
        return boost::apply_visitor(code_visitor{}, evt);
    }

    player_id_t event_player(const Event& evt) {
        return boost::apply_visitor(player_visitor{}, evt);
    };

    time_point event_time(const Event& evt) {
        return boost::apply_visitor(time_visitor{}, evt);
    };

    void event_timewarp(Event& evt, std::chrono::milliseconds warp) {
        boost::apply_visitor(timewarp_visitor{warp}, evt);
    };
}
