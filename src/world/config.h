#pragma once
#include <chrono>
#include <cstdint>
#include <random>

namespace amaze {
    constexpr double pi = 3.1415926535;

    using the_clock = std::chrono::system_clock;

    // define our time_points; we don't get the ns precision available on unix
    // because on windows the system/high_resolution clocks have 1ms step.
    using time_point = std::chrono::time_point<the_clock, std::chrono::milliseconds>;

    inline time_point now() {
        return std::chrono::time_point_cast<std::chrono::milliseconds>(the_clock::now());
    }

    extern std::mt19937 rnd;

    struct World_config {
        static constexpr float tile_size   = 1.0;
        static constexpr uint8_t pit_depth = 3;
        static constexpr float step_size   = 0.05;
        static constexpr double max_theta  = pi / 4 - pi / 10;
        static constexpr float player_height = 0.8;
    };
}
