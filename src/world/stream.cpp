#include "stream.h"
#include <boost/variant/static_visitor.hpp>

namespace amaze {
    optional<Event> Source::pull() {
        return dequeue();
    }

    void Sink::push(Event evt) {
        process(evt);
        enqueue(evt);
    }

    void Sink::process(Event&) {}

    Stream::Stream(int32_t id) : local_player_id{id} {}

    optional<Event> Stream::dequeue() {
        Event evt;
        bool found = queue.try_dequeue(evt);
        if (found) {
            return evt;
        }
        return {};
    }

    namespace {
        struct Event_visitor : boost::static_visitor<> {
            Event_visitor(player_id_t i) : id{i} {}

            template <typename T>
            void operator()(T& event) const {
                if (event.player == 0) {
                    event.player = id;
                }
            }

            player_id_t id;
        };
    }

    void Stream::process(Event& evt) {
        boost::apply_visitor(Event_visitor(local_player_id), evt);
    }

    void Stream::enqueue(const Event& evt) {
        queue.enqueue(evt);
    }
}
