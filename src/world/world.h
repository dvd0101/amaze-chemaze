#pragma once
#include "../models/camera.h"
#include "../models/entities.h"
#include "../level/level.h"
#include <vector>

namespace amaze {
    struct World_status {
        std::vector<Player> players;
        std::vector<Splat> splats;
        std::map<amaze::player_id_t, int32_t> scores;
    };

    class World {
      public:
        World();

        // The accessors
        //
        // The idea is that the code that setups the game holds an instance of a
        // World and uses the non const methods for the initial setup.
        //
        // After the initial setup only a const reference is passed around (for
        // example to the render thread), in this manner the World state cannot
        // be directly mutated
        level::Level& level();
        const level::Level& level() const;

        // returns the player given an id; throws if the id is not found
        Player& player(int32_t);
        const Player& player(int32_t) const;

        std::vector<Player>& players();
        const std::vector<Player>& players() const;

        std::vector<Splat>& splats();
        const std::vector<Splat>& splats() const;

        std::map<amaze::player_id_t, int32_t>& scores();
        const std::map<amaze::player_id_t, int32_t>& scores() const;

        Camera& camera();
        const Camera& camera() const;

        void applyScore(Player, int32_t);

        bool& intro_mode();
        bool intro_mode() const;

        uint8_t& outro_index();
        uint8_t outro_index() const;

        bool full() const;
        void operator=(const World_status&);

      private:
        // out World *HAS* all the elements we need to correctly define the
        // state of the game

        // the level we are playing
        level::Level _level;

        std::vector<Player> _players;
        std::vector<Splat> _splats;
        std::map<player_id_t, int32_t> _scores;

        // although not strictly necessary (because it can be derived from the
        // state of the player) is handy to have the camera because it can be
        // updated only when necessary
        Camera _camera;

        bool _intro_mode;
        uint8_t _outro_index;
    };
}
