#include "world.h"

namespace amaze {
    World::World() : _intro_mode{false} {}

    Camera& World::camera() {
        return _camera;
    }

    const Camera& World::camera() const {
        return _camera;
    }

    Player& World::player(int32_t id) {
        // nobody ever got fired for following Scott Meyers
        return const_cast<Player&>(static_cast<const World&>(*this).player(id));
    }

    const Player& World::player(int32_t id) const {
        for (auto& p : _players) {
            if (p.id == id) {
                return p;
            }
        }
        throw std::runtime_error{"id not found"};
    }

    std::vector<Player>& World::players() {
        return _players;
    }

    const std::vector<Player>& World::players() const {
        return _players;
    }

    level::Level& World::level() {
        return _level;
    }

    const level::Level& World::level() const {
        return _level;
    }

    std::vector<Splat>& World::splats() {
        return _splats;
    }

    const std::vector<Splat>& World::splats() const {
        return _splats;
    }

    bool& World::intro_mode() {
        return _intro_mode;
    }

    bool World::intro_mode() const {
        return _intro_mode;
    }

    uint8_t& World::outro_index() {
        return _outro_index;
    }

    uint8_t World::outro_index() const {
        return _outro_index;
    }

    std::map<amaze::player_id_t, int32_t>& World::scores() {
        return _scores;
    }

    const std::map<amaze::player_id_t, int32_t>& World::scores() const {
        return _scores;
    }

    bool World::full() const {
        uint16_t slots = level().stats().starts();
        size_t counter = players().size();

        return counter >= slots;
    }

    void World::operator=(const World_status& status) {
        _players = status.players;
        _splats  = status.splats;
        _scores  = status.scores;
    }

    void World::applyScore(Player player, int32_t score) {
        _scores[player.id] += score;
    }
}
