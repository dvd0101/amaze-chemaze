#include <array>
#include <utility>

namespace amaze {
    namespace details {
        constexpr float calc_probs(int index, int max) {
            return float(max - index) / (2 * max);
        }

        template <int... Ints>
        constexpr std::array<float, sizeof...(Ints)> splat_type_probability(std::integer_sequence<int, Ints...>) {
            constexpr int size = sizeof...(Ints);
            return std::array<float, size>{calc_probs(Ints, size)...};
        }
    }

    template <int M>
    constexpr std::array<float, M> splat_type_probability() {
        // generate a table (with size M) of probabilities; the probability
        // associated with every element is maximum at the index 0 (50%) and
        // decreases with increasing index.
        return details::splat_type_probability(std::make_integer_sequence<int, M>{});
    }
}
