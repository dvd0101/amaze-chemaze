#pragma once
#include "config.h"
#include "events.h"
#include "world.h"
#include "../models/geom.h"
#include "../level/builder.h"
#include "../audio/audio.h"
#include "splat_type_probability.h"
#include <cmath>
#include <boost/variant/get.hpp>
#include <boost/variant/static_visitor.hpp>
#include <vector>
#include <algorithm>
#include <random>

namespace amaze {
    namespace details {
        template <typename M>
        Cube_coord coord(const glm::vec3 pos) {
            return {static_cast<int32_t>(std::floor(pos.x / M::tile_size)),
                    static_cast<int32_t>(std::floor(pos.y / M::tile_size)),
                    static_cast<int32_t>(std::floor(pos.z / M::tile_size))};
        }

        optional<Point> intersect2d(const Segment& s, const Segment& q) {
            using std::swap;

            auto u = s.b - s.a;
            auto v = q.b - q.a;

            // Get an orthogonal vector
            auto vn = v;
            swap(vn.x, vn.y);
            vn.x *= -1;

            auto w  = s.a - q.a;
            float D = glm::dot(vn, u);
            if (std::abs(D) < 0.00001) {
                // s and q are parallel
                return {};
            }

            float t = (-1 * glm::dot(vn, w)) / D;
            if (t < 0 || t > 1) {
                return {};
            }
            Point I = s.a + t * u;
            return I;
        };

        template <typename M>
        Point tile_position(Tile_coord coord, uint16_t z = 0) {
            return Point{(coord.x * M::tile_size) + (M::tile_size / 2.0),
                         (coord.y * M::tile_size) + (M::tile_size / 2.0),
                         (z * M::tile_size)};
        }

        // adjust_next_position given a player and their direction returns
        // the new position and the portal score that should be assigned to
        // the player (0 if a portal wasn't crossed)
        template <typename M>
        std::tuple<Point, int8_t> adjust_next_position(const level::Level& lvl, const Player& player, const Direction& dir) {
            auto p0 = player.position;
            auto t0 = player.tile;

            const float step = World_config::step_size;
            const float steps = World_config::step_size * 8.f;

            // Candidate position
            Point p1 = p0 + dir * step;

            // Lookahead which tile we'll hit in some steps - we need to stop
            // in front of a wall way before than reaching it, or we won't be
            // able to draw it (too close)
            Point lh = p0 + dir * steps;
            auto t1  = coord<M>(lh);

            auto next_tile = lvl.what(t1);
            if (!next_tile) {
                // the next_pos is outside the level, better to stop here
                return std::make_tuple(p0, 0);
            }

            if (auto ptr = boost::get<level::Portal>(&(*next_tile))) {
                // it's a portal! let's relocate the player
                Point next_pos = tile_position<M>(ptr->dest);
                return std::make_tuple(next_pos, ptr->score);
            }

            if (boost::get<level::Wall>(&(*next_tile)) == nullptr) {
                // the next tile is not a wall, all fine
                return std::make_tuple(p1, 0);
            }

            // We're probably in front of a wall - try the X direction only
            Point xp1 = p0 + Direction(dir.x, 0.f, 0.f) * steps;
            auto xt1 = coord<M>(xp1);
            auto next_tile_x = lvl.what(xt1);

            if (boost::get<level::Wall>(&(*next_tile_x)) == nullptr) {
                Point next_pos = p0 + Direction(dir.x, 0.f, 0.f) * step;
                return std::make_tuple(next_pos, 0);
            }
            if (auto ptr = boost::get<level::Portal>(&(*next_tile_x))) {
                Point next_pos = tile_position<M>(ptr->dest);
                return std::make_tuple(next_pos, ptr->score);
            }

            // We're probably in front of a wall - try the Y direction only too
            Point yp1 = p0 + Direction(0.f, dir.y, 0.f) * steps;
            auto yt1 = coord<M>(yp1);
            auto next_tile_y = lvl.what(yt1);
            if (boost::get<level::Wall>(&(*next_tile_y)) == nullptr) {
                Point next_pos = p0 + Direction(0.f, dir.y, 0.f) * step;
                return std::make_tuple(next_pos, 0);
            }
            if (auto ptr = boost::get<level::Portal>(&(*next_tile_y))) {
                Point next_pos = tile_position<M>(ptr->dest);
                return std::make_tuple(next_pos, ptr->score);
            }

            // No candidate position available, confirm current position.
            return std::make_tuple(p0, 0);
        }

        template <typename M>
        void sync_camera_pos(World& world, const Player& p) {
            auto eye = p.position;
            eye.z += M::player_height;
            world.camera().position(eye);
        }

        template <typename M>
        optional<Splat> hit_point(const level::Level& lvl, const Player& p) {
            const auto eye = Point(p.position.x, p.position.y, p.position.z + M::player_height);
            auto f = [ p0 = eye, dir = p.looks_to.dir() ](float t0, float t1) {

                Point a = p0 + t0 * dir;
                Point b = p0 + t1 * dir;
                return Segment{a, b};
            };

            float t0      = 0;
            float delta_t = M::tile_size / 3;

            std::vector<Point> intersections;
            std::vector<Direction> impact_vectors;
            while (!intersections.size()) {
                auto t1         = t0 + delta_t;
                auto seg        = f(t0, t1);
                auto next_coord = coord<M>(seg.b);
                auto next_tile  = lvl.what(next_coord);

                if (!next_tile) {
                    // the shot fell out of the world
                    break;
                }

                // the cast from int32 to uint16_t is safe because we know that
                // next_tile is inside the level
                Tile_coord tc{static_cast<uint16_t>(next_coord.x),  //
                              static_cast<uint16_t>(next_coord.y)}; //
                auto triangles = level::build_single_tile(lvl, tc);
                for (auto& t : triangles) {
                    if (auto i = intersect(seg, t)) {
                        intersections.push_back(*i);
                        impact_vectors.push_back(glm::normalize(seg.a - seg.b));
                    }
                }

                t0 = t1;
            }

            // No intersections means the splat didn't reach any wall.
            if (!intersections.size()) {
                return {};
            }

            // Multiple intersections: the splat ray intersects more than one
            // wall, by definition we're only interested in the first one.
            if (intersections.size() > 1) {
                auto best = std::min_element(
                    std::begin(intersections),
                    std::end(intersections),
                    [eye](const Point& a, const Point& b) {
                        auto da = glm::distance(static_cast<glm::vec3>(eye), static_cast<glm::vec3>(a));
                        auto db = glm::distance(static_cast<glm::vec3>(eye), static_cast<glm::vec3>(b));
                        return da < db;
                    });

                size_t best_pos = best - std::begin(intersections);

                return Splat{*best, impact_vectors[best_pos]};
            }

            return Splat{intersections[0], impact_vectors[0]};
        }

        uint8_t pick_splat_type() {
            constexpr auto probabilities = amaze::splat_type_probability<Splat::types>();
            std::uniform_real_distribution<float> dis{0, 1};
            const float p = dis(amaze::rnd);

            uint8_t type = 0;
            float prob = 0;
            for (float v : probabilities) {
                if (p <= prob + v) {
                    break;
                }
                prob += v;
                type++;
            }
            if (type == Splat::types) {
                type--;
            }
            return type;
        }

        template <typename M>
        void process(World& world, player_id_t local_player, const Player_moves& evt) {
            if (world.intro_mode()) {
                return;
            }
            auto& p               = world.player(evt.player);
            glm::vec3 next_pos    = p.position;

            Direction towards;

            switch (evt.direction) {
            case Move_dir::forward:
            case Move_dir::backward: {
                float c  = evt.direction == Move_dir::forward ? 1 : -1;
                towards = p.looks_to.dir_xy() * c;
                break;
            }
            case Move_dir::left:
            case Move_dir::right:
                using std::swap;
                {
                    float c  = evt.direction == Move_dir::left ? -1 : 1;
                    auto dir = p.looks_to.dir_xy();
                    swap(dir.x, dir.y);
                    dir.x *= -1;
                    towards = dir * c;
                    break;
                }
            }

            auto np = adjust_next_position<M>(world.level(), p, towards);

            next_pos = std::get<0>(np);
            int8_t portal_score = std::get<1>(np);

            // The player might have jumped through a portal. Let's update the
            // world state accordingly.
            if (portal_score != 0) {
                world.applyScore(p, portal_score);
            }

            p.position = next_pos;

            auto next_tile = coord<M>(next_pos);
            p.tile.x       = next_tile.x;
            p.tile.y       = next_tile.y;

            if (p.id == local_player) {
                sync_camera_pos<M>(world, p);
            }
        }

        template <typename M>
        void process(World& world, player_id_t local_player, const Player_turns& evt) {
            if (world.intro_mode()) {
                return;
            }
            auto& p = world.player(evt.player);

            p.looks_to.phi += evt.delta_phi;
            p.looks_to.theta += evt.delta_theta;

            if (p.id == local_player) {
                world.camera().looks_to(p.looks_to.dir());
            }
        }

        template <typename M>
        void process(World& world, player_id_t local_player, const Player_starts& evt) {
            try {
                world.player(evt.player);
                // this player already exists
                return;
            } catch (std::runtime_error&) {
            }

            if (world.full()) {
                // no more room
                return;
            }

            auto& players  = world.players();
            size_t counter = players.size();
            Tile_coord pos;
            for (auto& spawn : world.level().iterate<level::Start>()) {
                if (counter == 0) {
                    pos = std::get<1>(spawn);
                    break;
                }
                --counter;
            }

            Player p;
            p.id = evt.player;
            p.position = tile_position<M>(pos);
            p.tile = pos;
            players.push_back(p);

            if (p.id == local_player) {
                sync_camera_pos<M>(world, p);
            }
        }

        template <typename M>
        void process(World& world, int32_t, const Player_fires& evt) {
            if (world.intro_mode()) {
                world.intro_mode() = false;
                return;
            }
            auto& p    = world.player(evt.player);
            auto splat = hit_point<M>(world.level(), p);
            if (!splat) {
                return;
            }
            splat->timestamp = evt.t;
            splat->type = pick_splat_type();
            world.splats().push_back(*splat);
        }

        template <typename M>
        void process(World&, player_id_t, const Clock_ticks&) {}

        template <typename M>
        void process(World& world, player_id_t, const Intro_starts&) {
            world.intro_mode() = true;

            playSound("../assets/intro.wav", 128);
        }

        template <typename M>
        void process(World& world, player_id_t, const Intro_ends&) {
            world.intro_mode() = false;
        }

        template <typename M>
        void process(World&, player_id_t, const Hurry_up&) {
            if (rand() % 10 == 0) {
                // Small chance for a secret wake up message to be played
                playSound("../assets/wake_up_easter.wav", 128);
            } else {
                playSound("../assets/wake_up.wav", 128);
            }
        }

        template <typename M>
        void process(World& world, player_id_t, const Outro_starts& evt) {
            world.outro_index() = evt.index;

            world.intro_mode() = true;
            if (evt.index == 1) {
                playSound("../assets/outro_1.wav", 128);
            } else {
                playSound("../assets/outro_2.wav", 128);
            }
        }

        template <typename M>
        void process(World& world, player_id_t, const Outro_ends&) {
            world.outro_index() = 0;
        }

        template <typename M>
        struct Event_visitor : boost::static_visitor<> {
            Event_visitor(World& w, player_id_t lid) : world{w}, local_player{lid} {}

            template <typename T>
            void operator()(const T& event) {
                process<M>(world, local_player, event);
            }

            World& world;
            player_id_t local_player;
        };
    }

    template <typename M = World_config>
    class Updater {
      public:
        Updater(World& w, player_id_t lid) : _world{w}, _local_player{lid} {}

        void process(const Event& evt) {
            details::Event_visitor<M> v{_world, _local_player};
            boost::apply_visitor(v, evt);
        }

        void reset(const World_status& status) {
            _world = status;
        }

        const World& world() const {
            return _world;
        }

      private:
        World& _world;
        player_id_t _local_player;
    };
}
