#pragma once
#include "stream.h"
#include <istream>
#include <vector>

namespace amaze {

    class Sink_stream : public Sink {
      public:
        Sink_stream(std::ostream&);

      private:
        void enqueue(const Event&) override;

      private:
        std::ostream& _stream;
        std::vector<uint8_t> _buffer;
    };

    class Source_stream : public Source {
      public:
        Source_stream(std::istream&);

      private:
        optional<Event> dequeue() override;
        bool fill_buffer();

      private:
        std::istream& _stream;
        std::vector<uint8_t> _buffer;
    };

    namespace details {
        class Sink_access {
            /*
            * This class is needed by `Sink_tee` in order to access the private
            * virtual method of the sink.
            */
          public:
            Sink_access(Sink& s) : sink{s} {}

            void call_process(Event& evt) {
                sink.process(evt);
            }

            void call_enqueue(const Event& evt) {
                sink.enqueue(evt);
            }

          private:
            Sink& sink;
        };

        class Source_access {
          public:
            Source_access(Source& s) : source{s} {}

            optional<Event> call_dequeue() {
                return source.dequeue();
            }

          private:
            Source& source;
        };
    }

    class Sink_tee : public Sink {
      public:
        Sink_tee(Sink& sink, Sink& copy);

      private:
        void process(Event&) override;
        void enqueue(const Event&) override;

      private:
        details::Sink_access _sink;
        details::Sink_access _copy;
    };

    class Source_cat : public Source {
      public:
        Source_cat(Source& first, Source& second);

      private:
        optional<Event> dequeue() override;

      private:
        Source& _first;
        Source& _second;
        bool _use_first;
    };

    inline auto tee(Sink& sink, Sink& copy) {
        return Sink_tee{sink, copy};
    }

    inline auto cat(Source& first, Source& second) {
        return Source_cat{first, second};
    }

    class Replay : public Source {
      public:
        Replay(Source& source, float speedup = 1.0f);

      private:
        optional<Event> dequeue() override;

      private:
        void fetch_next_event();

      private:
        Source& _source;
        float _speedup;

        uint32_t _counter;
        optional<Event> _next_event;
        amaze::time_point _replay_time;
        amaze::time_point _source_time;

        std::chrono::milliseconds _displacement_time;
    };
}
