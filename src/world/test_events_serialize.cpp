#include "catch/catch.hpp"
#include "events_serialize.h"
#include "../net/encoder.h"
#include <vector>

using namespace amaze;

SCENARIO("An event can be serialized and deserialized") {
    std::vector<uint8_t> buffer;
    auto w = make_writer(buffer);

    GIVEN("A `Clock_ticks` event") {
        Clock_ticks evt;

        WHEN("the event is serialized in a vector") {
            serialize(Event{evt}, w);

            THEN("The buffer contains the encoded bytes") {
                REQUIRE(buffer.size() == 13);
                REQUIRE(buffer[0] == Clock_ticks::action_code);
            }

            WHEN("the event is deserialized") {
                auto r = make_reader(buffer);
                Event evt2 = deserialize<Event>(r);

                THEN("The two events are equal") {
                    REQUIRE(Event{evt} == evt2);
                }
            }
        }
    }

    GIVEN("A `Player_moves` event") {
        Player_moves evt{Move_dir::backward};

        WHEN("the event is serialized in a vector") {
            serialize(Event{evt}, w);

            THEN("The buffer contains the encoded bytes") {
                REQUIRE(buffer.size() == 14);
                REQUIRE(buffer[0] == Player_moves::action_code);
            }

            WHEN("the event is deserialized") {
                auto r = make_reader(buffer);
                Event evt2 = deserialize<Event>(r);

                THEN("The two events are equal") {
                    REQUIRE(Event{evt} == evt2);
                }
            }
        }
    }

    GIVEN("A `Player_turns` event") {
        Player_turns evt{1, 0.8};

        WHEN("the event is serialized in a vector") {
            serialize(Event{evt}, w);

            THEN("The buffer contains the encoded bytes") {
                REQUIRE(buffer.size() == 21);
                REQUIRE(buffer[0] == Player_turns::action_code);
            }

            WHEN("the event is deserialized") {
                auto r = make_reader(buffer);
                Event evt2 = deserialize<Event>(r);

                THEN("The two events are equal") {
                    REQUIRE(Event{evt} == evt2);
                }
            }
        }
    }

    GIVEN("A `Player_starts` event") {
        Player_starts evt{};

        WHEN("the event is serialized in a vector") {
            serialize(Event{evt}, w);

            THEN("The buffer contains the encoded bytes") {
                REQUIRE(buffer.size() == 13);
                REQUIRE(buffer[0] == Player_starts::action_code);
            }

            WHEN("the event is deserialized") {
                auto r = make_reader(buffer);
                Event evt2 = deserialize<Event>(r);

                THEN("The two events are equal") {
                    REQUIRE(Event{evt} == evt2);
                }
            }
        }
    }
}
