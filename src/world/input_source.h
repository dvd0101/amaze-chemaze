#pragma once
#include <chrono>

namespace amaze {
    class Sink;

    class Input_source {
        /*
         * `Input_source` is a source user generated events.
         *
         * It fulfill two tasks:
         *
         * 1. It's a cross platform layer between our app and a platform
         * dependent source
         *
         *  for example the glue code between our app and a GLFW context calls
         *  the `move_forward` method whenever the `up` key is pressed.
         *
         * 2. It limits the generation of events; no matter how fast the inputs
         * are sent from the platform, it is generated at most one event every
         * X milliseconds (specified by the constructor)
         *
         *  for example the main drawing loop could be implemented like this
         *
         *  // draw the scene using opengl
         *  if (up_key is pressed)
         *    input_source.move_forward()
         *  else if (down_key is pressed)
         *    input_source.move_forward()
         *  ...etc
         *  input_source.touch();
         */
      public:
        Input_source(std::chrono::milliseconds, Sink&);

        void move_forward();
        void move_right();
        void move_backward();
        void move_left();
        void change_direction(float delta_phi, float delta_theta);
        void fire_pressed();

        // Informs the `Input_source` that some input arrived, an `Event`
        // may be generated and added to the `Sink` depending how much time has
        // passed from the previous time.
        // Returns `true` if an event has been generated.
        bool touch();

      private:
        void generate_events();
        void reset();

      private:
        using clock = std::chrono::system_clock;
        std::chrono::milliseconds interval;
        Sink& sink;

        clock::time_point t;
        bool forward;
        bool right;
        bool backward;
        bool left;
        float delta_phi, delta_theta;
        bool fire;

        clock::time_point last_shot;
    };
}
