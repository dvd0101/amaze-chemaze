#include "stream_adapters.h"
#include "events_serialize.h"
#include "../net/encoder.h"
#include <stdexcept>
#include <boost/endian/conversion.hpp>

namespace e = boost::endian;

namespace amaze {
    const int32_t max_record_size = 65535;

    Sink_stream::Sink_stream(std::ostream& os) : _stream{os} {}

    void Sink_stream::enqueue(const Event& evt) {
        // clears the buffer but keep the capacity, this should quickly
        // became a zero-allocation op
        _buffer.clear();

        auto w = make_writer(_buffer);
        serialize(evt, w);

        if (_buffer.size() > max_record_size) {
            throw std::runtime_error{"max size exceeded"};
        }
        uint16_t size   = e::native_to_big(static_cast<uint16_t>(_buffer.size()));
        const char* ptr = reinterpret_cast<const char*>(_buffer.data());

        _stream.write(reinterpret_cast<const char*>(&size), 2);
        _stream.write(ptr, _buffer.size());
    }

    Source_stream::Source_stream(std::istream& is) : _stream{is} {}

    optional<Event> Source_stream::dequeue() {
        if (!_stream.good() || !fill_buffer()) {
            return {};
        }

        auto r = make_reader(_buffer);

        try {
            return deserialize<Event>(r);
        } catch (std::runtime_error& e) {
            fmt::print("Error while decoding stream: {}\n", e.what());
            return {};
        }
    }

    bool Source_stream::fill_buffer() {
        // clears the buffer but keep the capacity, this should quickly
        // became a zero-allocation op
        _buffer.clear();

        int32_t length;
        {
            uint16_t l;
            _stream.read(reinterpret_cast<char*>(&l), 2);
            length = e::big_to_native(l);
        }

        if (length >= max_record_size) {
            throw std::runtime_error{"max size eceeded"};
        }

        _buffer.resize(length);
        auto ptr = reinterpret_cast<char*>(_buffer.data());
        _stream.read(ptr, length);
        if (_stream.gcount() != length) {
            return false;
        }
        return true;
    }

    Sink_tee::Sink_tee(Sink& sink, Sink& copy) : _sink{sink}, _copy{copy} {}

    void Sink_tee::process(Event& evt) {
        _sink.call_process(evt);
    }

    void Sink_tee::enqueue(const Event& evt) {
        _sink.call_enqueue(evt);
        _copy.call_enqueue(evt);
    }

    Source_cat::Source_cat(Source& first, Source& second) : _first{first}, _second{second}, _use_first{true} {}

    optional<Event> Source_cat::dequeue() {
        if (_use_first) {
            auto maybe = _first.pull();
            if (maybe) {
                return maybe;
            }
            _use_first = false;
        }
        return _second.pull();
    }

    Replay::Replay(Source& source, float speedup) : _source{source}, _speedup{speedup}, _counter{0} {}

    optional<Event> Replay::dequeue() {
        using std::swap;

        fetch_next_event();
        if (!_next_event) {
            return {};
        }

        auto& evt = *_next_event;
        if (_counter == 1) {
            _source_time = event_time(evt);
            _replay_time = now();

            _displacement_time = _replay_time - _source_time;
        } else {
            using namespace std::chrono;
            auto diff = now() - _replay_time;
            _replay_time += diff;
            _source_time += duration_cast<milliseconds>(diff * _speedup);
        }

        auto et = event_time(evt);
        if (et <= _source_time) {
            optional<Event> output;
            event_timewarp(*_next_event, _displacement_time);
            swap(output, _next_event);
            return output;
        } else {
            return {};
        }
    }

    void Replay::fetch_next_event() {
        if (_next_event) {
            return;
        }

        _next_event = _source.pull();
        ++_counter;
    }
}
