#include "catch/catch.hpp"
#include "stream.h"
#include "stream_adapters.h"
#include "../net/encoder.h"
#include "events_serialize.h"
#include <boost/variant/get.hpp>
#include <sstream>

using namespace amaze;

SCENARIO("An ostream can be adapted to work like a sink") {
    std::ostringstream dump;
    Sink_stream adapted{dump};

    Sink& sink = adapted;

    GIVEN("An event") {
        Clock_ticks evt;

        WHEN("The event is pushed in the sink") {
            sink.push(evt);

            THEN("the dump buffer contains a copy of the event") {
                std::string bytes = dump.str();

                REQUIRE(bytes.size() == 13 + 2);

                auto r = make_reader(bytes);
                r.discard<uint16_t>();
                auto evt2 = boost::get<Clock_ticks>(deserialize<Event>(r));

                REQUIRE(evt == evt2);
            }
        }
    }

    GIVEN("Multiple events") {
        Clock_ticks evt1;
        Player_starts evt2;

        WHEN("The events are pushed in the sink") {
            sink.push(evt1);
            sink.push(evt2);

            THEN("the dump buffer contains a copy of all the events") {
                std::string bytes = dump.str();

                REQUIRE(bytes.size() == 13 + 13 + 4);

                auto r = make_reader(bytes);

                r.discard<uint16_t>();
                auto out1 = boost::get<Clock_ticks>(deserialize<Event>(r));
                r.discard<uint16_t>();
                auto out2 = boost::get<Player_starts>(deserialize<Event>(r));

                REQUIRE(evt1 == out1);
                REQUIRE(evt2 == out2);
            }
        }
    }
}

SCENARIO("An istream can be adapted to work like a source") {
    GIVEN("a stream with two events") {
        // clang-format off
        std::istringstream dump{std::string{
            // Clock_ticks with t=10ms and player_id=15
            0x0, 0xd, 'T', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x0f,
            // Player_starts with t=10ms and player_id=15
            0x0, 0xd, 'S', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x0f
        }};
        // clang-format on
        Source_stream adapted{dump};
        Source& source = adapted;

        WHEN("an event is pulled from the source") {
            auto maybe = source.pull();
            THEN("The first event from the stream is retrieved") {
                REQUIRE(static_cast<bool>(maybe) == true);

                auto evt = boost::get<Clock_ticks>(*maybe);
                REQUIRE(evt.player == 15);
                REQUIRE(evt.t_ms() == 10);
            }
            WHEN("another event is pulled from the source") {
                auto maybe = source.pull();
                THEN("The second event from the stream is retrieved") {
                    REQUIRE(static_cast<bool>(maybe) == true);

                    auto evt = boost::get<Player_starts>(*maybe);
                    REQUIRE(evt.player == 15);
                    REQUIRE(evt.t_ms() == 10);
                }
                WHEN("another event is pulled from the source") {
                    auto maybe = source.pull();
                    THEN("no more events are retrieved") {
                        REQUIRE(static_cast<bool>(maybe) == false);
                    }
                }
            }
        }
    }
}

SCENARIO("The tee function can be used to copy the events pushed into a sink to another sink") {
    GIVEN("two sinks") {
        std::ostringstream dump1, dump2;
        Sink_stream s{dump1}, copy{dump2};

        auto sink = tee(s, copy);

        WHEN("an event is pushed into the sink") {
            Clock_ticks evt;
            sink.push(evt);

            THEN("both the sinks receive the event") {
                REQUIRE(dump1.str() == dump2.str());
            }
        }
    }
    GIVEN("two sinks, the first of which updates the events") {
        Stream s{15};
        std::ostringstream dump;
        Sink_stream copy{dump};

        auto sink = tee(s, copy);

        WHEN("an event is pushed into the sink") {
            Clock_ticks evt;
            sink.push(evt);

            THEN("both the sinks receive the updated event") {
                std::string bytes = dump.str();
                auto r            = make_reader(bytes);

                r.discard<uint16_t>();
                auto evt2 = boost::get<Clock_ticks>(deserialize<Event>(r));

                REQUIRE(evt2.player == 15);
                REQUIRE(evt.t == evt2.t);
            }
        }
    }
}

SCENARIO("The cat function can be used to concatenate two sources") {
    GIVEN("Two sources with an event each") {
        // clang-format off
        std::istringstream dump1{std::string{
            // Clock_ticks with t=10ms and player_id=15
            0x0, 0xd, 'T', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x0f
        }};
        // clang-format on
        Source_stream s1{dump1};

        // clang-format off
        std::istringstream dump2{std::string{
            // Player_starts with t=10ms and player_id=15
            0x0, 0xd, 'S', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x0f
        }};
        // clang-format on
        Source_stream s2{dump2};

        auto source = cat(s1, s2);

        WHEN("the first event is pulled") {
            auto maybe = source.pull();

            THEN("it is the one from the first source") {
                REQUIRE(static_cast<bool>(maybe) == true);
                boost::get<Clock_ticks>(*maybe);
            }

            WHEN("the second event is pulled") {
                auto maybe = source.pull();

                THEN("it is the one from the second source") {
                    REQUIRE(static_cast<bool>(maybe) == true);
                    boost::get<Player_starts>(*maybe);
                }
            }
        }
    }
    GIVEN("Two sources") {
        Stream s1{10}, s2{15};

        s1.push(Clock_ticks{});
        s2.push(Clock_ticks{});

        auto source = cat(s1, s2);

        WHEN("the first source is exhausted") {
            auto maybe = source.pull();
            REQUIRE(boost::get<Clock_ticks>(*maybe).player == 10);

            maybe = source.pull();
            REQUIRE(boost::get<Clock_ticks>(*maybe).player == 15);

            maybe = source.pull();
            REQUIRE(static_cast<bool>(maybe) == false);

            THEN("successive events are fetched only from the second source") {
                s1.push(Clock_ticks{});
                s2.push(Clock_ticks{});

                auto maybe = source.pull();
                REQUIRE(boost::get<Clock_ticks>(*maybe).player == 15);

                maybe = source.pull();
                REQUIRE(static_cast<bool>(maybe) == false);
            }
        }
    }
}
