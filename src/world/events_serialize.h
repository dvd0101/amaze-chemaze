#pragma once
#include "../net/serdes.h"
#include "events.h"
#include <boost/variant/static_visitor.hpp>
#include "format/format.h"

namespace amaze {

    namespace events_serialize {
        template <typename Writer>
        class Serialize_visitor : boost::static_visitor<> {
          public:
            Serialize_visitor(Writer& o) : sink{o} {}

            template <typename T>
            void operator()(const T& evt) {
                common(evt);
            }

            void operator()(const Player_moves& evt) {
                common(evt);
                sink << static_cast<uint8_t>(evt.direction);
            }

            void operator()(const Player_turns& evt) {
                common(evt);
                sink << evt.delta_phi << evt.delta_theta;
            }

            template <typename T>
            void common(const T& evt) {
                constexpr char code = T::action_code;

                sink << code;
                sink << evt.t;
                sink << evt.player;
            }

            Writer& sink;
        };

        template <typename Reader>
        class Deserialize_visitor : boost::static_visitor<> {
          public:
            Deserialize_visitor(Reader& i) : source{i} {}

            template <typename T>
            void operator()(T& evt) {
                common(evt);
            }

            void operator()(Player_moves& evt) {
                common(evt);
                uint8_t d;
                source >> d;
                evt.direction = static_cast<Move_dir>(d);
            }

            void operator()(Player_turns& evt) {
                common(evt);
                source >> evt.delta_phi >> evt.delta_theta;
            }

            template <typename T>
            void common(T& evt) {
                source >> evt.t;
                source >> evt.player;
            }

            Reader& source;
        };
    }

    template <>
    struct SerDes<Event> {
        template <typename Writer>
        void serialize(const Event& evt, Writer& sink) const {
            events_serialize::Serialize_visitor<Writer> v{sink};
            boost::apply_visitor(v, evt);
        }

        template <typename Reader>
        Event deserialize(Reader& source) const {
            char marker;
            source >> marker;

            Event evt;
            switch (marker) {
            case 'M':
                evt = Player_moves{};
                break;
            case 'D':
                evt = Player_turns{};
                break;
            case 'T':
                evt = Clock_ticks{};
                break;
            case 'S':
                evt = Player_starts{};
                break;
            case 'F':
                evt = Player_fires{};
                break;
            case 'I':
                evt = Intro_starts{};
                break;
            case 'J':
                evt = Intro_ends{};
                break;
            case 'H':
                evt = Hurry_up{};
                break;
            case 'Y':
                evt = Outro_starts{};
                break;
            case 'Z':
                evt = Outro_ends{};
                break;
            default:
                throw std::runtime_error{fmt::format("unexpected marker: {}\n", marker)};
            };
            events_serialize::Deserialize_visitor<Reader> v{source};
            boost::apply_visitor(v, evt);
            return evt;
        }
    };
}
