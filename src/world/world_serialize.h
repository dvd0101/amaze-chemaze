#pragma once
#include "../net/serdes.h"
#include "world.h"
#include "../models/entities_serialize.h"

namespace amaze {
    template <>
    struct SerDes<World_status> {
        template <typename Writer>
        void serialize(const World_status& w, Writer& sink) const {
            sink << static_cast<uint8_t>(w.players.size());
            for (const auto& p : w.players) {
                sink << p;
            }
            sink << static_cast<uint16_t>(w.splats.size());
            for (const auto& s : w.splats) {
                sink << s;
            }
            sink << static_cast<uint8_t>(w.scores.size());
            for (const auto& sc : w.scores) {
                sink << sc.first;
                sink << sc.second;
            }
        }

        template <typename Reader>
        World_status deserialize(Reader& source) const {
            World_status w;

            {
                uint8_t np;
                source >> np;
                w.players.resize(np);
            }

            for (auto& p : w.players) {
                source >> p;
            }

            {
                uint16_t ns;
                source >> ns;
                w.splats.resize(ns);
            }

            for (auto& s : w.splats) {
                source >> s;
            }

            {
                uint8_t nsc;
                source >> nsc;

                w.scores.clear();
                for (int is = 0; is < nsc; ++is) {
                    amaze::player_id_t key;
                    int32_t value;

                    source >> key;
                    source >> value;

                    w.scores[key] = value;
                }
            }

            return w;
        }
    };
}
