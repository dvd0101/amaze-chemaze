#include "catch/catch.hpp"
#include <cmath>
#include "input_source.h"
#include "stream.h"
#include "updater.h"
#include "world.h"

using namespace amaze;

namespace amaze {
    // XXX ATM Sphere_point does not have an operator== (due the lack of a
    // meaningful epsilon), this function is defined to ease the tests
    bool operator==(const amaze::Sphere_point& a, const amaze::Sphere_point& b) {
        const double epsilon = 0.00001;
        return std::abs(a.phi - b.phi) < epsilon && std::abs(a.theta - b.theta) < epsilon;
    }
}

auto process = [](Updater<>& u, Source& source) {

    size_t counter = 0;
    while (const auto& evt = source.pull()) {
        u.process(*evt);
        counter++;
    }
    return counter;
};

const float player_height = World_config::player_height;
const float step_size = World_config::step_size;
const float tile_size = World_config::tile_size;

SCENARIO("User input modify the world state", "[world]") {
    GIVEN("A world in a well known state, a Stream of events, an Input_source and an Updater") {
        World world;
        Stream stream{1};
        Input_source input{std::chrono::milliseconds{0}, stream};
        Updater<> u{world, 1};

        auto& lvl = world.level();
        lvl.add(level::Start{}, {0, 0});
        stream.push(Player_starts{});
        process(u, stream);

        auto& player = world.player(1);

        WHEN("The direction is changed by 90 degrees") {
            input.change_direction(pi / 2, 0);

            THEN("An event is pushed in the stream and the world updated") {
                REQUIRE(input.touch());
                REQUIRE(process(u, stream) == 1);

                THEN("the player (and thus the camera) is now looking at {0, 1, 0}") {
                    REQUIRE(player.position == Point(0.5, 0.5, 0));
                    REQUIRE(player.looks_to == Sphere_point(pi / 2, 0));
                    REQUIRE(world.camera().position() == Point(0.5, 0.5, player_height));
                    REQUIRE(world.camera().looks_to().x == Approx(0));
                    REQUIRE(world.camera().looks_to().y == Approx(1));
                    REQUIRE(world.camera().looks_to().z == Approx(0));
                }
            }
        }
        WHEN("The direction is changed by 90 degrees around the Z axis and 45 degrees around the X axis") {
            input.change_direction(pi / 2, pi / 4);

            THEN("An event is pushed in the stream and the world updated") {
                REQUIRE(input.touch());
                REQUIRE(process(u, stream) == 1);

                THEN("the player (and thus the camera) is now looking at {0, 1, 0}") {
                    REQUIRE(player.position == Point(0.5, 0.5, 0));
                    REQUIRE(player.looks_to == Sphere_point(pi / 2, pi / 4));
                    REQUIRE(world.camera().position() == Point(0.5, 0.5, player_height));
                    REQUIRE(world.camera().looks_to().x == Approx(0));
                    REQUIRE(world.camera().looks_to().y == Approx(std::cos(pi / 4)));
                    REQUIRE(world.camera().looks_to().z == Approx(std::sin(pi / 4)));
                }
            }
        }
        WHEN("The player moves forward") {
            input.move_forward();

            THEN("An event is pushed in the stream and the world is updated") {
                REQUIRE(input.touch());
                REQUIRE(process(u, stream) == 1);

                THEN("the player (and thus the camera) is now at {0.5 + step, 0.5, 0}") {
                    REQUIRE(player.position == Point(0.5 + step_size, 0.5, 0));
                    REQUIRE(player.looks_to == Sphere_point(0, 0));
                    REQUIRE(world.camera().position() == Point(0.5 + step_size, 0.5, 0 + player_height));
                }
            }
        }
        WHEN("The player moves backward") {
            input.move_backward();

            THEN("An event is pushed in the stream and the world updated") {
                REQUIRE(input.touch());
                REQUIRE(process(u, stream) == 1);

                THEN("the player (and thus the camera) is now at {0.5 - step, 0.5, 0}") {
                    REQUIRE(player.position == Point(0.5 - step_size, 0.5, 0));
                    REQUIRE(player.looks_to == Sphere_point(0, 0));
                    REQUIRE(world.camera().position() == Point(0.5 - step_size, 0.5, 0 + player_height));
                }
            }
        }
        WHEN("The level is extended to include another tile") {
            world.level().add(level::Floor{}, {1, 0});

            WHEN("The player move two steps forward and one backward") {
                input.move_forward();
                input.touch();

                input.move_forward();
                input.move_backward();
                input.touch();

                THEN("Three events are pushed in the stream and the world updated") {
                    REQUIRE(process(u, stream) == 3);

                    THEN("the player (and thus the camera) is now at {0.5 + step, 0.5, 0}") {
                        REQUIRE(player.position == Point(0.5 + step_size, 0.5, 0));
                        REQUIRE(player.looks_to == Sphere_point(0, 0));
                        REQUIRE(world.camera().position() == Point(0.5 + step_size, 0.5, 0 + player_height));
                    }
                }
            }
        }
    }
}

SCENARIO("Users enter the world via a start event", "[world]") {
    GIVEN("A level with two spawn points") {
        World world;
        Stream stream{1};
        Updater<> u{world, 1};

        auto& lvl = world.level();
        lvl.add(level::Floor{}, {0, 0});
        lvl.add(level::Start{}, {1, 0});
        lvl.add(level::Start{}, {2, 0});

        REQUIRE(world.players().size() == 0);

        WHEN("A source event is added to the sink") {
            stream.push(Player_starts{});
            process(u, stream);

            THEN("a new player appears in one of the start points") {
                REQUIRE(world.players().size() == 1);
                REQUIRE_NOTHROW(world.player(1));

                auto& p   = world.player(1);
                auto p1 = Point(1 + tile_size / 2.0, tile_size / 2.0, 0);
                auto p2 = Point(2 + tile_size / 2.0, tile_size / 2.0, 0);
                bool good = (p.position == p1) || (p.position == p2);
                REQUIRE(good);

                THEN("the camera is linked to the player") {
                    auto pos = p.position;
                    pos.z += player_height;
                    REQUIRE(world.camera().position() == pos);
                    REQUIRE(world.camera().looks_to() == Direction(1, 0, 0));
                }
            }
        }

        WHEN("Two source events are added to the sink") {
            Player_starts p1;
            Player_starts p2;
            p2.player = 2;
            stream.push(p1);
            stream.push(p2);
            REQUIRE(process(u, stream) == 2);

            THEN("both players appears") {
                REQUIRE(world.players().size() == 2);
                REQUIRE_NOTHROW(world.player(1));
                REQUIRE_NOTHROW(world.player(2));

                THEN("the camera is linked to local player") {
                    auto& p  = world.player(1);
                    auto pos = p.position;
                    pos.z += player_height;
                    REQUIRE(world.camera().position() == pos);
                    REQUIRE(world.camera().looks_to() == Direction(1, 0, 0));
                }
            }

            WHEN("a third player try to start") {
                Player_starts p3;
                p2.player = 3;
                stream.push(p3);
                THEN("the new player cannot enter") {
                    REQUIRE(world.players().size() == 2);
                    REQUIRE_THROWS_AS(world.player(3), std::runtime_error);
                }
            }
        }

        WHEN("A player try to enter multiple times") {
            Player_starts p1;
            stream.push(p1);
            stream.push(p1);
            REQUIRE(process(u, stream) == 2);

            THEN("only one registration is accepted") {
                REQUIRE(world.players().size() == 1);
                REQUIRE_NOTHROW(world.player(1));
            }
        }
    }
}
