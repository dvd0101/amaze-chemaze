#pragma once
#include "common.h"
#include "world/stream.h"
#include "gl/shaders.h"
#include <asio/ip/udp.hpp>
#include <asio/steady_timer.hpp>
#include <memory>
#include <string>

using amaze::optional;

struct config {
    struct {
        std::string filename;
        int32_t player;
        float speedup;
    } replay;

    struct {
        std::string filename;
    } record;

    struct {
        bool fullscreen;
    } display;

    optional<asio::ip::udp::endpoint> server;
};

optional<config> parse_cmdline(int, char* []);

class Sink_with_stream;

namespace details {
    class Sink_on_stream;
    class Source_from_stream;
}

class App_stream {
  public:
    App_stream(const config&, int32_t);
    ~App_stream();

    amaze::Sink& sink();
    amaze::Source& source();

  private:
    amaze::Stream _stream;

    amaze::Sink* _sink;
    std::unique_ptr<details::Sink_on_stream> _sink_stream;

    amaze::Source* _source;
    std::unique_ptr<details::Source_from_stream> _source_stream;
};

amaze::Program program(const std::string&);

asio::steady_timer timeout(asio::io_service&, std::chrono::milliseconds, std::function<void()>);
