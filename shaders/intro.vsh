// Input data: vertices position and texture coords
layout(location = 0) in highp vec3 v_position;
layout(location = 1) in highp vec2 v_texture_coord;

out highp vec2 f_texture_coord;

void main() {
    gl_Position = vec4(v_position, 1);
    f_texture_coord = v_texture_coord;
}

