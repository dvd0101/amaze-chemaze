precision highp float;

layout(location = 0) out highp vec4 out_color;

in highp vec2 f_texture_coord;
flat in int f_texture_id;

uniform sampler2D image_textures[4];

void main()
{
    vec4 color;
    switch (f_texture_id) {
        case 1:
            color = texture(image_textures[1], f_texture_coord);
            break;
        case 2:
            color = texture(image_textures[2], f_texture_coord);
            break;
        case 3:
            color = texture(image_textures[3], f_texture_coord);
            break;
        default:
            color = texture(image_textures[0], f_texture_coord);
            break;
    }
    out_color = 0.3 * vec4(1. - color.r, 1. - color.g, 1. - color.b, 1.);
}
