precision highp float;

layout(location = 0) out highp vec4 out_color;

in highp vec2 f_texture_coord;
uniform sampler2D image_texture;
uniform sampler2D noise_texture;
uniform float timestamp;

//   https://www.shadertoy.com/view/ldjGzV
float noise(vec2 p)
{
	float pick_sample = texture(noise_texture,vec2(1.,2.*cos(timestamp))*timestamp*8. + p*1.).x;
	pick_sample *= pick_sample;
	return pick_sample;
}

float onOff(float a, float b, float c)
{
	return step(c, sin(timestamp + a*cos(timestamp*b)));
}

float ramp(float y, float start, float end)
{
	float inside = step(start,y) - step(end,y);
	float fact = (y-start)/(end-start)*inside;
	return (1.-fact) * inside;
}

float stripes(vec2 uv)
{
	float noi = noise(uv*vec2(0.5,1.) + vec2(1.,3.));
	return ramp(mod(uv.y*4. + timestamp/2.+sin(timestamp + sin(timestamp*0.63)),1.),0.5,0.6)*noi;
}

vec3 getVideo(vec2 uv)
{
	vec2 look = uv;
	float window = 1./(1.+20.*(look.y-mod(timestamp/4.,1.))*(look.y-mod(timestamp/4.,1.)));
	look.x = look.x + sin(look.y*10. + timestamp)/50.*onOff(4.,4.,.3)*(1.+cos(timestamp*80.))*window;
	float vShift = 0.4*onOff(2.,3.,.9)*(sin(timestamp)*sin(timestamp*20.) +
										 (0.5 + 0.1*sin(timestamp*200.)*cos(timestamp)));
	look.y = mod(look.y + vShift, 1.);
	vec3 video = vec3(texture(image_texture,look));
	return video;
}

vec2 screenDistort(vec2 uv)
{
	uv -= vec2(.5,.5);
	uv = uv*1.2*(1./1.2+2.*uv.x*uv.x*uv.y*uv.y);
	uv += vec2(.5,.5);
	return uv;
}

void main()
{
	vec2 uv = f_texture_coord;
	uv = screenDistort(uv);
	vec3 video = getVideo(uv);
	float vigAmt = 3.+.3*sin(timestamp + 5.*cos(timestamp*5.));
	float vignette = (1.-vigAmt*(uv.y-.5)*(uv.y-.5))*(1.-vigAmt*(uv.x-.5)*(uv.x-.5));

	video += stripes(uv);
	video += noise(uv*2.)/2.;
	video *= vignette;
	video *= (12.+mod(uv.y*30.+timestamp,1.))/13.;

	out_color = vec4(video,1.0);
}
