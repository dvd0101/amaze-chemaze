// Input data: vertices position and texture coords
layout(location = 0) in highp vec3 v_position;
layout(location = 1) in highp vec2 v_texture_coord;
layout(location = 2) in int v_texture_id;
layout(location = 3) in int v_level;
layout(location = 4) in highp vec3 v_cube_center;

out highp vec2 f_texture_coord;
flat out int f_texture_id;
flat out vec3 f_color;

uniform mat4 mvp;
uniform mat4 transform;

void main() {
    vec4 p = vec4(v_position, 1);
    mat4 to_zero = mat4(
        1., 0., 0., -v_cube_center.x,
        0., 1., 0., -v_cube_center.y,
        0., 0., 1., -v_cube_center.z,
        0., 0., 0., 1.);
    mat4 from_zero = mat4(
        1., 0., 0., v_cube_center.x,
        0., 1., 0., v_cube_center.y,
        0., 0., 1., v_cube_center.z,
        0., 0., 0., 1.);
    mat4 t = mat4(1.0);
    t = from_zero * t;
    for (int i=0; i<v_level; i++) {
        t = transform * t;
    }
    t = to_zero * t;
    // Output position of the vertex, in clip space: MVP * position
    gl_Position = mvp * (p * t);

    f_texture_coord = v_texture_coord;
    f_texture_id = v_texture_id;
}
