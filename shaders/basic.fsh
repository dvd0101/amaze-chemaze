precision highp float;

layout(location = 0) out highp vec4 out_color;

in highp vec2 f_texture_coord;
flat in int f_texture_id;
flat in vec3 f_normal;
in highp vec3 f_position;

uniform sampler2D hint_textures[4];
uniform sampler2D splat_textures[4];
uniform sampler2D splats_data;
uniform int splats_count;
uniform sampler2D wall_sampler;

uniform float timestamp;

struct Splat
{
  //int id;
  vec3 center;
  vec3 normal;
  float timestamp;
  int type;
};

// easing functions: all take a value between 0 and 1, and return the value on
// the curve, again between 0 and 1.

float quadraticOut(float t) {
  return -t * (t - 2.0);
}

float exponentialOut(float t) {
  return t == 1.0 ? t : 1.0 - pow(2.0, -10.0 * t);
}

// pixel returns the UV coordinates from the splat texture we're supposed to
// use to populate fragment in f_position.
vec2 pixel(vec3 f_position, Splat splat) {

    // Distance normalized in 0..1 interval
    float norm_distance = distance(f_position, splat.center);

    // Projection of f_position onto the plane passing through the splat
    // point, and pointed in the same direction the splat arrived on wall.
    vec3 bd = f_position - splat.center;
    vec3 nn = dot(bd, splat.normal) * splat.normal;
    vec3 proj = bd - nn;

    // Reference vector on the same plane.
    // FIXME: the second argument of this cross product is chosen so that
    // it's the least likely to be parallel to the splat normal (usually
    // people doesn't shoot their own feet, hence the choice of the Z
    // vector).
    vec3 reference = normalize(cross(splat.normal, vec3(0, 0, -1)));

    // Decide whether the projected vector is on one side of the reference
    // or the other. Use the sign of cross product to discriminate.
    vec3 outgoing = cross(proj, reference);
    float sgn = sign(dot(outgoing, splat.normal));

    // Now, the dot product between normalized vectors is already the
    // cosine of the angle, while the cross product is the absolute value
    // of the sine (which we complete with the sign to cover -pi..0..pi).
    float cos_angle = dot(normalize(proj), reference);
    float sin_angle = length(cross(normalize(proj), reference)) * sgn;

    // Find a suitable (u, v) point rotating the vector
    float f = min(timestamp - splat.timestamp, 100.0) / 100.0;
    f = exponentialOut(f);
    float u = ((cos_angle * norm_distance) * 0.5) * (1.0 / f) + 0.5;
    float v = ((sin_angle * norm_distance) * 0.5) * (1.0 / f) + 0.5;

    return vec2(u, v);
}

void main()
{
    // Base color: what the player would see without a splat
    vec4 base_color = vec4(0.01, 0.01, 0.01, 1);

    for (int i = splats_count - 1; i >= 0; i--) {

        Splat splat;
        splat.center = texelFetch(splats_data, ivec2(0, i), 0).xyz;
        splat.normal = texelFetch(splats_data, ivec2(1, i), 0).xyz;

        vec3 aux = texelFetch(splats_data, ivec2(2, i), 0).xyz;
        splat.timestamp = aux.x;
        splat.type = int(aux.y);

        // If the current fragment is close enough to a splat point, consider
        // it affected
        float d = distance(f_position, splat.center);
        if (d < 1.0) {

            // Retrieve the UV coordinates and fetch texel
            vec2 uv = pixel(f_position, splat);
            vec4 splat_color;
            switch (splat.type) {
                case 1:
                    splat_color = texture(splat_textures[1], uv);
                    break;
                case 2:
                    splat_color = texture(splat_textures[2], uv);
                    break;
                case 3:
                    splat_color = texture(splat_textures[3], uv);
                    break;
                default:
                    splat_color = texture(splat_textures[0], uv);
                    break;
            }

            // Decode distance from the green and blue channels
            float dist = float(splat_color.g * 256.0 * 256.0 + splat_color.b * 256.0);

            float drip = 0.;
            // Normal doesn't point up or down, this is a wall
            if (abs(f_normal.z) < 0.01) {
                // Animation takes 10s in total
                drip = min(timestamp - splat.timestamp, 10000.0) / 10000.0;
                drip = quadraticOut(drip);
                // Maximum span is 512 pixels
                drip = drip * 512.0;
            }

            // Only draw if the pixel is part of the actual splat
            if (dist <= drip) {
                // We're part of the actual splat. If there's an underlay
                // draw it, otherwise go with the wall color

                vec4 hint_color;
                switch (f_texture_id) {
                    case 1:
                        hint_color = texture(hint_textures[1], f_texture_coord);
                        break;
                    case 2:
                        hint_color = texture(hint_textures[2], f_texture_coord);
                        break;
                    case 3:
                        hint_color = texture(hint_textures[3], f_texture_coord);
                        break;
                    default:
                        hint_color = texture(hint_textures[0], f_texture_coord);
                        break;
                }
                vec4 wall_color = texture(wall_sampler, f_texture_coord);
                float ff = 0.3;
                out_color = (1.0 - ff) * hint_color + ff * wall_color;
                return;
            }
        }
    }

    out_color = base_color;
}
