// Input data: vertices position and texture coords
layout(location = 0) in highp vec3 v_position;
layout(location = 1) in highp vec2 v_texture_coord;
layout(location = 2) in int v_texture_id;
layout(location = 3) in highp vec3 v_normal;

out highp vec2 f_texture_coord;
flat out int f_texture_id;
flat out vec3 f_normal;
out highp vec3 f_position;

// Model-View-Projection matrix
uniform mat4 mvp;

void main() {
    // Output position of the vertex, in clip space: MVP * position
    gl_Position = mvp * vec4(v_position, 1);

    f_texture_coord = v_texture_coord;
    f_texture_id = v_texture_id;
    f_normal = v_normal;
    f_position = v_position;
}
