# amaze Developers' manual

## Build on OS X

Install system dependencies:

```
brew install glfw3
brew install sdl2
```

Install clang 3.8.0 in <CLANG_PATH>, then create file ~/user-config.jam with the following content:

```
using darwin : : "<CLANG_PATH>/bin/clang" ;
```

Now, build dependencies:

```
cd support
./deps.sh all
```

And finally, build libamaze, tests and applications:

```
mkdir build
cd build
cmake .. --debug-output -DCMAKE_CXX_COMPILER=<CLANG_PATH>/bin/clang++
make -j4
make test
```

## Running client and server

amaze library includes a client/server protocol to host multiplayer sessions. To start a single client, just run:

```
./app
```

The client won't connect to any server and will allow the player to roam freely around the level.

To start a client/server session, start the server first with:

```
./net-server
```

Server will start listening on `0.0.0.0:5005`. Now, connect up to two clients this way:

```
./app 0.0.0.0 5005
```

## Building level

Before being able to load a level, it must be compiled in amaze format.

Install imagemagick on your system, then:

```
cd build
convert ../levels/level.xcf ../levels/level.png
./build_level ../levels/level.png ../levels/level1.txt
```